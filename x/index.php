<?php include('header.php'); ?>
        
  <main>
  
  	<section id="hero">
    	<div class="content">
    		<h1>Always made in SF, Always Fresh.</h1>
    		<h3>A small batch brewery in San Francisco handcrafting original, locally-driven brews.</h3>
    	</div>
  	</section>
  	
  	<section id="finder">
    	<div class="content">
      	<div class="left">
      		<h2>Find your<br>perfect brew</h2>
      		<h3>The beer list filters down based on your input. Compare your taste to a beer’s profile. Save your profiles and favorite beers. The beer list filters down based on your input. Compare your taste to a beer’s profile. Save your profiles and favorite beers.</h3>
      		<button class="launch large">Launch</button>
      	</div>
    	</div>
  	</section>
  	
  	<section id="learn">
    	<div class="content">
    		<h2>Let's Brew This Together</h2>
    		
    		<svg class="linebreak" width="100%" height="10px">
					<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
				</svg>
    		
    		<div class="items clearfix">
      		<div class="item">
        		<div class="image"></div>
        		<div class="title">Live. Local.</div>
        		<div class="description">Start creating your flavor profile by choosing a category to rate.</div>
      		</div>
      		<div class="item">
        		<div class="image"></div>
        		<div class="title">Our Brewers</div>
        		<div class="description">The beer list filters down based on your input. Compare your taste to a beer’s profile. Save your profiles and favorite beers.</div>
      		</div>
      		<div class="item">
        		<div class="image"></div>
        		<div class="title">Get Involved</div>
        		<div class="description">Use the slider to set your preference for the 3 flavor parameters.</div>
      		</div>
    		</div>
    	</div>
  	</section>
  	
  	<section id="story">
    	<div class="content">
	    	<div class="video">
    			<iframe src="//player.vimeo.com/video/108325180?wmode=transparent&title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="100%" height="607" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	    	</div>
    	</div>
  	</section>
  	
  </main>

<?php include('footer.php'); ?>