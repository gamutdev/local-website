
var	Application = function ()
{
	positron.Application.call (this);
}
positron.inherits (Application, positron.Application);

Application.prototype.start = function ()
{
	positron.Application.prototype.start.call (this);
	
	console.log('Application.start()');
	
	 $.ajax({
	   url: "http://localbrewingco.com/cms/json/?type=beerlist",
	   dataType: "jsonp",
	   success: function(inData){
		   //console.log(inData);
		   
		   var cacheKey = 'beerlist';
		   var cacheLifeTime = 15 * 60 * 1000;
		   gApplication.cache.put(cacheKey, inData, cacheLifeTime);
		   
		   console.log('beerlist cached');
		  }
	 });
	 
	 // Init tooltips
	 $('[data-toggle="tooltip"]').tooltip()
}

