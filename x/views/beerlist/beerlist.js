/**
*
* @license
* Copyright � 2013 Jason Proctor.  All rights reserved.
*
**/

var	BeerlistView = function ()
{
	console.log ("BeerlistView()");
	
	positron.View.call (this);
};
positron.inherits (BeerlistView, positron.View);

BeerlistView.prototype.onDOMReady = function ()
{
	console.log ("BeerlistView.onDOMReady()");
	
	var self = this;
	self.resizeScroller();
	
	var api = $(this).data('jsp');
	var throttleTimeout;
	
	$(window).resize(function(){
		self.resizeScroller();
		
		/*
		if (!throttleTimeout) {
			throttleTimeout = setTimeout(function(){
				api.reinitialise();
				throttleTimeout = null;
			}, 50);
		}
		*/
	});
	
	/*
	$("#list").mCustomScrollbar({
		axis: 'x',
		scrollbarPosition: 'inside',
		alwaysShowScrollbar: 2,
		setWidth: true,
		setHeight: true,
		setLeft: 0,
		setTop: '160px',
		scrollInertia: 0,
		mouseWheel: {
			enable: 1,
			axis: 'x',
			invert: 1
		}
	});
	*/
}

BeerlistView.prototype.resizeScroller = function ()
{
	var count = $('.beer-sum').length;
	var margin = $('section#beerlist .content').css('margin-right');
	
	$('#list .scroller').css('width', parseInt((count * 194) + parseInt(margin)) + 'px');
}

