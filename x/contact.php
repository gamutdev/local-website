<?php include('header.php'); ?>
        
  <main>
  
  	<section id="masthead">
    	<div class="content">
    		<div class="address">
	    		<img src="img/address.png">
    		</div>
    		<div class="hours">
					<h5>Open daily</h5>
					<ul class="leaders">
						<li><span>Monday</span><span>3pm - 12pm</span></li>
						<li><span>Tuesday</span><span>3pm - 12pm</span></li>
						<li><span>Wednesday</span><span>3pm - 12pm</span></li>
						<li><span>Thursday</span><span>3pm - 12pm</span></li>
						<li><span>Friday</span><span>3pm - 12pm</span></li>
						<li><span>Saturday</span><span>3pm - 12pm</span></li>
						<li><span>Sunday</span><span>3pm - 12pm</span></li>
					</ul>
					<a class="phone" href="tel: 415-555-5555">(415) 555-5555</a>
				</div>
    	</div>
  	</section>
  	
  	<section id="map">
	  	<div id="map-canvas"></div>
  	</section>
  	
  	<section id="contact"></section>
  	
  	<section id="coalition"></section>
  	
  </main>

<?php include('footer.php'); ?>