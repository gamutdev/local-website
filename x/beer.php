<?php include('header.php'); ?>
        
  <main>
  
  	<section id="masthead">
    	<div class="content">
    		<h1>Our Beer</h1>
    	</div>
  	</section>
  	
  	<section id="beerlist">
    	<div class="content">
      	<div class="left">
      		<h2>Beer List</h2>
      		<span class="selector">
      			<span class="hidden">Sort by: </span>
						<span class="">Style</span>
      		</span>
      	</div>
      	<div class="right">
	      	<div class="filters">
		      	<div 
			      	class="filter active"
				      p-action-1="(click) removeclass: active/#beerlist .filter"
					    p-action-2="(click) addclass: active/p-this-element"
						  p-action-3="(click) refreshview: beerlist"
							p-action-3-params="archive: false;"
				    >On Tap</div>
		      	<div 
			      	class="filter"
				      p-action-1="(click) removeclass: active/#beerlist .filter"
					    p-action-2="(click) addclass: active/p-this-element"
						  p-action-3="(click) refreshview: beerlist"
							p-action-3-params="archive: true;"
				    >Archive</div>
	      	</div>
      	</div>
      	<div id="list" p-view="beerlist:j" p-view-params="archive: false;">
	      	<div class="scroller scroll-pane">
		      	
		      	<p-json
							url="http://localbrewingco.com/cms/json/?type=beerlist"
							cachekey="beerlist"
							name="beers"
						>
						  <p-list key="beers.beers" name="beer">
							  
							  <p-if true="$params.archive; == false">
								  <p-if true="$beer.ontap; == true">
							
										<div 
											class="item beer-sum $beer.style;"
											p-action-1="(click) refreshview: single/nu-slide-in-from-right"
											p-action-1-params="name: $beer.name;; hoppyness: $beer.hoppyness;; alcohol: $beer.alcohol;; complexity: $beer.complexity;;"
											onclick="ga('send', 'event', 'taplist', 'beer', '$beer.name;');"
										>
							      	<div class="image">
								      	<img src="img/beer.png">
							      	</div>
							      	<div class="info">
												<h2 class="name">$beer.name;</h2>
												<div class="description">$beer.short_description;</div>
											</div>
							      	<div class="stats">
								      	<span class="abv"><span>$beer.abv;%</span> ABV</span> / 
								      	<span class="ibu"><span>$beer.ibu;</span> IBU</span>
							      	</div>
							      	<div class="levels">
												<div class="icon-hoppy"><div class="hoppy-level level">$beer.hoppyness;</div></div>
												<div class="icon-boozy"><div class="boozy-level level">$beer.alcohol;</div></div>
												<div class="icon-complex"><div class="complex-level level">$beer.complexity;</div></div>
											</div>
						      	</div>
									
								  </p-if>
							  </p-if>
							  
							  <p-if true="$params.archive; == true">
						
									<div 
										class="item beer-sum $beer.style;"
										p-action-1="(click) refreshview: single/nu-slide-in-from-right"
										p-action-1-params="name: $beer.name;; hoppyness: $beer.hoppyness;; alcohol: $beer.alcohol;; complexity: $beer.complexity;;"
										onclick="ga('send', 'event', 'taplist', 'beer', '$beer.name;');"
									>
						      	<div class="image">
							      	<img src="img/beer.png">
						      	</div>
						      	<div class="info">
											<h2 class="name">$beer.name;</h2>
											<div class="description">$beer.short_description;</div>
										</div>
						      	<div class="stats">
							      	<span class="abv"><span>$beer.abv;%</span> ABV</span> / 
							      	<span class="ibu"><span>$beer.ibu;</span> IBU</span>
						      	</div>
						      	<div class="levels">
											<div class="icon-hoppy"><div class="hoppy-level level">$beer.hoppyness;</div></div>
											<div class="icon-boozy"><div class="boozy-level level">$beer.alcohol;</div></div>
											<div class="icon-complex"><div class="complex-level level">$beer.complexity;</div></div>
										</div>
					      	</div>
								
							  </p-if>
							
							</p-list>	
						</p-json>
		      	
	      	</div>
      	</div>
    	</div>
  	</section>
  	
  	<section id="curator">
    	<div class="content">
	    	<div class="left">
		    	<div class="poi terms" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec mi non urna feugiat egestas. Nulla ut lacus felis. Aenean semper hendrerit nunc, at ultricies erat mattis sit amet. Aenean non tempor dolor. Nullam vel consequat augue.">1</div>
	    	</div>
    		<div class="right">
      		<h2>Beer Curator</h2>
      		<h3>The beer list filters down based on your input. Compare your taste to a beer’s profile. Save your profiles and favorite beers. The beer list filters down based on your input. Compare your taste to a beer’s profile. Save your profiles and favorite beers.</h3>
      		<button class="launch large">Launch</button>
      	</div>
    	</div>
  	</section>
  	
  	<section id="find">
	  	<div class="bar">
		  	<div class="content">
	    		<h2>Find Us</h2>
	    		<form>
		    		<div class="icon-search"></div>
		    		<input type="text" placeholder="Enter Zip Code">
	    		</form>
	    		
	    		<div class="right">
		    		<div class="filters">
			    		<div 
				      	class="filter active"
					      p-action-1="(click) removeclass: active/#find .filter"
						    p-action-2="(click) addclass: active/p-this-element"
					    >On Tap</div>
			      	<div 
				      	class="filter"
					      p-action-1="(click) removeclass: active/#find .filter"
						    p-action-2="(click) addclass: active/p-this-element"
					    >Retail</div>
		    		</div>
	    		</div>
	    	</div>
	  	</div>
	  	<div id="results"></div>
	  	<div id="map-canvas"></div>
  	</section>
  	
  	<section class="interstitial">
    	<div class="content">
    		<h2>Brewed by hand in San Francisco.</h2>
    	</div>
  	</section>
  	
  </main>

<?php include('footer.php'); ?>