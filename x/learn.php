<?php include('header.php'); ?>
        
  <main>
  
  	<section id="masthead">
    	<div class="content">
    		<h1>Learn</h1>
    	</div>
  	</section>
  	
  	<section id="live-local">
    	<div class="content">
	    	
	    	<h2>Live. Local.</h2>
    		
    		<svg class="linebreak" width="100%" height="10px">
					<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
				</svg>
	    	
        <div class="left">
        							                
          <div class="text">
            <div class="title">IF YOU&#039;RE HERE, YOU&#039;RE LOCAL.</div>
            <div class="description">We&#039;re building a place for those that value time with friends and contributing to locally made, handcrafted beer.</div> 
          </div>
						
									                
          <div class="text">
            <div class="title">WE’RE NOT A BREWPUB. WE’RE A BREWERY AND BEER BAR.</div>
            <div class="description">We’re a destination brewery on Bluxome St. in San Francisco that crafts small batches of beer built from your feedback.</div> 
          </div>
						
									                
          <div class="text">
            <div class="title">OUR SIGNATURE STYLE IS APPROACHABLE BEERS WITH CHARACTER</div>
            <div class="description">– the kind of brews you can enjoy more than one of and won’t be bored with. We use the finest ingredients we can get our hands on from the closest purveyors possible.</div> 
          </div>
						
						                    
	        <div class="text">
	        	<div class="red title"><span class="red">BEER CUSTOM BREWED FOR YOU.</span></div>
	        </div>
            
      	</div>
        <div class="right">
          <div class="pic_1 pic"></div>
          <div class="pic_2 pic"></div>
          <div class="pic_3 pic"></div>
          <div class="pic_4 pic"></div>
        </div>
    </div>
  	</section>
  	
  	<section id="team">
	  	
	  	<div class="content">
	  	
		  	<h2>Our Team</h2>
	  		
	  		<svg class="linebreak" width="100%" height="10px">
					<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
				</svg>
				
				<div class="grid">
					<div class="col-1">
						<div class="item">
							
							<div class="image"></div>
							
							<svg class="linebreak" width="100%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							
							<div class="name">Regan Long</div>
							<div class="title">Brewmaster <span>Since 2010</span></div>
							<div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur. Suspendisse mauris leo, aliquam quis auctor a, imperdiet molestie nulla. Sed ac purus nec augue dignissim condimentum in vel quam. Curabitur mollis sem sed massa pulvinar facilisis. Donec imperdiet molestie odio, ac feugiat est vulputate ut. Praesent id consequat odio.</div>
							
						</div>
					</div>
					<div class="col-3">
						
						<div class="item">
							
							<div class="image"></div>
							
							<svg class="linebreak" width="100%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							
							<div class="name">Regan Long</div>
							<div class="title">Brewmaster</div>
							
						</div>
						
						<div class="item">
							
							<div class="image"></div>
							
							<svg class="linebreak" width="100%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							
							<div class="name">Regan Long</div>
							<div class="title">Brewmaster</div>
							
						</div>
						
						<div class="item">
							
							<div class="image"></div>
							
							<svg class="linebreak" width="100%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							
							<div class="name">Regan Long</div>
							<div class="title">Brewmaster</div>
							
						</div>
						
					</div>
				</div>
			
	  	</div>
	  	
  	</section>
  	
  	<section id="guest-brewers">
	  	
	  	<div class="content">
		  	
		  	<h2>Guest Brewers</h2>
		  	
		  	<div class="list">
			  	
			  	<div class="item">
				  	<div class="left">
				  		<div class="image"></div>
			  		</div>
			  		<div class="right">
				  		<div class="name">Dave McLean</div>
				  		<svg class="linebreak" width="50%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#fafafa" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							<div class="beer">
								<div class="brewed">Beer brewed:</div>
								<span class="date">12/07/14</span>: <span class="name">Specialty Beer Name</span>
							</div>
							<div class="description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur. Suspendisse mauris leo, aliquam quis auctor a, imperdiet molestie nulla.
							</div>
			  		</div>
			  	</div>
			  	
			  	<div class="item">
				  	<div class="left">
				  		<div class="image"></div>
			  		</div>
			  		<div class="right">
				  		<div class="name">Dave McLean</div>
				  		<svg class="linebreak" width="50%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#fafafa" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							<div class="beer">
								<div class="brewed">Beer brewed:</div>
								<span class="date">12/07/14</span>: <span class="name">Specialty Beer Name</span>
							</div>
							<div class="description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur. Suspendisse mauris leo, aliquam quis auctor a, imperdiet molestie nulla.
							</div>
			  		</div>
			  	</div>
			  	
		  	</div>
		  	
	  	</div>
	  	
  	</section>
  	
  	<section id="workshops">
	  	
	  	<div class="content">
		  	<div class="left">
			  	<h2>Learn to brew!</h2>
			  	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur.</p>
		  	</div>
		  	<div class="right">
			  	<h2>Brewery Workshops</h2>
			  	
			  	<div class="list">
				  	
				  	<div class="item">
					  	<div class="event">
								<span class="date">12/07/14</span>: <span class="name">Workshop Name</span> <a href="#">Link</a>
							</div>
							<div class="description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur. Suspendisse mauris leo, aliquam quis auctor a, imperdiet molestie nulla.
							</div>
				  	</div>
				  	
				  	<div class="item">
					  	<div class="event">
								<span class="date">12/07/14</span>: <span class="name">Workshop Name</span> <a href="#">Link</a>
							</div>
							<div class="description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur. Suspendisse mauris leo, aliquam quis auctor a, imperdiet molestie nulla.
							</div>
				  	</div>
				  	
				  	<div class="item">
					  	<div class="event">
								<span class="date">12/07/14</span>: <span class="name">Workshop Name</span> <a href="#">Link</a>
							</div>
							<div class="description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur. Suspendisse mauris leo, aliquam quis auctor a, imperdiet molestie nulla.
							</div>
				  	</div>
				  	
				  	<div class="item">
					  	<div class="event">
								<span class="date">12/07/14</span>: <span class="name">Workshop Name</span> <a href="#">Link</a>
							</div>
							<div class="description">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ante sagittis ante iaculis consectetur. Suspendisse mauris leo, aliquam quis auctor a, imperdiet molestie nulla.
							</div>
				  	</div>
				  	
			  	</div>
		  	</div>
	  	</div>
	  	
  	</section>
  	
  	<section id="live"></section>
  	
  </main>

<?php include('footer.php'); ?>