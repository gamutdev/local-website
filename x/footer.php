    <footer>
      <div class="content">
        
        <div class="left">
	        <nav>
		        <ul>
			        <li class="header">Home</li>
			        <li class="header">Shop</li>
			        <li class="header">Contact</li>
		        </ul>
		        <ul>
			        <li class="header">Our Beer</li>
			        <li>On Tap</li>
			        <li>Archive</li>
			        <li>Beer Curator</li>
			        <li>Find</li>
		        </ul>
		        <ul>
			        <li class="header">Learn</li>
			        <li>Live. Local.</li>
			        <li>Our Team</li>
			        <li>Workshops</li>
			        <li>Live Feed</li>
		        </ul>
		        <ul>
			        <li class="header">Jobs</li>
			        <li class="header">Distributors</li>
			        <li class="header">Press</li>
			        <li class="header">Log In / Register</li>
		        </ul>
	        </nav>
	        
	        <ul class="socials">
		        <li class="facebook ding">G</li>
	        	<li class="twitter ding">t</li>
	        	<li class="instagram icon-instagram"></li>
	        	<li class="untappd icon-untappd"></li>
	        </ul>
	        
	        <div class="mailing">
		        <form action="http://localbrewingco.us7.list-manage.com/subscribe/post?u=6d79c2a11ade5ff422b6c3416&amp;id=f71339c37f" method="POST">
			        <label for="email">Get on our mailing list</label>
			        <input type="email" id="email" placeholder="Email Address">
			        <button>JOIN</button>
		        </form>
	        </div>
	        
	        <div class="copyright">&copy; 2015 Local Brewing Co. All Rights Reserved.</div>
        </div>
        
        <div class="right">
	        <div class="contact">
						<p class="address clearfix">
							<a href="http://maps.apple.com/?daddr=69+Bluxome+St+San+Francisco,+CA+94107">
								<img src="img/home-address.png">
							</a>
						</p>
						<div class="hours">
							<h5>Open daily</h5>
							<ul class="leaders">
								<li><span>Monday</span><span>3pm - 12pm</span></li>
								<li><span>Tuesday</span><span>3pm - 12pm</span></li>
								<li><span>Wednesday</span><span>3pm - 12pm</span></li>
								<li><span>Thursday</span><span>3pm - 12pm</span></li>
								<li><span>Friday</span><span>3pm - 12pm</span></li>
								<li><span>Saturday</span><span>3pm - 12pm</span></li>
								<li><span>Sunday</span><span>3pm - 12pm</span></li>
							</ul>
							<a class="phone" href="tel: 415-555-5555">(415) 555-5555</a>
						</div>	
					</div>
        </div>
        
    		<div class="logo">
      		<div p-view="logo:h"></div>
    		</div>
    	</div>
    </footer>

    <script src="js/jquery.mCustomScrollbar.js" type="text/javascript"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
  </body>
</html>