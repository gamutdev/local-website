<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Local Brewing Co. -- San Francisco, CA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <!-- styles -->
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
		<link href="positron/positron.css" type="text/css" rel="stylesheet"></link>
		<link type="text/css" href="css/jquery.mCustomScrollbar.min.css" rel="stylesheet" media="all" />
		
		<!-- scripts -->
		
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<script src="positron/prefixfree.min.js" type="text/javascript"></script>
		<script src="positron/prefixfree.dynamic-dom.min.js" type="text/javascript"></script>
		<script src="positron/positron.js" type="text/javascript"></script>
		
		<script type="text/javascript">
		  	var map, markers;
				var markersArray = [];
				var latlng = [];
				var infos = [];
		    var MY_MAPTYPE_ID = 'sfbrewersguild';
		    
		    function initialize() {
		  	  var stylez = [
				    {
					    "stylers": [
					      { "saturation": -100 }
					    ]
					  },{
					    "stylers": [
					      { "weight": 0.3 }
					    ]
					  },{
					    "featureType": "water",
					    "stylers": [
					      { "lightness": -65 }
					    ]
					  },{
					    "featureType": "road"  }
				  ];
		
				  var san_francisco = new google.maps.LatLng(37.775497756975646, -122.41795063018799);
		      var mapOptions = {
				    zoom: 13,
				    mapTypeControl: false,
				    mapTypeControlOptions: {
					    mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
					  },
					  panControl: false,
				    panControlOptions: {
				        position: google.maps.ControlPosition.TOP_RIGHT
				    },
				    streetViewControl: false,
				    zoomControl: true,
				    zoomControlOptions: {
				        style: google.maps.ZoomControlStyle.DEFAULT,
				        position: google.maps.ControlPosition.TOP_RIGHT
				    },
					  mapTypeId: MY_MAPTYPE_ID,
				    scrollwheel: false,
				    center: san_francisco
				  };
		
		      map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
		      map.panBy(-300, 0);       
		      
		      var styledMapOptions = {
				    name: 'SFBG'
				  };
				        
				  var guildMapType = new google.maps.StyledMapType(stylez, styledMapOptions);
				  map.mapTypes.set(MY_MAPTYPE_ID, guildMapType);
				  
				  //plotPoints();
				}
				
				function plotPoints(){
					$('.breweries .post').each(function(){
		    		var location_icon;  		
			    	var lat = $(this).data('lat');
			    	var lng = $(this).data('lng');
			    	var order = $(this).data('order');
			    	var title = $('.name', this).html();
					  var address = $('.info', this).html();
					  var street = $('.info .address', this).html();
					  var citystatezip = $('.info .city-state-zip', this).html();
			    	
			    	var contentString = '<div id="content">'+
				    '<div id="siteNotice">'+
					    '</div>'+
					    '<h2 style="font-size:16px;margin:0;text-shadow:none;">'+ title +'</h2>'+
					    '<div id="bodyContent">'+ address +'</div>'+
					    '<div class="directions" style="margin-top:6px;"><a href="http://maps.google.com/maps?saddr=&daddr='+ street + ', ' + citystatezip +'" target="_blank">Get Directions</a></div>'+
				    '</div>';
						
						var infowindow = new google.maps.InfoWindow({
						    content: contentString
						});
				    	
		        var markerpos = new google.maps.LatLng(lat, lng);
		        var marker = new google.maps.Marker({
			          map: map,
			          position: markerpos,
			          icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+ order +'|000000|FFFFFF'
			      });
			      
			      latlng.push(markerpos);
			      markersArray.push(marker);
			      
			      
			      function closeInfos(){
							if(infos.length > 0){
								infos[0].set("marker",null);
								infos[0].close();
								infos.length = 0;
							}
		        }
			      
			      google.maps.event.addListener(marker, 'click', function() {
			      	closeInfos();          
					    infowindow.open(map,marker); 
		          infos[0]=infowindow;
				    });
				    
				    // Clickthrough
						$(document).on('click', '.post', function(){
							 var markerID = $(this).data('order');
							 var marker = markersArray[markerID - 1];
				 	   	 var markerlatLng = marker.getPosition();
							 map.setCenter(markerlatLng);
							 google.maps.event.trigger(markersArray[markerID - 1], "click");
						});
		
		    	});
				}
				
				function loadScript() {
				  var script = document.createElement('script');
				  script.type = 'text/javascript';
				  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
				      '&callback=initialize';
				  document.body.appendChild(script);
				}
				
				window.onload = loadScript;
		</script>
</head>
<body class="contact">
    <!--[if lt IE 8]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <header>
      <nav>
	      
	      <div class="icon-menu"></div>
	      
        <ul>
	        <li class="logo">
	        	<div p-view="logo:h"></div>
	        </li>
	        <li
	        	p-action-1="(mouseover) removeclass: hidden/header nav.sub#beer"
	        	p-action-2="(mouseout) addclass: hidden/header nav.sub#beer"
	        >Our Beer
		        <nav 
			        id="beer"
			        class="sub hidden"
				      p-action-1="(mouseover) removeclass: hidden/header nav.sub#beer"
				      p-action-2="(mouseout) addclass: hidden/header nav.sub#beer"
				     >
			        <ul>
				        <li>On Tap</li>
				        <li>Archive</li>
				        <li>Curator</li>
				        <li>Find Us</li>
			        </ul>
		        </nav>
	        </li>
	        <li
	        	p-action-1="(mouseover) removeclass: hidden/header nav.sub#learn"
	        	p-action-2="(mouseout) addclass: hidden/header nav.sub#learn"
	        >Learn
	        	<nav
		        	id="learn" 
			        class="sub hidden"
				      p-action-1="(mouseover) removeclass: hidden/header nav.sub#learn"
				      p-action-2="(mouseout) addclass: hidden/header nav.sub#learn"
				     >
			        <ul>
				        <li>Live Local</li>
				        <li>Our Team</li>
				        <li>Workshops</li>
				        <li>Live Feed</li>
			        </ul>
		        </nav>
	        </li>
	        <li>Contact</li>
	        <li>Shop</li>
	        <li class="socials">
	        	<ul>
		        	<li class="facebook ding">G</li>
		        	<li class="twitter ding">t</li>
		        	<li class="instagram icon-instagram"></li>
		        	<li class="untappd icon-untappd"></li>
	        	</ul>
	        </li>
        </ul>
        
        <ul class="account">
	        <li class="username hidden">Shawn Scott</li>
	        <li class="avatar"></li>
        </ul>
      </nav>
    </header>