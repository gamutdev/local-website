<?php 
	/**
	 * Template Name: Contact
	 */
	 
	include('header.php'); 
?>
        
  <main>
  
  	<section id="masthead">
    	<div class="content">
    		<div class="address">
	    		<img src="img/address.png">
    		</div>
    		<div class="hours">
					<h5>Open daily</h5>
					<ul class="leaders">
						
						<?php
						$temp = $wp_query;
						$wp_query = null;
						$args = array( 'post_type' => 'page', 'pagename' => 'general' );
						$wp_query = new WP_Query($args);
						while ($wp_query->have_posts()) : $wp_query->the_post();
						
						if(get_field('hours')):
							while(has_sub_field('hours')):
							?>
						
							<li><span><?php the_sub_field('day'); ?></span><span><?php the_sub_field('time'); ?></span></li>
									
							<?php 
							endwhile;
						endif; 
						?> 
						
						<?php 
						endwhile; 
						wp_reset_postdata(); 
						?>
						
					</ul>
					
					<?php
					$temp = $wp_query;
					$wp_query = null;
					$args = array( 'post_type' => 'page', 'pagename' => 'general' );
					$wp_query = new WP_Query($args);
					while ($wp_query->have_posts()) : $wp_query->the_post();
					
					$phone = get_field('phone');
					$bar_menu = get_field('bar_menu');
					?>
					
					<a class="phone" href="tel: <?php echo $phone; ?>"><?php echo $phone; ?></a>
					
					<?php
					if(!empty($bar_menu)){
					?>
					<a class="bar-menu" href="<?php echo $bar_menu; ?>" target="_blank">Check out our FOOD MENU here! &raquo;</a>
					<?php
					}
					?>
					
					<?php 
					endwhile; 
					wp_reset_postdata(); 
					?>
					
				</div>
    	</div>
  	</section>
  	
  	<section id="map" class="visibility-web">
	  	<div p-view="map:cj" p-view-params="section: contact;">
		  	<div id="map-canvas">
			  	<div id="map"></div>
		  	</div>
	  	</div>
  	</section>
  	
  	<section id="contact" class="">
	  	<div class="content">
	  		<h2 class="visibility-web">Contact</h2>
	  		<div class="items">
		  		
		  		<?php
					$temp = $wp_query;
					$wp_query = null;
					$args = array( 'post_type' => 'page', 'pagename' => 'contact' );
					$wp_query = new WP_Query($args);
					while ($wp_query->have_posts()) : $wp_query->the_post();
					
					if(get_field('contact')):
						while(has_sub_field('contact')):
						
						$name = get_sub_field('name');
						$title = get_sub_field('title');
						$email_address = get_sub_field('email_address');
						?>
						
						<div class="item">
				  		<div class="name"><?php echo $name; ?></div>
				  		<?php
					  	if(!empty($title)){
					  	?>
				  		<div class="title"><?php echo $title; ?></div>
				  		<?php
					  	}
					  	?>
				  		<div class="email"><a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></div>
			  		</div>
								
						<?php 
						endwhile;
					endif; 
					?> 
					
					<?php 
					endwhile; 
					wp_reset_postdata(); 
					?>
		  		
	  		</div>
	  	</div>
  	</section>
  	
  	<section id="coalition" class="visibility-web">
	  	<div class="content">
		  	
		  	<?php
				$temp = $wp_query;
				$wp_query = null;
				$args = array( 'post_type' => 'page', 'pagename' => 'contact' );
				$wp_query = new WP_Query($args);
				while ($wp_query->have_posts()) : $wp_query->the_post();
				
				// Post specific	
				$post_id = $post->ID;
				$post_title = $post->post_title;
				$post_slug = $post->post_name;
				
				$headline = get_field('contact_coalition_headline');
				$intro = get_field('contact_coalition_intro');
				
				$investors = get_field('contact_coalition_investors');
				$geeks = get_field('contact_coalition_beer_geeks');
				?>
		  	
		  	<div class="top">
            <div class="title"><?php echo $headline; ?></div>
            <div class="text"><?php echo $intro; ?></div>
            <div class="sig"></div>
            <div class="emboss"></div>
        </div>

        <div class="dots"></div>
        <div class="bottom clearfix">
        
            <form class="investors block" action="<?=$_SERVER['PHP_SELF']; ?>" method="GET">
              <div class="title">Investors</div>
						
							<div class="text"><?php echo $investors; ?></div>						                
              
              <div class="first">
                  <input type="text" name="FULL_NAME" id="NAME" placeholder="Full Name" />
              </div>
              <div class="email">
                  <input type="text" name="EMAIL" id="INVESTOR_EMAIL" placeholder="Email Address"/>
              </div>
              <span id="response" style="display:none;">
								<?php //require_once('inc/store-address-createsend.php'); if($_GET['submit']){ echo storeAddress(); } ?>
							</span>
              <div id="send">
                  <button type="submit">REQUEST PACKET</button>
              </div>
            </form>
            
            <form class="geeks block" action="http://localbrewingco.us7.list-manage.com/subscribe/post?u=6d79c2a11ade5ff422b6c3416&amp;id=f71339c37f" method="POST">
              <div class="title">Beer Geeks</div>
              <div class="text"><?php echo $geeks; ?></div>							                      
              <div class="email">
                  <input type="text" name="EMAIL" id="LOCAL_EMAIL" placeholder="Email Address"/>
              </div>                    
							<div id="send">
              	<button type="submit">JOIN</button>
							</div>              
            </form>
            
        </div>
        
        <?php 
				endwhile; 
				wp_reset_postdata(); 
				?>
		  	
	  	</div>
  	</section>
  	
  </main>

<?php include('footer.php'); ?>