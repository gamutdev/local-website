<?php

/*
header("Content-Type: text/html");
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
*/
 
if (in_array($_SERVER['HTTP_USER_AGENT'], array(
  'facebookexternalhit/1.0 (+https://www.facebook.com/externalhit_uatext.php)',
  'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)'
))) {
  //it's probably Facebook's bot
  
  if(empty($_GET['job'])){
		$args = array( 'post_type' => 'jobs', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' );
		$url = 'http://localbrewingco.com/jobs';
	}	
	else {
		$args = array( 'post_type' => 'jobs', 'name' => $_GET['job'] );
		$url = 'http://localbrewingco.com/jobs?job='. $_GET['job'];
	}
  
  $temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query($args);
	while ($wp_query->have_posts()) : $wp_query->the_post();
	
	// Post specific	
	$post_id = $post->ID;
	$post_slug = $post->post_name;
	
	
	if(empty($_GET['job'])){
		$post_title = "Join Our Team";
		$description = "Jobs at Local Brewing Co.";
	}
	else {
		$post_title = "Join Our Team: ". $post->post_title;
		$description = get_field( "description" );
	}

  ?>
  <!doctype html>
	<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js" lang="en" xmlns:fb="https://www.facebook.com/2008/fbml"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Local Brewing Co. -- San Francisco, CA -- Small Tanks, Big Beer.</title>
		
		<meta property="og:site_name" content="Local Brewing Co."> 
	  <meta property="og:title" content="<?php echo $post_title; ?>" /> 
	  <meta property="og:url" content="<?php echo $url; ?>" />
		<meta property="og:image" content="http://localbrewingco.com/img/agecheck-logo.png" />
	  <meta property="og:description" content="<?php echo $description; ?>" /> 

	</head>
	
	<body>
	
	<?php 
		endwhile; 
		wp_reset_postdata(); 
	?>
  <?php
}
else {
	get_header();
?>
  
  <main>
	  
		<section id="masthead" style="background: url(img/masthead-distributors.png) 50% 50% no-repeat; background-size:cover;">
    	<div class="content">
    		<h1 class="visibility-web">Jobs</h1>
    	</div>
  	</section>
	  
		<section id="page">
			
			<?php
				if(empty($_GET['job'])){
					$activeclass = 'list';	
				}
				else {
					$activeclass = 'expanded';
				}
			?>
			
			<div class="content <?php echo $activeclass; ?>">
		  	
		  	<div class="left-column">
			    
			    <div class="content-section clearfix">
		  	
				  	<?php
						if(empty($_GET['job'])){
							$args = array( 'post_type' => 'jobs', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' );
						}	
						else {
							$args = array( 'post_type' => 'jobs', 'name' => $_GET['job'] );
						}
							
						$temp = $wp_query;
						$wp_query = null;
						$wp_query = new WP_Query($args);
						while ($wp_query->have_posts()) : $wp_query->the_post();
						
						// Post specific	
						$post_id = $post->ID;
						$post_title = $post->post_title;
						$post_slug = $post->post_name;
						
						$description = get_field('description');
						$requirements = get_field('requirements');
						$toapply = get_field('to_apply');
						$contact = get_field('contact');
						
						?>
              
            <div class="text">
              <h1 class="title"><?php echo $post_title; ?></h1>
              <div class="description"><?php echo $description; ?></div>
              <div class="requirements">
	              <h3>Required Skills &amp; Experience</h3>
	              <?php echo $requirements; ?>
	            </div>
              <div class="toapply">
	              <h3>To Apply</h3>
	              <?php echo make_clickable($toapply); ?>
	            </div> 
            </div>
            
            <?php 
						endwhile; 
						wp_reset_postdata(); 
						?>
					
		    	</div>
	    
				</div>
				
				<aside>
		    	<ul class="sections">
			    	
			    	<li class="all"><a href="<?php echo remove_query_arg( 'job' ); ?>">All</a></li>
			    	
			    	<?php
						$temp = $wp_query;
						$wp_query = null;
						$args = array( 'post_type' => 'jobs', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' );
						$wp_query = new WP_Query($args);
						while ($wp_query->have_posts()) : $wp_query->the_post();
						
						// Post specific	
						$post_id = $post->ID;
						$post_title = $post->post_title;
						$post_slug = $post->post_name;
						
						?>
						
						<li class="<?php if($_GET['job'] == $post_slug){ echo 'active'; } ?>"><a href="<?php echo add_query_arg( 'job', $post_slug ); ?>"><?php echo $post_title; ?></a></li>
						
						<?php 
						endwhile; 
						wp_reset_postdata(); 
						?>
						
					</ul>
		  	</aside>
		  	
		  	<a class="button launch large" href="<?php echo remove_query_arg( 'job' ); ?>">All Jobs</a>
				
			</div>
	  	
		</section>
  </main>
        
<?php 
get_footer(); 
}
?>
