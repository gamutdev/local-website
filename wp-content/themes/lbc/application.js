
var	Application = function ()
{
	positron.Application.call (this);
}
positron.inherits (Application, positron.Application);

Application.prototype.start = function ()
{
	positron.Application.prototype.start.call (this);
	
	console.log('Application.start()');
	
	 $.ajax({
	   url: "https://localbrewingco.com/json/?type=beerlist",
	   dataType: "jsonp",
	   success: function(inData){
		   //console.log(inData);
		   
		   var cacheKey = 'beerlist';
		   var cacheLifeTime = 15 * 60 * 1000;
		   gApplication.cache.put(cacheKey, inData, cacheLifeTime);
		   
		   console.log('beerlist cached');
		  }
	 });
	 
	 // Init tooltips
	 $('[data-toggle="tooltip"]').tooltip();
}

Application.prototype.destroyScroller = function ()
{
	
	console.log('Application.destroyScroller()');
	
	$('#beerlist #list').mCustomScrollbar("destroy");
	
}

Application.prototype.openCurator = function ()
{
	
	console.log('Application.openCurator()');
	
	var options = {
      height: 650, // sets the height in pixels of the window.
      width: 900, // sets the width in pixels of the window.
      toolbar: 0, // determines whether a toolbar (includes the forward and back buttons) is displayed {1 (YES) or 0 (NO)}.
      scrollbars: 0, // determines whether scrollbars appear on the window {1 (YES) or 0 (NO)}.
      status: 0, // whether a status line appears at the bottom of the window {1 (YES) or 0 (NO)}.
      resizable: 1, // whether the window can be resized {1 (YES) or 0 (NO)}. Can also be overloaded using resizable.
      left: 0, // left position when the window appears.
      top: 0, // top position when the window appears.
      center: 1, // should we center the window? {1 (YES) or 0 (NO)}. overrides top and left
      createnew: 0, // should we create a new window for each occurance {1 (YES) or 0 (NO)}.
      location: 0, // determines whether the address bar is displayed {1 (YES) or 0 (NO)}.
      menubar: 0 // determines whether the menu bar is displayed {1 (YES) or 0 (NO)}.
  };
  
  var left = (screen.width/2)-(options.width/2);
  var top = (screen.height/2)-(options.height/2);

  var parameters = "location=" + options.location +
                   ",menubar=" + options.menubar +
                   ",height=" + options.height +
                   ",width=" + options.width +
                   ",toolbar=" + options.toolbar +
                   ",scrollbars=" + options.scrollbars +
                   ",status=" + options.status +
                   ",resizable=" + options.resizable +
                   ",left=" + options.left +
                   ",screenX=" + options.left +
                   ",top=" + options.top +
                   ",screenY=" + options.top;

  var target = 'http://localbrewingco.com/curator/';  

  var popup = window.open(target, 'Local Brewing Co. -- Beer Curator', parameters);
	
}


Application.prototype.goToSection = function ()
{
	
	console.log('Application.goToSection()');
	
	var timing = 400;
	
	if(document.URL.indexOf('#') > -1){
		if(document.URL.indexOf(',') > -1){
			var section = document.URL.split('#')[1].split(',')[0];
			var subsection = document.URL.split(',')[1];
			page_subsection = subsection;
			
			if(subsection == "archive"){
				var archive = true;
			}
			else {
				var archive = false;
			}
			
			console.log(section);
		}
		else {
			var section = document.URL.split('#')[1];
			console.log(section);
		}
		
		if(section == "/curator"){
			var offset = -60;
		}
		else {
			var offset = -110;
		}
		
		if( $('body').hasClass('p-handheld') ){
			var timing = 1;
			var offset = -60;	
		}
		if( $('body').hasClass('p-tablet') ){
			var timing = 1;
			var offset = -110;	
		}
		if( $('body').hasClass('p-fullsize') ){
			var timing = 400;
			var offset = -110;	
		}
		
		var pre = '';
		
		if( $('body').hasClass('p-fullsize') ){
			if(section == "/beerlist"){
				var pre = '.visibility-web ';	
			}
		}
		
		if( $('body').hasClass('p-tablet') ){
			if(section == "/beerlist"){
				var pre = '.visibility-web ';	
			}
		}
		
		console.log(pre + '#' + section.replace('/', ''));
		
		if(section.length > 1){
			$(window).scrollTo( $(pre + '#'+ section.replace('/', '')), timing, {offset: offset} );
		}
		
		/*
		if(subsection){
			var params = new Object();
					params.archive = archive;
			gApplication.refreshView('beerlist', params);
		}
		*/
	}
	
}