<?php
	
if ( function_exists( 'add_theme_support' ) ) {
	set_post_thumbnail_size( 150, 150, true ); // Normal post thumbnails
	add_theme_support( 'automatic-feed-links' );
}

// Custom Post Type
add_action('init', 'beer_register');
 
function beer_register() {
 
	$labels = array(
		'name' => _x('Beer List', 'post type general name'),
		'singular_name' => _x('Beer', 'post type singular name'),
		'add_new' => _x('Add New', 'beer'),
		'add_new_item' => __('Add New Beer'),
		'edit_item' => __('Edit Beer'),
		'new_item' => __('New Beer'),
		'view_item' => __('View Beer'),
		'search_items' => __('Search Beer'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		//'supports' => array('title','editor','thumbnail')
		'supports' => array('title','editor','author','thumbnail'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'beer' , $args );
	register_taxonomy("Style", array("beer"), array("hierarchical" => true, "label" => "Style", "singular_label" => "Style", "rewrite" => true));

}

add_action("manage_posts_custom_column",  "beer_custom_columns");
add_filter("manage_edit-beer_columns", "beer_edit_columns");
 
function beer_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
    "title" => "Beer Name",
    "abv" => "ABV", 
    "ibu" => "IBU",
    "description" => "Description",
  );
 
  return $columns;
}

function ttruncat($text,$numb) {
if (strlen($text) > $numb) { 
  $text = substr($text, 0, $numb); 
  $text = substr($text,0,strrpos($text," ")); 
  $etc = " ...";  
  $text = $text.$etc; 
  }
return $text; 
}

function beer_custom_columns($column){
  global $post;
 
  switch ($column) {
    case "description":
      echo ttruncat(get_field('description'), 100);
      break;
    case "abv":
      $custom = get_post_custom();
      echo get_field('abv', $post->ID);
      break;
    case "ibu":
      $custom = get_post_custom();
       echo get_field('ibu', $post->ID);
      break;
  }
}


// User Post Type
add_action('init', 'profiles_register');
 
function profiles_register() {
 
	$labels = array(
		'name' => _x('Profiles', 'post type general name'),
		'singular_name' => _x('Profile', 'post type singular name'),
		'add_new' => _x('Add New', 'profile'),
		'add_new_item' => __('Add New Profile'),
		'edit_item' => __('Edit Profile'),
		'new_item' => __('New Profile'),
		'view_item' => __('View Profile'),
		'search_items' => __('Search Profiles'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 12,
		//'supports' => array('title','editor','thumbnail')
		'supports' => array('title','author'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'profiles' , $args );

}


// Locations Post Type
add_action('init', 'locations_register');
 
function locations_register() {
 
	$labels = array(
		'name' => _x('Locations', 'post type general name'),
		'singular_name' => _x('Location', 'post type singular name'),
		'add_new' => _x('Add New', 'location'),
		'add_new_item' => __('Add New Location'),
		'edit_item' => __('Edit Location'),
		'new_item' => __('New Location'),
		'view_item' => __('View Location'),
		'search_items' => __('Search Locations'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/callout16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 8,
		//'supports' => array('title','editor','thumbnail')
		'supports' => array('title','thumbnail'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'locations' , $args );
	register_taxonomy("Region", array("locations"), array("hierarchical" => false, "label" => "Region", "singular_label" => "Region", "rewrite" => true));
	register_taxonomy("Account", array("locations"), array("hierarchical" => true, "label" => "Account Type", "singular_label" => "Account", "rewrite" => true));

}


// Jobs Post Type
add_action('init', 'jobs_register');
 
function jobs_register() {
 
	$labels = array(
		'name' => _x('Jobs', 'post type general name'),
		'singular_name' => _x('Job', 'post type singular name'),
		'add_new' => _x('Add New', 'job'),
		'add_new_item' => __('Add New Job'),
		'edit_item' => __('Edit Job'),
		'new_item' => __('New Job'),
		'view_item' => __('View Job'),
		'search_items' => __('Search Jobs'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/callout16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 9,
		//'supports' => array('title','editor','thumbnail')
		'supports' => array('title','thumbnail'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'jobs' , $args );

}


// Press Post Type
add_action('init', 'press_register');
 
function press_register() {
 
	$labels = array(
		'name' => _x('Press', 'post type general name'),
		'singular_name' => _x('Press', 'post type singular name'),
		'add_new' => _x('Add New', 'press'),
		'add_new_item' => __('Add New Press'),
		'edit_item' => __('Edit Press'),
		'new_item' => __('New Press'),
		'view_item' => __('View Press'),
		'search_items' => __('Search Press'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/callout16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 9,
		//'supports' => array('title','editor','thumbnail')
		'supports' => array('title','thumbnail'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'press' , $args );

}

// Locations Post Type
add_action('init', 'team_register');
 
function team_register() {
 
	$labels = array(
		'name' => _x('Team', 'post type general name'),
		'singular_name' => _x('Team', 'post type singular name'),
		'add_new' => _x('Add New', 'team'),
		'add_new_item' => __('Add New Team'),
		'edit_item' => __('Edit Team'),
		'new_item' => __('New Team'),
		'view_item' => __('View Team'),
		'search_items' => __('Search Team'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => '',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 8,
		//'supports' => array('title','editor','thumbnail')
		'supports' => array('title','thumbnail'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'team' , $args );
	register_taxonomy("Title", array("team"), array("hierarchical" => true, "label" => "Job Title", "singular_label" => "Job Title", "rewrite" => true));

}

// Locations Post Type
add_action('init', 'guestbrewer_register');
 
function guestbrewer_register() {
 
	$labels = array(
		'name' => _x('Guest Brewers', 'post type general name'),
		'singular_name' => _x('Guest Brewer', 'post type singular name'),
		'add_new' => _x('Add New', 'guestbrewer'),
		'add_new_item' => __('Add New Guest Brewer'),
		'edit_item' => __('Edit Guest Brewer'),
		'new_item' => __('New Guest Brewer'),
		'view_item' => __('View Guest Brewer'),
		'search_items' => __('Search Guest Brewers'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => '',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 8,
		//'supports' => array('title','editor','thumbnail')
		'supports' => array('title','thumbnail'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'guestbrewer' , $args );

}

// Locations Post Type
add_action('init', 'workshops_register');
 
function workshops_register() {
 
	$labels = array(
		'name' => _x('Events', 'post type general name'),
		'singular_name' => _x('Event', 'post type singular name'),
		'add_new' => _x('Add New', 'workshops'),
		'add_new_item' => __('Add New Event'),
		'edit_item' => __('Edit Event'),
		'new_item' => __('New Event'),
		'view_item' => __('View Event'),
		'search_items' => __('Search Events'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => '',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 8,
		//'supports' => array('title','editor','thumbnail')
		'supports' => array('title','thumbnail'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'workshops' , $args );

}


/********/


function my_acf_save_post( $post_id ) {
    
    // bail early if no ACF data
    if( empty($_POST['fields']) ) {
        
        return;
        
    }
    
    if( get_post_type( $post_id ) == "locations" ){
    
	    // array of field values
	    $fields = $_POST['fields'];
	
	    // specific field value
	    $address = $_POST['fields']['field_5457c5b32dd4a'];
	    $city = $_POST['fields']['field_5457c5c62dd4b'];
	    $state = $_POST['fields']['field_5457c5d32dd4c'];
	    $zip = $_POST['fields']['field_5457c6542dd4d'];
	    
	    function lookup($string){
				 
				$string = str_replace (" ", "+", urlencode($string));
				$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $details_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = json_decode(curl_exec($ch), true);
				
				// If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
				if ($response['status'] != 'OK') {
				return null;
				}
				
				//print_r($response);
				$geometry = $response['results'][0]['geometry'];
				
				$latitude = $geometry['location']['lat'];
				$longitude = $geometry['location']['lng'];
				
				$array = array(
				    'latitude' => $latitude,
				    'longitude' => $longitude,
				);
				
				//$geo = $latitude . ',' . $longitude;
				
				update_field( "field_5457d188bdf4b", $latitude, $post_id ); // lat
				update_field( "field_5457d192bdf4c", $longitude, $post_id ); // lng
				
				//return $array;
				return $geo;
				 
			}
				 
			$array = lookup($address . '+' . $city . '+' . $state . '+' . $zip);	  
		  //$coords = explode(",", $array);
		  
		  /*
		  if(isset($address)){
			  echo '<div class="map" style="display: block; position: absolute; top: 0px; right: 14px;"><img src="http://maps.googleapis.com/maps/api/staticmap?size=212x212&center='. $address . '+' . $city . '+' . $state . '+' . $zip .'&zoom=15&scale=1&sensor=false"></div>';
		  }
		  */
	  
	  }
    
}

// run before ACF saves the $_POST['acf'] data
add_action('acf/save_post', 'my_acf_save_post', 20);


/*********/


add_action("manage_posts_custom_column",  "locations_custom_columns");
add_filter("manage_edit-locations_columns", "locations_edit_columns");
 
function locations_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
    "title" => "Title",
    "address" => "Address",
    "region" => "Region",
    "account" => "Account Type",
  );
 
  return $columns;
}
function locations_custom_columns($column){
  global $post;
 
  switch ($column) {
    case "title":
      the_title();
      break;
    case "address":
      $custom = get_post_custom();
      echo $custom["address"][0] . '<br>' . $custom["city"][0] . ', ' . $custom["state"][0] . ' ' . $custom["zip"][0];
      break;
    case "region":
      echo get_the_term_list($post->ID,'Region','',', ','');
      break;
    case "account":
      echo get_the_term_list($post->ID,'Account','',', ','');
  }
}


// Distributors Post Type
add_action('init', 'distributors_register');
 
function distributors_register() {
 
	$labels = array(
		'name' => _x('Distributors', 'post type general name'),
		'singular_name' => _x('Distributor', 'post type singular name'),
		'add_new' => _x('Add New', 'asset'),
		'add_new_item' => __('Add New Asset'),
		'edit_item' => __('Edit Asset'),
		'new_item' => __('New Asset'),
		'view_item' => __('View Asset'),
		'search_items' => __('Search Assets'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/callout16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail'),
		//'supports' => array('title','thumbnail'),
    //'taxonomies' => array('post_tag')
    //'taxonomies' => array('category')
	  ); 
 
	register_post_type( 'distributors' , $args );
	register_taxonomy("Type", array("distributors"), array("hierarchical" => true, "label" => "Type", "singular_label" => "Type", "rewrite" => true));

}

add_action("admin_init", "admin_init_6");
 
function admin_init_6(){
	add_meta_box("upload-meta", "Attachments", "upload_meta", "distributors", "normal", "low");
}

function upload_meta(){
  global $post;
  $custom = get_post_custom($post->ID);
  
  $rows = get_field('files', $post->ID);
	if($rows)
	{
		foreach($rows as $row)
		{
			$valid_ext = array("tif", "pdf", "psd", "zip", "png", "tiff", "ai", "eps");
			$ext = pathinfo($row['file']['url'], PATHINFO_EXTENSION);
  		if(in_array($ext, $valid_ext)) {
        echo '<li><a href="'.$row['file']['url'].'">';
        echo strtoupper($ext);
        echo '</a></li>';
      }
      
		}
	}
}

add_action("manage_posts_custom_column",  "distributors_custom_columns");
add_filter("manage_edit-distributors_columns", "distributors_edit_columns");
 
function distributors_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
    "title" => "Title",
    "attachments" => "Attachments",
  );
 
  return $columns;
}
function distributors_custom_columns($column){
  global $post;
 
  switch ($column) {
    case "title":
      the_title();
      break;
    case "attachments":
      $args = array(
			    'post_type' => 'attachment',
			    'numberposts' => null,
			    'post_status' => null,
			    'post_parent' => $post->ID
			);
			$valid_ext = array("tif", "pdf", "psd", "zip", "png", "tiff", "ai", "eps");
			$rows = get_field('files', $post->ID);
			if($rows)
			{
				foreach($rows as $row)
				{
					
					$ext = pathinfo($row['file']['url'], PATHINFO_EXTENSION);
	    		if(in_array($ext, $valid_ext)) {
		        echo '<li><a href="'.$row['file']['url'].'">';
	          echo strtoupper($ext);
	          echo '</a></li>';
          }
          
				}
			}
      break;
  }
}

$result = add_role( 'distributor', __('Distributor'), array(	 
	'read' => true, // true allows this capability
	'edit_posts' => false, // Allows user to edit their own posts
	'edit_pages' => false, // Allows user to edit pages
	'edit_others_posts' => false, // Allows user to edit others posts not just their own
	'create_posts' => false, // Allows user to create new posts
	'manage_categories' => false, // Allows user to manage post categories
	'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
	'edit_themes' => false, // false denies this capability. User can’t edit your theme
	'install_plugins' => false, // User cant add new plugins
	'update_plugin' => false, // User can’t update any plugins
	'update_core' => false // user cant perform core updates
));

/********/

// Added to extend allowed files types in Media upload
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {

// Add *.EPS files to Media upload
$existing_mimes['eps'] = 'application/postscript';

// Add *.AI files to Media upload
$existing_mimes['ai'] = 'application/postscript';

// Add *.PSD files to Media upload
$existing_mimes['psd'] = 'image/photoshop' || 'image/x-photoshop' || 'image/x-photoshop' || 'image/psd' || 'application/photoshop' || 'application/psd';

return $existing_mimes;
}

/**********/

function login_with_email_address($username) {
	$user = get_user_by_email($username);
	if(!empty($user->user_login))
		$username = $user->user_login;
	return $username;
}
add_action('wp_authenticate','login_with_email_address');

// AJAX Login
function ajaxLogin(){

	//get data from our ajax() call
	$protocol = $_GET['protocol'];
	$callback = $_GET['callback'];
	
	if($protocol == 'fb'){
		
		// Using FB to login
		$fb_uid = $_GET['user_login'];
		$fb_email = $_GET['user_password'];
		
		//See if we were given permission to access the user's email
		//This isn't required, and will only matter if it's a new user without an existing WP account
		//(since we'll auto-register an account for them, using the contact_email we get from Facebook - if we can...)
		$userRevealedEmail = false;
		if( strlen($fb_email) != 0 && strpos($fb_email, 'proxymail.facebook.com') === FALSE )
		{
		    $userRevealedEmail = true;
		}
		else if( strlen($fb_email) != 0 )
		{
		}
		else
		{
		    $fb_email = "FB_" . $fb_uid . '@unknown.com';
		}
		
		global $wpdb, $blog_id;
			if ( empty($id) ) $id = (int) $blog_id;
			$blog_prefix = $wpdb->get_blog_prefix($id);
			$sql = "SELECT user_id, user_id AS ID, user_login, display_name, user_email, meta_value ".
				     "FROM $wpdb->users, $wpdb->usermeta ".
				     "WHERE {$wpdb->users}.ID = {$wpdb->usermeta}.user_id AND meta_key = '{$blog_prefix}capabilities' ".
				     "AND {$wpdb->users}.ID IN (SELECT user_id FROM {$wpdb->usermeta} WHERE meta_key = 'facebook_uid' AND meta_value = '$fb_uid')"; 
			$wp_users = $wpdb->get_results( $sql );
			
			foreach ($wp_users as $wp_user)
			{
			    $meta_uid  = get_user_meta($wp_user->ID, 'facebook_uid', true);
			    if( $meta_uid && $meta_uid == $fb_uid )
			    {
			        $user_data       = get_userdata($wp_user->ID);
			        $user_login_id   = $wp_user->ID;
			        $user_login_name = $user_data->user_login;
			        break;
			    }
			}
			
		//Next, try to lookup their email directly (via Wordpress).  Obviously this will only work if they've revealed
		//their "real" address (vs denying access, or changing it to a "proxy" in the popup)
		if ( !$user_login_id && $userRevealedEmail )
		{
		    if ( $wp_user = get_user_by('email', $fb_email) )
		    {
		        $user_login_id = $wp_user->ID;
		        $user_data = get_userdata($wp_user->ID);
		        $user_login_name = $user_data->user_login;
		    }
		}
		
		//If we found an existing user, check if they'd previously denied access to their email but have now allowed it.
		//If so, we'll want to update their WP account with their *real* email.
		if( $user_login_id )
		{
		    //Check 1: It was previously denied, but is now allowed
		    $updateEmail = false;
		    if( strpos($user_data->user_email, '@unknown.com') !== FALSE && strpos($fb_email, '@unknown.com') === FALSE )
		    {
		        $updateEmail = true;
		    }
		    //Check 2: It was previously allowed, but only as an anonymous proxy.  They've now revealed their "true" email.
		    if( strpos($user_data->user_email, "@proxymail.facebook.com") !== FALSE && strpos($fb_email, "@proxymail.facebook.com") === FALSE )
		    {
		        $updateEmail = true;
		    }
		    if( $updateEmail )
		    {
		        $user_upd = array();
		        $user_upd['ID']         = $user_login_id;
		        $user_upd['user_email'] = $fb_email;
		        wp_update_user($user_upd);
		    }
		    
		    //Run a hook when an existing user logs in
		    do_action('wpfb_existing_user', array('WP_ID' => $user_login_id, 'FB_ID' => $fb_uid, 'facebook' => $facebook, 'WP_UserData' => $user_data) );
		}
		
		$pageContent = file_get_contents('https://graph.facebook.com/v2.0?id='.$fb_uid.'&access_token=1530679393856403|0ab7aaf4c67976bdc08249fb272daa30');
		$parsedJson  = json_decode($pageContent);
		$usersFirstLastName   = $parsedJson->first_name . ' ' . $parsedJson->last_name;
		
		wp_update_user( array( 
    	'ID' => $user_login_id, 
    	'first_name' => $parsedJson->first_name,
    	'last_name' => $parsedJson->last_name,
    	'display_name' => $usersFirstLastName,
    	'user_email' => $parsedJson->email  
    ));
		
		
		//If we still don't have a user_login_id, the FB user who's logging in has never been to this blog.
		//We'll auto-register them a new account.  Note that if they haven't allowed email permissions, the
		//account we register will have a bogus email address (but that's OK, since we still know their Facebook ID)
		if( !$user_login_id )
		{
		    $user_data = array();
		    $user_data['user_login']    = "FB_" . $fb_uid;
		    $user_data['user_pass']     = wp_generate_password();
		    $user_data['user_nicename'] = sanitize_title($user_data['user_login']);
		    $user_data['first_name']    = $fbuser['first_name'];
		    $user_data['last_name']     = $fbuser['last_name'];
		    $user_data['display_name']  = $fbuser['first_name'];
		    $user_data['user_url']      = $fbuser["profile_url"];
		    $user_data['user_email']    = $fbuser["email"];
		    
		    //Run a filter so the user can be modified to something different before registration
		    //NOTE: If the user has selected "pretty names", this'll change FB_xxx to i.e. "John.Smith"
		    $user_data = apply_filters('wpfb_insert_user', $user_data, $fbuser );
		    $user_data = apply_filters('wpfb_inserting_user', $user_data, array('WP_ID' => $user_login_id, 'FB_ID' => $fb_uid, 'facebook' => $facebook, 'FB_UserData' => $fbuser) );
		    
		    //Insert a new user to our database and make sure it worked
		    $user_login_id   = wp_insert_user($user_data);
		    if( is_wp_error($user_login_id) )
		    {
		        j_die("Error: wp_insert_user failed!<br/><br/>".
		              "If you get this error while running a Wordpress MultiSite installation, it means you'll need to purchase the <a href=\"$jfb_homepage#premium\">premium version</a> of this plugin to enable full MultiSite support.<br/><br/>".
		              "If you're <u><i>not</i></u> using MultiSite, please report this bug to the plugin author on the support page <a href=\"$jfb_homepage#feedback\">here</a>.<br /><br />".
		              "Error message: " . (function_exists(array(&$user_login_id,'get_error_message'))?$user_login_id->get_error_message():"Undefined") . "<br />".
		              "WP_ALLOW_MULTISITE: " . (defined('WP_ALLOW_MULTISITE')?constant('WP_ALLOW_MULTISITE'):"Undefined") . "<br />".
		              "is_multisite: " . (function_exists('is_multisite')?is_multisite():"Undefined"));
		    }
		    
		    $pageContent = file_get_contents('https://graph.facebook.com/v2.0?id='.$fb_uid.'&access_token=1530679393856403|0ab7aaf4c67976bdc08249fb272daa30');
				$parsedJson  = json_decode($pageContent);
				$usersFirstLastName   = $parsedJson->first_name . ' ' . $parsedJson->last_name;
				
				wp_update_user( array( 
		    	'ID' => $user_login_id, 
		    	'first_name' => $parsedJson->first_name,
		    	'last_name' => $parsedJson->last_name,
		    	'display_name' => $usersFirstLastName,
		    	'user_email' => $parsedJson->email  
		    ));
		    
		    //Success! Notify the site admin.
		    $user_login_name = $user_data['user_login'];
		    wp_new_user_notification($user_login_id);
		    
		    //Run an action so i.e. usermeta can be added to a user after registration
		    do_action('wpfb_inserted_user', array('WP_ID' => $user_login_id, 'FB_ID' => $fb_uid, 'facebook' => $facebook, 'WP_UserData' => $user_data) );
		}
		
		//Tag the user with our meta so we can recognize them next time, without resorting to email hashes
		update_user_meta($user_login_id, "facebook_uid", $fb_uid);
		
		//Also store the user's facebook avatar(s), in case the user wants to use them later
		$avatarThumb = 'https://graph.facebook.com/v2.0/picture?id='.$fb_uid.'&access_token=1530679393856403|0ab7aaf4c67976bdc08249fb272daa30&type=square';
		$avatarFull = 'https://graph.facebook.com/v2.0/picture?id='.$fb_uid.'&access_token=1530679393856403|0ab7aaf4c67976bdc08249fb272daa30&type=large';
		
		if( $avatarThumb )
		{
			update_user_meta($user_login_id, 'facebook_avatar_full', $avatarFull);
			update_user_meta($user_login_id, 'facebook_avatar_thumb', $avatarThumb);
		}
		else
		{
		  update_user_meta($user_login_id, 'facebook_avatar_thumb', '');
		  update_user_meta($user_login_id, 'facebook_avatar_full', '');
		}
		
		//Log them in
		$rememberme = apply_filters('wpfb_rememberme', isset($_POST['rememberme'])&&$_POST['rememberme']);
		wp_set_auth_cookie( $user_login_id, $rememberme );
		
		//Run a custom action.  You can use this to modify a logging-in user however you like,
		//i.e. add them to a "Recent FB Visitors" log, assign a role if they're friends with you on Facebook, etc.
		do_action('wpfb_login', array('WP_ID' => $user_login_id, 'FB_ID' => $fb_uid, 'facebook' => $facebook) );
		do_action('wp_login', $user_login_name);
		
		$pageContent = file_get_contents('https://graph.facebook.com/v2.0?id='.$fb_uid.'&access_token=1530679393856403|0ab7aaf4c67976bdc08249fb272daa30');
		$parsedJson  = json_decode($pageContent);
		$usersFirstLastName   = $parsedJson->first_name . ' ' . $parsedJson->last_name;
		
		wp_update_user( array( 
    	'ID' => $user_login_id, 
    	'first_name' => $parsedJson->first_name,
    	'last_name' => $parsedJson->last_name,
    	'display_name' => $usersFirstLastName,
    	'user_email' => $parsedJson->email  
    ));
		
		$output = array(
			'status' => 'success',
		);
		
		$output['data'] = array(
			'userid' => $user_login_id,
			'username' => $usersFirstLastName,
			'first_name' => $parsedJson->first_name,
			'last_name' => $parsedJson->last_name,
			'avatar' => $avatarFull,
		);
		
		header("Content-type: application/javascript");
		//die(json_encode($output));
		
		if(isset($callback) && !empty($callback)){
			$jsonp = json_encode($output);
			die( $callback.'('.$jsonp.')' );
		}
		else {
			die( json_encode($output) );
		}
		
		//echo $user_login_id .','. $usersFirstLastName .','. $avatarThumb;
	
	} else if($protocol == 'tw'){
			
		// Twitter login stuff would go here	
			
	} else {
	
		// Using WP to login
		$user_login = $_GET['user_login'];
		$user_password = $_GET['user_password'];
		
		$creds = array();
		$creds['user_login'] = $user_login;
		$creds['user_password'] = $user_password;
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
		
		$user_id = $user->ID;
		$username = $user->display_name;
		
		function get_wp_avatar_url($get_avatar){
		  preg_match("/src='(.*?)'/i", $get_avatar, $matches);
		  return $matches[1];
		}
		$userimg = get_wp_avatar_url(get_avatar( $user_id, 100 ));
		
		if ( is_wp_error($user) ) {
			echo $user->get_error_message();
			die($user->get_error_message());
			
			$output = array(
				'status' => 'fail',
				'error' => $user->get_error_message(),
			);
			
			header("Content-type: application/javascript");
			
			if(isset($callback) && !empty($callback)){
				$jsonp = json_encode($output);
				die( $callback.'('.$jsonp.')' );
			}
			else {
				die( json_encode($output) );
			}
		}
		else {
			wp_set_auth_cookie( $user_id, 0, 0);
			//die('success');
			
			$output = array(
				'status' => 'success',
			);
			
			$output['data'] = array(
				'userid' => $user_id,
				'username' => $username,
				'avatar' => str_replace("&amp;", "&", urldecode($userimg)),
			);
			
			header("Content-type: application/javascript");
			//die(json_encode($output));
			
			if(isset($callback) && !empty($callback)){
				$jsonp = json_encode($output);
				die( $callback.'('.$jsonp.')' );
			}
			else {
				die( json_encode($output) );
			}
		}
	
	}
		
}

// create custom Ajax call for WordPress
add_action( 'wp_ajax_nopriv_ajaxLogin', 'ajaxLogin' );
add_action( 'wp_ajax_ajaxLogin', 'ajaxLogin' );


// AJAX Logout
function ajaxLogout(){
	
	$callback = $_GET['callback'];

	wp_logout();
	
	$output = array(
		'status' => 'success'
	);
	
	header("Content-type: application/javascript");
	//die(json_encode($output));
	
	if(isset($callback) && !empty($callback)){
		$jsonp = json_encode($output);
		die( $callback.'('.$jsonp.')' );
	}
	else {
		die( json_encode($output) );
	}
		
}

// create custom Ajax call for WordPress
add_action( 'wp_ajax_nopriv_ajaxLogout', 'ajaxLogout' );
add_action( 'wp_ajax_ajaxLogout', 'ajaxLogout' );


// AJAX Register
function ajaxRegister(){
	
	$callback = $_GET['callback'];

	//get data from our ajax() call
	$user_login = trim($_GET['user_login']);
	$user_email = trim($_GET['user_email']);
	$user_pass = $_GET['user_password'];
	$user_pass_confirm = $_GET['user_password_confirm'];
	$honeypot = $_POST['honeypot'];
	
	if ( $honeypot != '' ) {
		// Stop spam registrations	    
	 	//die("spam"); 
	 	$output = array(
			'status' => 'fail',
			'error' => 'spam',
		);
		
		header("Content-type: application/json");
		die(json_encode($output));	    
	} 
	else {
	
		//initialize a WP_Errror object
		$errors = new WP_Error();
		
		//username is required
		if (  isset($user_login) && $user_login == '' )
		  $errors->add('login_required', __('The username field is required. '));
		  
		//email is required
		if (  isset($user_email) && $user_email == '' )
		  $errors->add('email_required', __('The email field is required. '));
		  
		//email needs to be a valid email
		if (  isset($user_email) && !is_email($user_email) )
		  $errors->add('email_invalid', __('The email is not valid. '));
		  
		//passwords must match
		if ( ($user_pass != '' && $user_pass_confirm != '') && $user_pass != $user_pass_confirm )
		  $errors->add('password_mismatch', __('The passwords do not match. '));
		  
		//password is required
		if (  isset($user_pass) && $user_pass == '' )
		   $errors->add('password_required', __('The password is required. '));
		   
		//confirm password is required
		if (  isset($user_pass_confirm) && $user_pass_confirm == '' )
		  $errors->add('password_confirm_required', __('The confirm password field is required. '));
		  
		//if we don't have any errors lets try to insert the user
		if(!$errors->get_error_codes())
		   $errors = $user_id = wp_insert_user(array('user_login' => $user_login, 'user_email' => $user_email, 'user_pass' => $user_pass));
		   
		//if we still do not have any errors it was a success
		if (!is_wp_error($errors)) {
		  $user_reg = 'success';
		  
		  // Log them in
		  wp_set_auth_cookie( $user_id, true );	  
		  //echo 'success';
		  
		  $output = array(
				'status' => 'success',
			);
			
			// Send confirmation mail
			$subject = "Sign Up Confirmation";
			$body = "
		  	<h1 style=\"color:#022c37;margin:0;line-height:1.1em;font-size:30px; padding-bottom:10px;\">Thanks for signing up!</h1>
		    <p>For future reference, your username is: ". $user_login ."</p>
		    <div style=\"margin-top:30px;\">
		    	<p>Cheers,<br>
				  Local Brewing Co.</p>
		    </div>";
		    
			add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
			wp_mail($user_email, $subject, $body);
			
			// Output ID for front end use
			$output['data'] = array(
				'userid' => $user_id,
			);
			
			header("Content-type: application/javascript");
			die(json_encode($output));
		  
		} else{ //output the errors
			$user_reg = 'fail';
		
		  foreach($errors->errors as $code => $error){
				//echo $error[0];
				$output = array(
					'status' => 'fail',
					'error' => $error[0]
				);
				
				header("Content-type: application/javascript");
				die(json_encode($output));
		  }
		  
		}
	}
	 
}

add_action( 'wp_ajax_nopriv_ajaxRegister', 'ajaxRegister' );
add_action( 'wp_ajax_ajaxRegister', 'ajaxRegister' );


// AJAX forgot password
function ajaxForgotPassword(){

	global $wpdb;
	$username = trim($_POST['user_input']);
	$user_exists = false;
	$error = "";
	
	// First check by username
	if ( username_exists( $username ) ){
    $user_exists = true;
    $user = get_user_by('login', $username);
	}
	// Then, by e-mail address
	elseif( email_exists($username) ){
    $user_exists = true;
    $user = get_user_by_email($username);
	} 
	
	else {
	  $error = $username .' was not found, try again!';
	}
	
	if ($user_exists){
    $user_login = $user->user_login;
    $user_email = $user->user_email;

    $key = $wpdb->get_var($wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login));
    
    if ( empty($key) ) {
        // Generate something random for a key...
        $key = wp_generate_password(20, false);
        do_action('retrieve_password_key', $user_login, $key);
        
        // Now insert the new md5 key into the db
        $wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));
    }

    //create email message
    $subject = "Password Reset";
    $body = "
    	<h1 style=\"color:#022c37;margin:0;line-height:1.1em;font-size:30px; padding-bottom:10px;\">Password Reset</h1>
    	<p>Someone has asked to reset the password for the following username: <strong>". sprintf(__('%s'), $user_login) ."</strong></p>
    	<p>To reset your password, visit the following address, otherwise just ignore this email and nothing will happen.</p>
    	<p>". home_url() . "?reset=true&key=$key&u=" . rawurlencode($user_login) ."</p>
    	<div style=\"margin-top:30px;\">
      	<p>Cheers,<br>
			  Local Brewing Co.</p>
      </div>";
						      
		add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
		wp_mail($user_email, $subject, $body);
	}
	
	if (!empty($error)){
		echo $error;
	}
	
	else {
		echo 'success'; 
	}
	
}

add_action( 'wp_ajax_nopriv_ajaxForgotPassword', 'ajaxForgotPassword' );
add_action( 'wp_ajax_ajaxForgotPassword', 'ajaxForgotPassword' );


// AJAX reset password
function ajaxResetPassword(){

	global $wpdb, $user_ID;

	if (!$user_ID) { // block logged in users
	
		if ( !wp_verify_nonce( $_POST['pwd_nonce'], "pwd_nonce")) {
		  exit("C'mon now...");
		}
	
		if( isset($_POST['key']) && isset($_POST['reset']) == "true" ) {
			$reset_key = $_POST['key'];
			$user_login = $_POST['u'];
			$password = $_POST['user_password'];
			$password_confirm = $_POST['user_password_confirm'];			
			$success = false;
			
			$user_data = $wpdb->get_row($wpdb->prepare("SELECT ID, user_login, user_email FROM $wpdb->users WHERE user_activation_key = %s AND user_login = %s", $reset_key, $user_login));
			
			$user_login = $user_data->user_login;
			$user_email = $user_data->user_email;
			
			if(!empty($reset_key) && !empty($user_data)) {
			
				if(empty($password) || empty($password_confirm)) { 
					$error = 'Password field is required.';
					echo $error; 
				} 
				else if($password !== $password_confirm){
					$error = 'Password does not match.';
					echo $error;
				} 
				else {			
					// generate new password
					//$new_password = wp_generate_password(7, false);
					
					// set new password
					wp_set_password( $password, $user_data->ID );
					
					// Log them in
					wp_set_auth_cookie( $user_data->ID, true );
					$success = true;				
				}
				
				// mailing reset details to the user
				//create email message
				if($success == true){
			    $subject = "Password Reset Complete";
			    $body = "
			    	<h1 style=\"color:#022c37;margin:0;line-height:1.1em;font-size:30px; padding-bottom:10px;\">Hi, ". sprintf(__('%s'), $user_login) ."</h1>
			    	<p>Your password has been reset.</p>
			    	<div style=\"margin-top:30px;\">
			      	<p>Cheers,<br>
			      	Local Brewing Co.</p>
			      </div>";
									      
					add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
					wp_mail($user_email, $subject, $body);
					
					echo "success";
					exit();
				}
			} 
			else exit('Not a Valid Key.');
			
		}
			
	}
	
}

add_action( 'wp_ajax_nopriv_ajaxResetPassword', 'ajaxResetPassword' );
add_action( 'wp_ajax_ajaxResetPassword', 'ajaxResetPassword' );

/*******/


function addFavorite(){
	
		$current_user = wp_get_current_user();

    //get data from our ajax() call
    $post_id = $_GET['beer_id'];
    $user_id = $_GET['user_id'];
    $callback = $_GET['callback'];
    
    $user_info = get_userdata($user_id);
    
    if( isset($user_id) ){
    	wp_set_current_user( $user_id );
    }
    
    if (defined('COAUTHORS_PLUS_VERSION')) {
		
			if ( isset($user_id) ) {
				
				//Check if they have requested to be an author

				if (isset($post_id) && is_numeric($post_id) && $post_id > 0) {
	
					if (!is_coauthor_for_post($user_info->user_login, $post_id)) {
	
						$cAuthors = array();
	
						foreach (get_coauthors($post_id) as $theAuthor) {
	
							$cAuthors[] = $theAuthor->user_login;
	
						}
	
						$cAuthors[] = $user_info->user_login;
	
						$cObj = new coauthors_plus();
	
						$cObj->add_coauthors($post_id, $cAuthors);
	
						$output = array(
							"status" => "added",
							"user" => $user_info->user_login
						);
	
					}
					else {
						$output = array(
							"status" => "exists"
						);
					}
					
				}
		
			}
			else {
				$output = array(
					"status" => "not logged in"
				);
			}
		
		}

    // Return String    
    header("Content-type: application/javascript");
		
		if(isset($callback) && !empty($callback)){
			$jsonp = json_encode($output);
			die( $callback.'('.$jsonp.')' );
		}
		else {
			die( json_encode($output) );
		}
}

add_action( 'wp_ajax_nopriv_addFavorite', 'addFavorite' );
add_action( 'wp_ajax_addFavorite', 'addFavorite' );


function removeFavorite(){

    $current_user = wp_get_current_user();

    //get data from our ajax() call
    $post_id = $_GET['beer_id'];
    $user_id = $_GET['user_id'];
    $callback = $_GET['callback'];
    
    $user_info = get_userdata($user_id);
    
    if( isset($user_id) ){
    	wp_set_current_user( $user_id );
    }
    
    if (defined('COAUTHORS_PLUS_VERSION')) {
		
			global $current_user, $post;
		
			if ( isset($user_id) ) {
				
				if (isset($post_id)){
					foreach (get_coauthors($post_id) as $theAuthor) {
		
						$cAuthors[] = $theAuthor->user_login;
		
					}
					
					while (($userkey = array_search($user_info->user_login, $cAuthors)) !== false) {
						unset($cAuthors[$userkey]);
					}
		
					//print_r($cAuthors);
					
					$cObj = new coauthors_plus();
		
					$cObj->add_coauthors($post_id, $cAuthors);
					
					$output = array(
						"status" => "removed"
					);
					
				}
		
			}
		
		}

    // Return String    
    header("Content-type: application/javascript");
		
		if(isset($callback) && !empty($callback)){
			$jsonp = json_encode($output);
			die( $callback.'('.$jsonp.')' );
		}
		else {
			die( json_encode($output) );
		}
}

// create custom Ajax call for WordPress
add_action( 'wp_ajax_nopriv_removeFavorite', 'removeFavorite' );
add_action( 'wp_ajax_removeFavorite', 'removeFavorite' );


// AJAX addProfile
function ajaxAddProfile(){
	
	$callback = $_GET['callback'];
	$profile_name = $_GET['profile_name'];
	$author_id = $_GET['author_id'];
	
	$bitterness = $_GET['bitterness'];
	$alcohol = $_GET['alcohol'];
	$complexity = $_GET['complexity'];

	$post = array(
		'post_type' => 'profiles',
	  'post_title' => $profile_name,
	  'post_author' => $author_id,
	  'post_status' => 'publish'
	);
	
	// Insert the post into the database	
	$post_id = wp_insert_post( $post, $wp_error );
	
	// update meta fields here
	update_field( "field_5462bcc0f9b64", $bitterness, $post_id ); // bitterness
	update_field( "field_5462bd3a15f64", $alcohol, $post_id ); // alcohol
	update_field( "field_5462bd4515f65", $complexity, $post_id ); // complexity
	
	if(isset($post_id)){
		$output = array(
			"status" => "success"
		);
	}
	else {
		$output = array(
			"status" => "fail"
		);
	}

  // Return String    
  header("Content-type: application/javascript");
	
	if(isset($callback) && !empty($callback)){
		$jsonp = json_encode($output);
		die( $callback.'('.$jsonp.')' );
	}
	else {
		die( json_encode($output) );
	}
		
}

// create custom Ajax call for WordPress
add_action( 'wp_ajax_nopriv_ajaxAddProfile', 'ajaxAddProfile' );
add_action( 'wp_ajax_ajaxAddProfile', 'ajaxAddProfile' );


// AJAX addProfile
function ajaxRemoveProfile(){
	
	$callback = $_GET['callback'];
	$profile_id = $_GET['profile_id'];
	
	// Insert the post into the database	
	wp_delete_post( $profile_id, true );
	
	$output = array(
		"status" => "success"
	);

  // Return String    
  header("Content-type: application/javascript");
	
	if(isset($callback) && !empty($callback)){
		$jsonp = json_encode($output);
		die( $callback.'('.$jsonp.')' );
	}
	else {
		die( json_encode($output) );
	}
		
}

// create custom Ajax call for WordPress
add_action( 'wp_ajax_nopriv_ajaxRemoveProfile', 'ajaxRemoveProfile' );
add_action( 'wp_ajax_ajaxRemoveProfile', 'ajaxRemoveProfile' );


// AJAX Onboard
function ajaxOnboard(){
	
	$callback = $_GET['callback'];
	$user_id = $_GET['user_id'];
	
	$isOnboard = "true";
	update_user_meta($user_id, 'lbc_onboard', $isOnboard);
	
	$output = array(
		"status" => "success"
	);

  // Return String    
  header("Content-type: application/javascript");
	
	if(isset($callback) && !empty($callback)){
		$jsonp = json_encode($output);
		die( $callback.'('.$jsonp.')' );
	}
	else {
		die( json_encode($output) );
	}
		
}

// create custom Ajax call for WordPress
add_action( 'wp_ajax_nopriv_ajaxOnboard', 'ajaxOnboard' );
add_action( 'wp_ajax_ajaxOnboard', 'ajaxOnboard' );


/**********/

// getPage Info
if(!function_exists('getPageImage')) {
    function getPageImage($pageId) {
        if(!is_numeric($pageId)) {
            return;
        }
        global $wpdb;
        $sql_query = 'SELECT DISTINCT * FROM ' . $wpdb->posts .
        ' WHERE ' . $wpdb->posts . '.ID=' . $pageId;
        $posts = $wpdb->get_results($sql_query);
        if(!empty($posts)) {
            foreach($posts as $post) {
            	$content = $post->post_content;
            		
        		// Get image
        		$first_img = '';
        		ob_start();
				ob_end_clean();
				$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
				$first_img = $matches [1] [0];
				
				if(empty($first_img)){
					$first_img = "/images/default.jpg";
				}
				
				return $first_img;			
            }
        }
    }
}

if(!function_exists('getPageContent')) {
    function getPageContent($pageId) {
        if(!is_numeric($pageId)) {
            return;
        }
        global $wpdb;
        $sql_query = 'SELECT DISTINCT * FROM ' . $wpdb->posts .
        ' WHERE ' . $wpdb->posts . '.ID=' . $pageId;
        $posts = $wpdb->get_results($sql_query);
        if(!empty($posts)) {
            foreach($posts as $post) {
            	$content = $post->post_content;
            		
        		// Remove Image
				$postOutput = preg_replace('/<img[^>]+./','', $content);
				
				// Swap for break tags
				$pregOutput = nl2br($postOutput);
				$result = preg_replace('/^(?:<br\s*\/?>\s*)+/', '', $pregOutput);
				$result = str_replace( '<br />', '', $result );
				$result = str_replace( '<br>', '', $result );
				return $result;				
            }
        }
    }
}

if(!function_exists('getPageTitle')) {
    function getPageTitle($pageId) {
        if(!is_numeric($pageId)) {
            return;
        }
        global $wpdb;
        $sql_query = 'SELECT DISTINCT * FROM ' . $wpdb->posts .
        ' WHERE ' . $wpdb->posts . '.ID=' . $pageId;
        $posts = $wpdb->get_results($sql_query);
        if(!empty($posts)) {
            foreach($posts as $post) {
            	$title = $post->post_title;
            		
        		return $title;				
            }
        }
    }
}


/********/

function add_admin_scripts( $hook ) {

    global $post;

    if ( $hook == 'edit.php' || 'post.php' ) {
        if ( 'beer' === $post->post_type ) {
            ?>
            <style type="text/css">
					 
							#acf_15 #acf-image  { 
								display: none;
							}
					 
						</style>
            <?php
        }
    }
}
add_action( 'admin_head', 'add_admin_scripts');


/********/

?>