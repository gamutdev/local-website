<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" xmlns:fb="http://ogp.me/ns/fb#" style="margin: 0 !important;"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8" xmlns:fb="http://ogp.me/ns/fb#" style="margin: 0 !important;"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9" xmlns:fb="http://ogp.me/ns/fb#" style="margin: 0 !important;"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10" xmlns:fb="http://ogp.me/ns/fb#" style="margin: 0 !important;"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" xmlns:fb="http://ogp.me/ns/fb#" style="margin: 0 !important;"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Local Brewing Co. -- San Francisco, CA</title>
  <meta name="google" content="notranslate" />
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=0">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <!-- styles -->
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
	  <link href="<?php echo get_template_directory_uri(); ?>/positron/positron.css" type="text/css" rel="stylesheet"></link>
	   <link href="<?php echo get_template_directory_uri(); ?>/css/jquery.mCustomScrollbar.css" type="text/css" rel="stylesheet"></link>
	  <link rel="stylesheet" href="<?php echo get_stylesheet_uri() .'?v='. mt_rand(); ?>">
	  
	  <!-- Libraries -->
	  <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.6.2.min.js"></script>
	  
	  <!-- Base -->
	  <?php
		  if(!(strpos($_SERVER['SERVER_NAME'], 'localhost') === false)){
		?>
	  	<base href="wp-content/themes/lbc/">
	  <?php
	  	} else {
		?>
			<base href="/cms/wp-content/themes/lbc/">
		<?php
	  	}
	  ?>
	  
	  <!-- WP Head -->
	  <?php wp_head(); ?>
		
		<!-- scripts -->
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<script src="http://cdn.jsdelivr.net/jquery.scrollto/2.1.0/jquery.scrollTo.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/positron/prefixfree.min.js" type="text/javascript"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/positron/prefixfree.dynamic-dom.min.js" type="text/javascript"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/positron/positron.js" type="text/javascript"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle.all.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/viewport-units-buggyfill.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBylfVvV1-AMC0eEPnLY-vA8b4HVap0s0M" type="text/javascript"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
		
		<script>
			
			window.viewportUnitsBuggyfill.init();
			
		  // Better resizing
			(function($,sr){
			
			  // debouncing function from John Hann
			  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
			  var debounce = function (func, threshold, execAsap) {
			      var timeout;
			
			      return function debounced () {
			          var obj = this, args = arguments;
			          function delayed () {
			              if (!execAsap)
			                  func.apply(obj, args);
			              timeout = null;
			          };
			
			          if (timeout)
			              clearTimeout(timeout);
			          else if (execAsap)
			              func.apply(obj, args);
			
			          timeout = setTimeout(delayed, threshold || 300);
			      };
			  }
			  // smartresize 
			  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };
			
			})(jQuery,'smartresize');
			
			<?php
				if( !isset($_GET['log']) && $_GET['log'] != "true" ){
			?>
			//console.log = function() {}
			<?php
				}
			?>
			
			<?php
				if( !isset($_GET['errors']) && $_GET['errors'] != "true" ){
			?>
			//console.error = function() {}
			<?php
				}
			?>
	  </script>
</head>

<?php
	if(is_home()) {
		$page_slug = 'home';
	}
	else if(is_page()) { 
		$page_slug = $post->post_name; 
	} 
?>

<body <?php body_class( array( $page_slug, "p-invisible" ) ) ?>>
    <!--[if lt IE 8]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <div id="fb-root"></div>
		<script>
			window.fbAsyncInit = function() {
		    FB.init({
			    version: 'v2.1',
		      appId: '1530679393856403', // '1600251093565899' (demo)
		      status: true, 
		      cookie: true, 
		      xfbml: true, 
		      oauth:true, 
		      channelUrl: 'http://www.sfbeerweek.org/wp-content/plugins/wp-fb-autoconnect/assets/channel.html'
		    });
		  };
		  
		  // Load the SDK Asynchronously
			(function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "http://connect.facebook.net/en_US/sdk.js";
		     fjs.parentNode.insertBefore(js, fjs);
		  }(document, 'script', 'facebook-jssdk'));
		</script>
    
    <?php
	  //include('modal.php');  
	  ?>
	  
	  <?php
		if (!isset($_COOKIE["agecheck-cookie"])){
		?>
		<div class="intro agecheck visibility-web">
			
			<div class="top-right"></div>
			<div class="bottom-left"></div>
			
			<div id="main">
				<div class="content clearfix">
					<div class="main">
						<div class="logo"></div>
						<div class="statement">
							<div class="title">small tanks. big beer. drink local</div>
							<div class="warning">You must be at least 21 to enter this site.</div>
							<div class="agecheck">
								<button class="confirm"></button>
							</div>
						</div>
					</div>
				</div>
				<div class="background bottom"></div>
			</div>
			
		</div>
		<?php
		}
		?>
	  
	  <section p-view="menu:cj" class="p-invisible overlay">
		  <header>
				<div class="section-header title"></div>
				<div 
					p-action-1="(click) hideview: menu"
					class="close-btn ding right"
				>&#8217;</div>
			</header>
			
			<div class="scroller">
			
				<p-if true="'$params.section;' == 'navigation'">
				
					<!-- Navigation -->
					
					<div p-view="navigation:" class="navigation content">
						
						<ul class="block">
			        
			        <li class="home">
								<h1><a href="<?php echo home_url(); ?>">Home</a></h1>
							</li>
							
							<ul class="block">
								<svg width="100%" height="10px" style="margin-top: 10px; width: 100%;">
									<line x1="7" x2="100%" y1="4" y2="4" stroke="#5184AF" stroke-width="4" stroke-linecap="round" stroke-dasharray="1, 10" style="stroke: white;"></line>
								</svg>
							</ul>
							
							<li
								class="<?php if (is_page('beer')) { echo "active"; } ?>"
								p-action-1="<?php if (is_home()) { echo "(mouseover) removeclass: hidden/header nav.sub#beer"; } ?>"
								p-action-2="<?php if (is_home()) { echo "(mouseout) addclass: hidden/header nav.sub#beer"; } ?>"
							>
								<h1><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>">Our Beer</a></h1>
							  <nav 
							    id="beer"
							    class="sub"
							    p-action-1="<?php if (is_home()) { echo "(mouseover) removeclass: hidden/header nav.sub#beer"; } ?>"
									p-action-2="<?php if (is_home()) { echo "(mouseout) addclass: hidden/header nav.sub#beer"; } ?>"
							   >
							    <ul>
							      <li class="beerlist"><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>">Beer List</a></li>
							      <li class="curator"><a href="<?php echo home_url() . '/curator'; ?>" target="_blank" p-action-1="(click) hideview: menu">Curator</a></li>
							      <li class="find"><a href="<?php echo get_page_link( get_page_by_path( 'map' ) ); ?>">Find Us</a></li>
							    </ul>
							  </nav>
							</li>
							
							<ul class="block">
								<svg width="100%" height="10px" style="margin-top: 10px; width: 100%;">
									<line x1="7" x2="100%" y1="4" y2="4" stroke="#5184AF" stroke-width="4" stroke-linecap="round" stroke-dasharray="1, 10" style="stroke: white;"></line>
								</svg>
							</ul>
							
							<li
								class="<?php if (is_page('learn')) { echo "active"; } ?>"
								p-action-1="<?php if (is_home()) { echo "(mouseover) removeclass: hidden/header nav.sub#learn"; } ?>"
								p-action-2="<?php if (is_home()) { echo "(mouseout) addclass: hidden/header nav.sub#learn"; } ?>"
							>
								<h1><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/">Learn</a></h1>
								<nav
							  	id="learn" 
							    class="sub"
							    p-action-1="<?php if (is_home()) { echo "(mouseover) removeclass: hidden/header nav.sub#learn"; } ?>"
									p-action-2="<?php if (is_home()) { echo "(mouseout) addclass: hidden/header nav.sub#learn"; } ?>"
							   >
							    <ul>
							      <li class="live-local"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/live-local" p-action-1="(click) hideview: menu">Live Local</a></li>
						        <li class="team"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/team" p-action-1="(click) hideview: menu">Our Team</a></li>
						        
						        <?php
										$args = array( 'post_type' => 'guestbrewer', 'posts_per_page' => -1 );
										$guestbrewers_wp_query = new WP_Query($args);
										
										$guestbrewers_count = $guestbrewers_wp_query->found_posts;
							      if($guestbrewers_count > 0){
							      ?>
						        <li class="guest-brewers"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/guest-brewers" p-action-1="(click) hideview: menu">Guest Brewers</a></li>
						        <?php
							      }
							      ?>
							    </ul>
							  </nav>
							</li>
							
							<ul class="block">
								<svg width="100%" height="10px" style="margin-top: 10px; width: 100%;">
									<line x1="7" x2="100%" y1="4" y2="4" stroke="#5184AF" stroke-width="4" stroke-linecap="round" stroke-dasharray="1, 10" style="stroke: white;"></line>
								</svg>
							</ul>
							
							<li 
								class="<?php if (is_page('contact')) { echo "active"; } ?>"
								p-action-1="<?php if (is_page('contact')) { echo "(mouseover) addclass: hidden/header nav.sub"; } ?>"
								p-action-2="<?php if (is_page('contact')) { echo "(mouseout) removeclass: hidden/header nav li.active nav.sub"; } ?>"
							>
								<h1><a href="<?php echo get_page_link( get_page_by_path( 'contact' ) ); ?>#/">Contact</a></h1>
							</li>
							<li>
								<h1><a href="https://www.enjoylocalbeer.com/" target="_blank">Shop</a></h1>
							</li>
		       </ul>
					
					</div>
				
				</p-if>
			
			</div>
	  </section>

    <header>
      <nav>
	      
	      <div 
		      class="icon-menu"
			    p-action-1="(click) refreshview: menu"
					p-action-1-params="section: navigation;"
			  ></div>
			  
			  <?php
				if( is_home() ){  
				?>
			  <div class="title">Local Brewing Co.</div>
			  <?php
				}  
				?>
			  
			  <?php
				if( is_page('beer') ){  
				?>
			  <div class="title">Beer List</div>
			  <?php
				}  
				?>
				
				<?php
				if( is_page('map') ){  
				?>
			  <div class="title">Find Us</div>
			  <?php
				}  
				?>
				
				<?php
				if( is_page('learn') ){  
				?>
			  <div class="title">Learn</div>
			  <?php
				}  
				?>
				
				<?php
				if( is_page('contact') ){  
				?>
			  <div class="title">Contact</div>
			  <?php
				}  
				?>
				
				<?php
				if( is_page('jobs') ){  
				?>
			  <div class="title">Jobs</div>
			  <?php
				}  
				?>
				
				<?php
				if( is_page('distributors') ){  
				?>
			  <div class="title">Distributors</div>
			  <?php
				}  
				?>
				
				<?php
				if( is_page('press') ){  
				?>
			  <div class="title">Press</div>
			  <?php
				}  
				?>
	      
        <ul class="nav-container">
	        <li class="logo">
	        	<a href="<?php echo home_url(); ?>">
	        		<div p-view="logo:h"></div>
	        	</a>
	        </li>
	        
	        <li class="home">
	        	<a href="<?php echo home_url(); ?>">Home</a>
	        </li>

	        <li
	        	class="<?php if (is_page('beer')) { echo "active"; } ?>"
	        	p-action-1="<?php if (is_home()) { echo "(mouseover) removeclass: hidden/header nav.sub#beer"; } ?>"
	        	p-action-2="<?php if (is_home()) { echo "(mouseout) addclass: hidden/header nav.sub#beer"; } ?>"
	        >
	        	<a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>#/">Our Beer</a>
		        <nav 
			        id="beer"
			        class="sub <?php if (!is_page('beer')) { echo "hidden"; } ?>"
				      p-action-1="<?php if (is_home()) { echo "(mouseover) removeclass: hidden/header nav.sub#beer"; } ?>"
							p-action-2="<?php if (is_home()) { echo "(mouseout) addclass: hidden/header nav.sub#beer"; } ?>"
				     >
			        <ul>
				        <li class="beerlist"><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>#/beerlist">Beer List</a></li>
				        <li class="curator"><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>#/curator">Curator</a></li>
				        <li class="find"><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>#/find">Find Us</a></li>
			        </ul>
		        </nav>
	        </li>
	        <li
	        	class="<?php if (is_page('learn')) { echo "active"; } ?>"
	        	p-action-1="<?php if (is_home()) { echo "(mouseover) removeclass: hidden/header nav.sub#learn"; } ?>"
	        	p-action-2="<?php if (is_home()) { echo "(mouseout) addclass: hidden/header nav.sub#learn"; } ?>"
	        >
	        	<a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/">Learn</a>
	        	<nav
		        	id="learn" 
			        class="sub <?php if (!is_page('learn')) { echo "hidden"; } ?>"
				      p-action-1="<?php if (is_home()) { echo "(mouseover) removeclass: hidden/header nav.sub#learn"; } ?>"
							p-action-2="<?php if (is_home()) { echo "(mouseout) addclass: hidden/header nav.sub#learn"; } ?>"
				     >
			        <ul>
				        <li class="live-local"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/live-local">Live Local</a></li>
				        <li class="team"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/team">Our Team</a></li>
				        <?php
								$args = array( 'post_type' => 'guestbrewer', 'posts_per_page' => -1 );
								$guestbrewers_wp_query = new WP_Query($args);
								
								$guestbrewers_count = $guestbrewers_wp_query->found_posts;
					      if($guestbrewers_count > 0){
					      ?>
				        <li class="guest-brewers"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/guest-brewers">Guest Brewers</a></li>
				        <?php
					      }
					      ?>
				        <li class="events"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/events">Events</a></li>
				        <li class="live"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/live">Live Feed</a></li>
			        </ul>
		        </nav>
	        </li>
	        <li 
	        	class="<?php if (is_page('contact')) { echo "active"; } ?>"
	        	p-action-1="<?php if (is_page('contact')) { echo "(mouseover) addclass: hidden/header nav.sub"; } ?>"
	        	p-action-2="<?php if (is_page('contact')) { echo "(mouseout) removeclass: hidden/header nav li.active nav.sub"; } ?>"
	        >
	        	<a href="<?php echo get_page_link( get_page_by_path( 'contact' ) ); ?>#/">Contact</a>
		      </li>
	        <li class="">
						<a href="https://www.enjoylocalbeer.com/" target="_blank">Shop</a>
					</li>
	        
	        <div class="visibility-web">
	        <li class="socials">
	        	<ul>
		        	
		        	<?php
							$temp = $wp_query;
							$wp_query = null;
							$args = array( 'post_type' => 'page', 'pagename' => 'general' );
							$wp_query = new WP_Query($args);
							while ($wp_query->have_posts()) : $wp_query->the_post();
							
							if(get_field('social_media')):
								while(has_sub_field('social_media')):
								
								$service = get_sub_field('service');
								$url = get_sub_field('url');
								?>
								
								<?php
								if(strtolower($service) == "facebook"){
								?>
								<li><a class="<?php echo strtolower($service); ?> ding" href="<?php echo $url; ?>" target="_blank">G</a></li>
								<?php
								}
								?>
								<?php
								if(strtolower($service) == "twitter"){
								?>
			        	<li><a class="<?php echo strtolower($service); ?> ding" href="<?php echo $url; ?>" target="_blank">t</a></li>
			        	<?php
								}
								?>
			        	<?php
								if(strtolower($service) == "instagram"){
								?>
			        	<li><a class="<?php echo strtolower($service); ?> icon-instagram" href="<?php echo $url; ?>" target="_blank"></a></li>
			        	<?php
								}
								?>
			        	<?php
								if(strtolower($service) == "untappd"){
								?>
			        	<li><a class="<?php echo strtolower($service); ?> icon-untappd" href="<?php echo $url; ?>" target="_blank"></a></li>
			        	<?php
								}
								?>
										
								<?php 
								endwhile;
							endif; 
							?> 
							
							<?php 
							endwhile; 
							wp_reset_postdata(); 
							?>
							
	        	</ul>
	        </li>
	        </div>
        </ul>
        
        <!--
        <ul class="account">
	        
	        <p-localstorage key="lbc_user">
						<p-if notempty="$localstorage.lbc_user;">
							<p-json
								url="http://localbrewingco.com/cms/user/?userid=$localstorage.lbc_user;"
								name="user"
								cachekey="userdata"
								class="p-invisible"
							>
								<li class="username">$user.username;</li>
								<li
									p-action-1="(click) refreshview: menu"
									p-action-1-params="section: account;"
									class=""
								>
									<div class="avatar">
										<p-img src="$user.avatar;"></p-img>
									</div>
								</li>
							</p-json>
						</p-if>
						<p-if empty="$localstorage.lbc_user;">
							<li
								data-toggle="modal" 
								data-target="#modal"
								class="login-btn" 
								style=""
							><a href="javascript:;">Log In</a></li>
						</p-if>
					</p-localstorage>
	        
        </ul>
        -->
        
      </nav>
    </header>