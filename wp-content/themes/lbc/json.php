<?php
/*
Template Name: JSON
*/

if( !isset($_GET['log']) && !isset($_GET['errors']) ){
	error_reporting(0);
	ini_set('display_errors', 0);
}

function normalize ($string) {
  $table = array(
      'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
      'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
      'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
      'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
      'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
      'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
      'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
      'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
  );

  return strtr($string, $table);
}

$callback = $_GET['callback'];
$type = $_GET['type'];

// Beerlist query params
$ontap = $_GET['ontap'];

// Locations query params
$region = $_GET['region'];
$account = $_GET['account'];
$zip = $_GET['zip'];
$beername = $_GET['beername'];


// Beer list
if( $type == "beerlist" ){

	$args = array( 
		'post_type' => 'beer', 
		'posts_per_page' => -1, 
		'post_status' => array('publish'), 
		'orderby' => 'title', 
		'order' => 'ASC' 
	);
	
	$temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query($args);
	if ( $wp_query->have_posts() ) :
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		// General
		$post_count = $wp_query->post_count;
		$found_posts = $wp_query->found_posts;
		$pagecount = $wp_query->max_num_pages;
		
		// Post specific	
		$post_id = $post->ID;
		$post_title = $post->post_title;
		$post_slug = $post->post_name;
		$custom = get_post_custom($post_id);	
		
		// Beer Info
		$name = $post_title;
		$style_data = get_field_object('style');
		if($style_data){
			foreach($style_data as $key) {
				$style = $key->slug;
			}
		}
		
		$short_description = get_field('short_description');
		$full_description = get_field('description');
		
		// Measurements
		$abv = get_field('abv');
		$ibu = get_field('ibu');
		
		// Profile
		$hoppyness = get_field('hoppyness');
		$alcohol = get_field('alcohol');
		$complexity = get_field('complexity');
		
		// Availability
		//$ontap = get_field('ontap');
		$packaged = get_field('packaged');
		
		// Image
		//$image = get_field('image');
		$image_name = get_field('beer_image');
		if(empty($image_name)){
			$image_name = 'beer-16oz-medium';
		}
		
		$image = get_template_directory_uri() . '/img/beers/' . $image_name . '.png';
		
		// Check to see if the beer is on tap
		$ids = get_field('manage', 96, false);		
		$args2 = array( 
			'post_type' => 'beer', 
			'posts_per_page' => -1, 
			'post_status' => array('publish'), 
			'post__in' => $ids, 
			'orderby' => 'title', 
			'order' => 'ASC' 
		);
		
		$ontap = false;
		
		$temp = $taplist_query;
		$taplist_query = null;
		$taplist_query = new WP_Query($args2);
		if ( $taplist_query->have_posts() ) :
			while ($taplist_query->have_posts()) : $taplist_query->the_post();
			
				if($post->ID == $post_id){
					$ontap = true;
				}
			
			endwhile;
		endif;
		wp_reset_postdata();
		$taplist_query = null;
	
		$beer[] = array(
			'id' => $post_id,
			'name' => $name,
			'slug' => $post_slug,
			'style' => $style,
			'short_description' => $short_description,
		  'full_description' => $full_description,
		  'abv' => $abv,
		  'ibu' => $ibu,
		  'hoppyness' => $hoppyness,
		  'alcohol' => $alcohol,
		  'complexity' => $complexity,
		  'ontap' => $ontap,
		  'packaged' => $packaged,
		  'image' => $image
		);
			
		// Output
		$output['beers'] = $beer;	
			
		endwhile; 
		wp_reset_postdata();
		$wp_query = null; 
		$wp_query = $temp;
	
	else:
	
		$output['beers'] = array();
	
	endif;
	
	if(count($output) == 0){
		$output['beers'] = array();
	}

}

// Taplist
/*
if( $type == "taplist" ){
	
	$ids = get_field('manage', 96, false);
	
	$args = array( 'post_type' => 'beer', 'posts_per_page' => -1, 'post_status' => array('publish'), 'post__in' => $ids, 'orderby' => 'title', 'order' => 'ASC' );
	
	$temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query($args);
	if ( $wp_query->have_posts() ) :
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		// General
		$post_count = $wp_query->post_count;
		$found_posts = $wp_query->found_posts;
		$pagecount = $wp_query->max_num_pages;
		
		// Post specific	
		$post_id = $post->ID;
		$post_title = $post->post_title;
		$custom = get_post_custom($post_id);	
		
		// Beer Info
		$name = $post_title;
		$style_data = get_field('style');
		foreach($style_data as $key) {
			$style = $key->slug;
		}
		
		$short_description = get_field('short_description');
		$full_description = get_field('description');
		
		// Measurements
		$abv = get_field('abv');
		$ibu = get_field('ibu');
		
		// Profile
		$hoppyness = get_field('hoppyness');
		$alcohol = get_field('alcohol');
		$complexity = get_field('complexity');
		
		// Availability
		$ontap = get_field('ontap');
		$packaged = get_field('packaged');
		
		// Image
		$image = get_field('image');
	
		$beer[] = array(
			'id' => $post_id,
			'name' => $name,
			'style' => $style,
			'short_description' => $short_description,
		  'full_description' => $full_description,
		  'abv' => $abv,
		  'ibu' => $ibu,
		  'hoppyness' => $hoppyness,
		  'alcohol' => $alcohol,
		  'complexity' => $complexity,
		  'ontap' => $ontap,
		  'packaged' => $packaged,
		  'image' => $image
		);
			
		// Output
		$output['taplist'] = $beer;	
			
		endwhile; 
		wp_reset_postdata();
		$wp_query = null; 
		$wp_query = $temp;
	
	else:
	
		$output['taplist'] = array();
	
	endif;
	
	if(count($output) == 0){
		$output['taplist'] = array();
	}
	
}
*/

// Locations
if( $type == "locations" ){
	
	$meta_query = array();
	$tax_query = array();
	$search_query = null;
	
	if(isset($region)){
		
		$tax_query[] = array(
		  'taxonomy' => 'Region',
	    'field'    => 'name',
	    'terms'    => $region
	  );
	} 
	
	if(isset($zip)){
		
		$meta_query[] = array(
		  'key' => 'zip',
			'value' => $zip,
			'compare' => 'LIKE'
	  );
	}
	
	if(isset($account)){
		
		$meta_query[] = array(
		  'key' => 'availability',
			'value' => $account,
			'compare' => 'LIKE'
	  );
		
	}
	
	if(isset($beername) && !empty($beername)){
		
		$beergs = array(
			 'post_type' => 'beer', 
			 'posts_per_page' => -1, 
			 'post_status' => array('publish'),
		);
		
		$beer_query = new WP_Query( $beergs );
	
		while ( $beer_query->have_posts() ) : $beer_query->the_post();
			
			if(strtolower($post->post_title) == strtolower($beername)){
				$beerID = $post->ID;
			}
		
		endwhile;
		wp_reset_postdata();
		
		if(isset($account) && strtolower($account) == "on tap"){
			$meta_query[] = array(
			  'key' => 'on_tap',
				'value' => '"' . $beerID . '"',
				'compare' => 'LIKE'
		  );
	  }
	  
	  if(isset($account) && strtolower($account) == "retail"){
		  $meta_query[] = array(
			  'key' => 'retail',
				'value' => '"' . $beerID . '"',
				'compare' => 'LIKE'
		  );
	  }
		
	}
	
	//var_dump($meta_query);
	//var_dump($search_query);
	
	$args = array( 
		'post_type' => 'locations', 
		'posts_per_page' => -1, 
		'order' => 'ASC',
		'orderby' => 'title', 
		'post_status' => 'publish',
		'meta_query' => $meta_query,
		'tax_query' => $tax_query,
		's' => $search_query,
	);
	
	$temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query($args);
	if ( $wp_query->have_posts() ) :
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		// General
		$post_count = $wp_query->post_count;
		$found_posts = $wp_query->found_posts;
		$pagecount = $wp_query->max_num_pages;
		
		// Post specific	
		$post_id = $post->ID;
		$post_title = $post->post_title;
		
		// Location details
		$custom = get_post_custom($post->ID);
		$address = get_field('address');
		$city = get_field('city');
		$state = get_field('state');
		$zip = get_field('zip');
		$lat = get_field('lat');
		$lng = get_field('lng');
		$phone = get_field('phone');
		$website = get_field('website');
		
		// Account type
		$availability = get_field('availability');
				
		// Available at location
		
		$location_taplist = array();
		$location_taplist_data = get_field('on_tap');
		
		if($location_taplist_data){
			foreach($location_taplist_data as $key) {
				$location_taplist[] = array(
					'id' => $key->ID,
					'name' => $key->post_title,
					'slug' => $key->post_name,
				);
			}
		}
		
		$location_retail = array();
		$location_retail_data = get_field('retail');
		if($location_retail_data){
			foreach($location_retail_data as $key) {
				$location_retail[] = array(
					'id' => $key->ID,
					'name' => $key->post_title,
					'slug' => $key->post_name,
				);
			}
		}
	
		$location[] = array(
			'id' => $post_id,
			'name' => $post_title,
			'address' => $address,
		  'city' => $city,
		  'state' => $state,
		  'zip' => $zip,
		  'lat' => $lat,
		  'lng' => $lng,
		  'phone' => $phone,
		  'website' => $website,
		  'availability' => $availability,
		  'ontap' => $location_taplist,
		  'retail' => $location_retail
		);
		
		// Output
		$output['locations'] = $location;	
			
		endwhile; 
		wp_reset_postdata();
		$wp_query = null; 
		$wp_query = $temp;
	
	else:
	
		$output['locations'] = array();
	
	endif;
	
	if(count($output) == 0){
		$output['locations'] = array();
	}

}

// Curator
if( $type == "onboard" ){

	$args = array( 'post_type' => 'page', 'p' => 497, 'posts_per_page' => 1 );
	
	$temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query($args);
	if ( $wp_query->have_posts() ) :
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		// Post specific	
		$post_id = $post->ID;
		$post_title = $post->post_title;
		$post_slug = $post->post_name;
		
		$tutorial = array();
		
		if(get_field('tutorial')):
			while(has_sub_field('tutorial')):
			
			$title = get_sub_field('title');
			$description = get_sub_field('description');
			
			$images = new stdClass();
			$mobile_image = get_sub_field('mobile_image');
			$web_image = get_sub_field('web_image');
			
			$images->mobile = $mobile_image['url'];
			$images->web = $web_image['url'];
			
			$tutorial[] = array(
				'title' => $title,
				'description' => $description,
				'images' => $images,
			);
				
			endwhile;
		endif;			
	
		$onboard[] = array(
			'tutorial' => $tutorial,
		);
			
		// Output
		$output['onboard'] = $tutorial;	
			
		endwhile; 
		wp_reset_postdata();
		$wp_query = null; 
		$wp_query = $temp;
	
	else:
	
		$output['onboard'] = array();
	
	endif;
	
	if(count($output) == 0){
		$output['onboard'] = array();
	}

}

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Max-Age: 86400');
header('Cache-Control: no-cache');
header("Content-type: application/javascript");

if(isset($callback) && !empty($callback)){
	$jsonp = json_encode($output);
	die( $callback.'('.$jsonp.')' );
}
else {
	die( json_encode($output) );
}


?>