<div id="modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
	    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
	      
        <!-- Login -->
		    <div class="form login-form">    
							
				  <h1>Access these features with an account</h1>
				  
				  <svg class="linebreak" width="100%" height="10px">
						<line x1="7" x2="100%" y1="4" y2="4" stroke="#acacac" stroke-width="4" stroke-linecap="round" stroke-dasharray="1, 10" style="stroke: #acacac;"></line>
					</svg>
					
					<div class="description">
						Save a custom flavor profile to compare our beers, and keep a list of your favorites.
					</div>
				
					<form id="loginform" name="loginform" action="<?php $_SERVER['REQUEST_URI'] ?>" method="post">				
						
						<div class="block submit clearfix">
							<div class="loader hidden" style="margin-top:10px;"><img src="https://sfbeerweek.s3-us-west-1.amazonaws.com/images/loader.gif" width="36" height="36" style="width:36px !important; height:36px !important;"></div>
							<div class="fb_login">
								<span class="fbLoginButton">
				        	<a class="wpfbac-button btn btn-default" href="javascript:;" onclick="lbc.user.facebook.getLoginStatus();"><span class="ding">G</span> SIGN IN</a>
				        </span>
							</div>
						</div>	
									
					</form>
       	
		    </div>
		    
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->