<?php 
	/**
	 * Template Name: Beer
	 */
 
	include('header.php'); 
?>
        
  <main>
	  
	  <section p-view="overlay" id="overlay" class="p-invisible"></section>
	  
	  <div class="visibility-tablet visibility-mobile">

			<section id="beerlist" class="tap-list">
				<div class="p-visible shelf filters clearfix" p-view="shelf:">
					
					<div 
						class="btn btn-left filter ontap active"
						p-action-1="(click) refreshview: list"
						p-action-1-params="type: ontap;"
						p-action-2="(click) refreshview: beerlist"
						p-action-2-params="type: ontap;"
					>On Tap</div>
					<div 
						class="btn btn-right filter archive"
						p-action-1="(click) refreshview: list"
						p-action-1-params="type: archive;"
						p-action-2="(click) refreshview: beerlist"
						p-action-2-params="type: archive;"
					>Archive</div>
					
				</div>
				
				<div class="list" p-view="list:" p-view-params="type: ontap;">
					
					<p-json
						url="http://localbrewingco.com/json/?type=beerlist"
						cachekey="beerlist"
						name="beers"
						class="p-invisible"
					>
					  <p-list key="beers.beers" name="beer">
					
							<p-if true="'$params.type;' == 'ontap'">							  
							  <p-if true="$beer.ontap; == true">
						
									<div class="beer-sum $beer.style;" 
										p-action-1="(click) refreshview: overlay/nu-slide-in-from-right"
										p-action-1-params="action: beer-detail; id: $beer.id;; name: $beer.name;; slug: $beer.slug;; hoppyness: $beer.hoppyness;; alcohol: $beer.alcohol;; complexity: $beer.complexity;;"
										onclick="ga('send', 'event', 'taplist', 'beer', '$beer.name;');"
										ontouchend=""
									>
										<div class="image">
											<p-img src="$beer.image;"></p-img>
										</div>
										<div class="info">
											<h2 class="name">$beer.name;</h2>
											<div class="description">$beer.short_description;</div>
										</div>
										<div class="levels">
											<div class="icon-hoppy"><div class="hoppy-level level">$beer.hoppyness;</div></div>
											<div class="icon-boozy"><div class="boozy-level level">$beer.alcohol;</div></div>
											<div class="icon-complex"><div class="complex-level level">$beer.complexity;</div></div>
										</div>
									</div>			
												
							  </p-if>							
							</p-if>
					
							<p-if true="'$params.type;' == 'archive'">
								
								<div class="beer-sum $beer.style;" 
									p-action-1="(click) refreshview: overlay/nu-slide-in-from-right"
									p-action-1-params="action: beer-detail; id: $beer.id;; name: $beer.name;; slug: $beer.slug;; hoppyness: $beer.hoppyness;; alcohol: $beer.alcohol;; complexity: $beer.complexity;;"
									onclick="ga('send', 'event', 'archive', 'beer', '$beer.name;');"
								>
									<div class="image">
										<p-img src="$beer.image;"></p-img>
									</div>
									<div class="info">
										<h2 class="name">$beer.name;</h2>
										<div class="description">$beer.short_description;</div>
									</div>
									<div class="levels">
										<div class="icon-hoppy"><div class="hoppy-level level">$beer.hoppyness;</div></div>
										<div class="icon-boozy"><div class="boozy-level level">$beer.alcohol;</div></div>
										<div class="icon-complex"><div class="complex-level level">$beer.complexity;</div></div>
									</div>
								</div>
							
							</p-if>
							
					  </p-list>
					</p-json>
					
				</div>
			</section>
			
	  </div>
	  
	  <div class="visibility-web">
		
		<?php	
		$temp = $wp_query;
		$wp_query = null;
		$args = array( 'post_type' => 'page', 'pagename' => 'beer' );
		$wp_query = new WP_Query($args);
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		$post_title = $post->post_title;
		$image = get_field('beer_background_image');
		?>    
  	
  	<section id="masthead" style="background: url(<?php echo $image['url']; ?>) 50% 50% no-repeat; background-size:cover;">
    	<div class="content">
    		<h1><?php echo $post_title; ?></h1>
    	</div>
  	</section>
  	
  	<?php 
		endwhile; 
		wp_reset_postdata(); 
		?>
  	
  	<section id="beerlist">
    	<div class="content">
      	<div class="left">
      		<h2>Beer List</h2>
      		<span class="selector">
      			<span class="hidden">Sort by: </span>
						<span class="hidden">Style</span>
      		</span>
      	</div>
      	<div class="right">
	      	<div class="filters">
		      	<div 
			      	class="filter ontap active"
				      p-action-1="(click) refreshview: beerlist"
							p-action-1-params="type: ontap;"
							p-action-2="(click) refreshview: list"
							p-action-2-params="type: ontap;"
				    >On Tap</div>
		      	<div 
			      	class="filter archive"
						  p-action-1="(click) refreshview: beerlist"
							p-action-1-params="type: archive;"
							p-action-2="(click) refreshview: list"
							p-action-2-params="type: archive;"
				    >Archive</div>
	      	</div>
      	</div>
      	<div id="list" p-view="beerlist:j" p-view-params="type: ontap;">
	      	<div class="scroller scroll-pane">
		      	
		      	<p-json
							url="http://localbrewingco.com/json/?type=beerlist"
							cachekey="beerlist"
							name="beers"
							class="p-invisible"
						>
						  <p-list key="beers.beers" name="beer">
							  
							  <p-if true="'$params.type;' == 'ontap'">
								  <p-if true="$beer.ontap; == true">
							
										<div 
											class="item beer-sum $beer.style;"
											p-action-1="(click) refreshview: overlay"
											p-action-1-params="action: beer-detail; id: $beer.id;; name: $beer.name;; slug: $beer.slug;; hoppyness: $beer.hoppyness;; alcohol: $beer.alcohol;; complexity: $beer.complexity;;"
											onclick="ga('send', 'event', 'taplist', 'beer', '$beer.name;');"
										>
							      	<div class="image">
								      	<p-img src="$beer.image;"></p-img>
							      	</div>
							      	<div class="info">
												<h2 class="name">$beer.name;</h2>
												<div class="description">$beer.short_description;</div>
											</div>
							      	<div class="stats">
								      	<span class="abv"><span>$beer.abv;%</span> ABV</span> / 
								      	<span class="ibu"><span>$beer.ibu;</span> IBU</span>
							      	</div>
							      	<div class="levels">
												<div class="icon-hoppy"><div class="hoppy-level level">$beer.hoppyness;</div></div>
												<div class="icon-boozy"><div class="boozy-level level">$beer.alcohol;</div></div>
												<div class="icon-complex"><div class="complex-level level">$beer.complexity;</div></div>
											</div>
						      	</div>
									
								  </p-if>
							  </p-if>
							  
							  <p-if true="'$params.type;' == 'archive'">
						
									<div 
										class="item beer-sum $beer.style;"
										p-action-1="(click) refreshview: overlay"
										p-action-1-params="action: beer-detail; id: $beer.id;; name: $beer.name;; slug: $beer.slug;; hoppyness: $beer.hoppyness;; alcohol: $beer.alcohol;; complexity: $beer.complexity;;"
										onclick="ga('send', 'event', 'taplist', 'beer', '$beer.name;');"
									>
						      	<div class="image">
							      	<p-img src="$beer.image;"></p-img>
						      	</div>
						      	<div class="info">
											<h2 class="name">$beer.name;</h2>
											<div class="description">$beer.short_description;</div>
										</div>
						      	<div class="stats">
							      	<span class="abv"><span>$beer.abv;%</span> ABV</span> / 
							      	<span class="ibu"><span>$beer.ibu;</span> IBU</span>
						      	</div>
						      	<div class="levels">
											<div class="icon-hoppy"><div class="hoppy-level level">$beer.hoppyness;</div></div>
											<div class="icon-boozy"><div class="boozy-level level">$beer.alcohol;</div></div>
											<div class="icon-complex"><div class="complex-level level">$beer.complexity;</div></div>
										</div>
					      	</div>
								
							  </p-if>
							
							</p-list>	
						</p-json>
		      	
	      	</div>
      	</div>
    	</div>
  	</section>
  	
  	<section id="curator">
    	<div class="content">
	    	
	    	<?php
				$temp = $wp_query;
				$wp_query = null;
				$args = array( 'post_type' => 'page', 'pagename' => 'beer' );
				$wp_query = new WP_Query($args);
				while ($wp_query->have_posts()) : $wp_query->the_post();
				
				$headline = get_field('beer_curator_headline');
				$curator_description = get_field('beer_curator_description');				
				?> 
	    	
	    	<div class="left">
		    	
		    	<?php
			    $count = 1;
		    	if(get_field('points_of_interest')):
						while(has_sub_field('points_of_interest')):
						
						$poi = get_sub_field('poi');
						$description = get_sub_field('description');
						?>
	            
	            <div class="poi <?php echo $poi; ?>" data-toggle="tooltip" data-placement="right" title="<?php echo $description; ?>"><?php echo $count; ?></div>
								
						<?php
						$count++; 
						endwhile;
					endif; 
					?>

		    	<img src="img/beer-curator.png">
		    	
	    	</div>
    		<div class="right">
      		<h2><?php echo $headline; ?></h2>
      		<h3><?php echo $curator_description; ?></h3>
      		<button 
      			class="launch large"
      			p-action-1="(click) call: openCurator"
      		>Launch</button>
      	</div>
      	
      	<?php 
				endwhile; 
				wp_reset_postdata(); 
				?>
      	
    	</div>
  	</section>
  	
  	<section id="find">
	  	<div class="bar">
		  	<div class="content">
			  	<div class="left">
		    		<h2>Find Us</h2>
		    		<form>
			    		<div class="icon-search"></div>
			    		<input type="text" placeholder="Enter Zip Code">
		    		</form>
		    		<a 
			    		class="reset hidden"
				    	href="javascript: getAllLocations()"
						  p-action-1="(click) addclass: hidden/p-this-element"
				    >Reset</a>
			  	</div>
	    		
	    		<div class="right">
		    		<div class="filters">
			    		<div 
				      	class="filter active"
					      p-action-1="(click) removeclass: active/#find .filter"
						    p-action-2="(click) addclass: active/p-this-element"
					    >On Tap</div>
			      	<div 
				      	class="filter"
					      p-action-1="(click) removeclass: active/#find .filter"
						    p-action-2="(click) addclass: active/p-this-element"
					    >Retail</div>
		    		</div>
	    		</div>
	    	</div>
	  	</div>
	  	<div p-view="map:cj" p-view-params="section: beer;">
		  	<div id="map-canvas">
			  	<div id="map"></div>
		  	</div>
	  	</div>
	  	<div id="results">
		  	<div class="tap-list">
					<div class="map-result p-invisible">Results for "<span></span>"</div>
					<div class="list"></div>
				</div>
	  	</div>
  	</section>
  	
  	<?php
		$temp = $wp_query;
		$wp_query = null;
		$args = array( 'post_type' => 'page', 'pagename' => 'beer' );
		$wp_query = new WP_Query($args);
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		$headline = get_field('beer_endcap_headline');
		$image = get_field('beer_endcap_background_image');
		
		if($image){
			$image = 'background: url('. $image['url'] .') 50% 0 no-repeat; background-size:cover;';
		}				
		?> 
		
		<section id="interstitial" class="interstitial" style="<?php echo $image; ?>">
    	<div class="content">
    		<h2><?php echo $headline; ?></h2>
    	</div>
  	</section>
  	
  	<?php 
		endwhile; 
		wp_reset_postdata(); 
		?>
		
	  </div>
  	
  </main>

<?php include('footer.php'); ?>