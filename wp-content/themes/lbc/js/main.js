var lbc = lbc || {};

jQuery(document).ready(function(){
	
	
	/* Get sections for scroll spying */
	var page = $('nav li.active nav').attr('id');
	var page_sections = [];
	var page_subsection = false;
	
	function getSections(){
		$('main section').each(function(){
			var id = this.id;
			/*
			var href = $(this).attr('href');
			var hash = href.split('#/')[1];
			
			if(hash.indexOf(',') > -1){
				hash = hash.split(',')[0];
			}
			*/
			page_sections.push(id);
		});
		
		console.log(page_sections);
	}
	
	if(page){
		getSections();
	}
	
	
	/* Scroll to sections */
	function goToSectio(){
		
		console.log('goToSection()');
		
		var timing = 400;
		
		if(document.URL.indexOf('#') > -1){
			if(document.URL.indexOf(',') > -1){
				var section = document.URL.split('#')[1].split(',')[0];
				var subsection = document.URL.split(',')[1];
				page_subsection = subsection;
				
				if(subsection == "archive"){
					var archive = true;
				}
				else {
					var archive = false;
				}
				
				console.log(section);
			}
			else {
				var section = document.URL.split('#')[1];
				console.log(section);
			}
			
			if(section == "/curator"){
				var offset = -60;
			}
			else {
				var offset = -110;
			}
			
			if( $('body').hasClass('p-handheld') ){
				var timing = 1;
				var offset = -60;	
			}
			if( $('body').hasClass('p-tablet') ){
				var timing = 1;
				var offset = -110;	
			}
			if( $('body').hasClass('p-fullsize') ){
				var timing = 400;
				var offset = -110;	
			}
			
			var pre = '';
			
			if( $('body').hasClass('p-fullsize') ){
				if(section == "/beerlist"){
					var pre = '.visibility-web ';	
				}
			}
			
			if( $('body').hasClass('p-tablet') ){
				if(section == "/beerlist"){
					var pre = '.visibility-web ';	
				}
			}
			
			console.log(pre + '#' + section.replace('/', ''));
			
			if(section.length > 1){
				$(window).scrollTo( $(pre + '#'+ section.replace('/', '')), timing, {offset: offset} );
			}
			
			/*
			if(subsection){
				var params = new Object();
						params.archive = archive;
				gApplication.refreshView('beerlist', params);
			}
			*/
			
		}
	
	}
	
	
	/***************************/
	
	
	// Login
	lbc.user = {
		
		login: function(u, p, protocol){
			
			var lastURL = document.URL;
		
			if(protocol == 'fb'){
				console.log('logging in with facebook');
				//_gaq.push(['_trackEvent', 'Login', 'Facebook', 'Facebook Login']);
				var username = u;
				var password = p;
				
				$('form#loginform .fb_login a').attr('disabled', 'disabled');
			} else {
				//_gaq.push(['_trackEvent', 'Login', 'Regular', 'Regular Login']);
				var username = u.val();
				var password = p.val();
				
				$('form#loginform input[type=submit]').attr('disabled', 'disabled');
			}
			
			// Show loader
			$('.modal form .loader').removeClass('hidden');
			$('.modal .login-form .reg_login, .modal .login-form .fb_login').addClass('hidden');
			$('.modal .register-form .reg_login, .modal .register-form .fb_login').addClass('hidden');
	    
			$.ajax({
				type: 'POST',
				url: '/cms/wp-admin/admin-ajax.php',
				dataType: 'jsonp',
				data: {
				  action: 'ajaxLogin',
				  user_login: username,
	        user_password: password,
	        protocol: protocol
				},
				success: function(result){
					console.log(result);
					
					var obj = result;
					
					if(obj.status == "success"){
					
						var userID = obj.data.userid;
						//console.log(userID);
						
						// If successful, load the users itinerary
						lbc.user.success(obj.data.userid, lastURL);
						
						// then, reload the page
						//location.href = '/dashboard/';
						
	        } else {
	        	console.log('fail!!');
	        	
	        	// Error handling
	        	var error = obj.error;
	        	if(username == "" && password == ""){
		        	error = "You'll need to type something in...";
	        	}
	        	if(error == "Invalid username. <a href=\"http://localbrewingco.com/?action=lostpassword\">Lost your password</a>?"){
		        	error = 'Invalid username. <a href="javascript:;" onclick="lbc.modal.load(\'passforgot\');" title="Forgot Password">Lost your password</a>?';
	        	}
	        	if(error == '<strong>ERROR</strong>: Invalid username. <a href="http://localbrewingco.com/?action=lostpassword">Lost your password</a>?'){
		        	error = 'Invalid username. <a href="javascript:;" onclick="lbc.modal.load(\'passforgot\');" title="Forgot Password">Lost your password</a>?';
	        	}
	        	if(error == '<strong>ERROR</strong>: The password you entered for the username <strong>'+ username +'</strong> is incorrect. <a href="http://localbrewingco.com/?action=lostpassword">Lost your password</a>?'){
		        	error = 'Invalid password. <a href="javascript:;" onclick="lbc.modal.load(\'passforgot\');" title="Forgot Password">Lost your password</a>?';
	        	}
	        	if(error == '<strong>ERROR</strong>: The password you entered for the username <strong>'+ username +'</strong> is incorrect. <a href="http://localbrewingco.com/?action=lostpassword">Lost your password</a>?'){
		        	error = 'Invalid password. <a href="javascript:;" onclick="lbc.modal.load(\'passforgot\');" title="Forgot Password">Lost your password</a>?';
	        	}
	        	
	        	$('.modal .error').html(error).removeClass('nu-invisible');
						$('form#loginform input[type=submit]').removeAttr('disabled');
						$('.modal form .loader').addClass('hidden');
						$('.modal .reg_login, .modal .fb_login').removeClass('hidden');
	        	//lbc.error.hide();
	        	
	        	console.log(error);
	        	
	        }
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					console.log(errorThrown);
				}
			});
		},
		
		facebook: {
			
			getLoginStatus: function() {
				
				console.log('getting login status');
				console.log(FB);
				
		    FB.getLoginStatus(function(response) {
		      if (response.status == 'connected') {
		      	console.log('logged in');
		      
						// this part auto-logs in someone with facebook if theyve already said yes
		      	lbc.user.facebook.getUserCredentials(response);
		      
		      } else {	          	
		      	
		      	console.log('not logged in');
		      	
		      	lbc.user.facebook.login();
	
		      }
		    });
		  },
		  
		  getUserCredentials: function(response){
		    FB.api('/me', function(response) {
		    
					var FBid = response.id;
					var FBemail = response.email;
					
					lbc.user.login(FBid, FBemail, 'fb');
		    });
		  },
		  
		  login: function() {
		    FB.login(function(response) {
		       //console.log(response.status);
		       //if (response.session) {
		       if(response.status == "connected"){
		       	console.log('logged in');
		       
		       	lbc.user.facebook.getUserCredentials(response);
		       
		       } else {
		       	console.log('not logged in');
		       }
		     }, { scope: "email" });
		  }
			
		},
		
		success: function(id, callback){
			console.log(id);
			
			// Set a LS variable for user ID
			localStorage.setItem('lbc_user', id);
			
			location.href = callback + '?loggedin=true';		
		},
		
		logout: function(x){
			$.ajax({
				type: 'POST',
				url: '/cms/wp-admin/admin-ajax.php',
				dataType: 'jsonp',
				data: {
				  action: 'ajaxLogout',
				  logout: x
				},
				success: function(result){
					console.log(result);
					
					if(result == "success" || result.substring(0, result.length - 1) == "success"){
						// Remove LS userid
						localStorage.removeItem('lbc_user');
						
						// If successful, reload the page
						location.href = '/';
	        }
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					console.log(errorThrown);
				}
			});
		}
		
	};
	
	
	/***************************/
	
	
	/* Home */
	if( $('body').hasClass('home') ){
		
		var count = $('#hero .item');
		console.log(count.length);
		
		$('#hero .carousel-indicators').empty();
		
		if(count.length < 2){
			$('#hero .carousel-indicators').hide();
		}
		
		for(var x=0; x<count.length; x++){
			$('#hero .carousel-indicators').append('<li data-target="#carousel-example-generic" data-slide-to="'+ x +'"></li>');
		}
		
		$('#hero .carousel-indicators li').first().addClass('active');
    
	}
	
	/* Beer */
	if( $('body').hasClass('beer') ){
		
	}
	
	
	/* Map */
	
	if( $('body').hasClass('map') ){
		
		function relocate_map(){
			if( $(window).width() > 767 ){
		    location.href = 'http://localbrewingco.com/beer/#/find';
	    }
    }
    
    $(window).resize(function(){
	    relocate_map();
    });
		
		relocate_map();
		
	}
	
	
	/* Learn */
	if( $('body').hasClass('learn') ){
		
		if( $('body').hasClass('p-fullsize') ){
		
			var elHeight = $('#workshops .left').outerHeight(true);
			$('#workshops .right').css('height', elHeight);
			
			$(window).load(function(){
				$('#workshops .right').mCustomScrollbar({
					scrollInertia:150,
					axis: 'y',
					preventDefault: 0,
				});
			});
			
			$(window).resize(function(){
				var elHeight = $('#workshops .left').outerHeight(true);
				$('#workshops .right').css('height', elHeight);
			});
		
		}
		
	}
	
	
	
	/* Distributors */
	if( $('body').hasClass('distributors') ){
		
		console.log('distributors');
		
		var login = $(".distributors #loginform");
		
		$(document).on('submit', '.distributors #loginform', function(e){
			e.preventDefault();
			
			var u = $('.distributors #loginform #user_login');
			var p = $('.distributors #loginform #user_pass');
			
			var username = u.val();
			var password = p.val();
				
			$('form#loginform input[type=submit]').attr('disabled', 'disabled');
	    
			$.ajax({
				type: 'GET',
				url: '/cms/wp-admin/admin-ajax.php',
				dataType: 'jsonp',
				data: {
				  action: 'ajaxLogin',
				  user_login: username,
	        user_password: password
				},
				success: function(result){
					//console.log(result);
					
					var obj = result;
					
					if(obj.status == "success"){
					
						var userID = obj.data.userid;
						//console.log(userID);
						
						// If successful, load the users itinerary
						location.href = document.URL;		
						
	        } else {
	        	console.log('fail!!');
	        	
	        	// Error handling
	        	var error = obj.error;
	        	
						$('form#loginform input[type=submit]').removeAttr('disabled');
	        	
	        }
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					console.log(errorThrown);
				}
			});
			
		});
		
	}
	
	
	/* Events */
	function setHash(i){
		// Change hash to current
	  if (Modernizr.history) {
		  var newurl = window.location.protocol + "//" + window.location.host + '/' + (location.pathname+location.search).substr(1) + '#/'+ page_sections[i];
			window.history.pushState({path:newurl},'',newurl);		  
		}
	}
	
	$(window).on('scroll', function (e) {
		var top = $(this).scrollTop();
		var values = [];
		
		for (var i = 0; i < page_sections.length; i++) {
			
			var offset = $('main section#'+ page_sections[i]).offset().top - 250;
			values.push(offset);
		  
		}
		
		for (var i = 0; i < values.length; i++) {
		
			//console.log(values[i]);
			//console.log(values[i+1]);
			
			//console.log(i + 1);
		  
		  if(top <= values[0]){
			  $('nav.sub li').removeClass('active');
			  
			  //setHash(i);
		  }
		  
		  if((top >= values[i]) && (top < values[i+1])){
			  //console.log('match: '+ values[i]);
			  //console.log('match: '+ page_sections[i]);
			  
			  $('nav.sub li').removeClass('active');
			  $('nav#'+ page + '.sub li.'+ page_sections[i]).addClass('active');
			  
			  //setHash(i);
		  }
		  
		  if(top >= values[values.length-1]){
			  $('nav.sub li').removeClass('active');
			  $('nav#'+ page + '.sub li.'+ page_sections[i]).addClass('active');
			  
			  //setHash(i);
		  }
		  
		}
		
		//console.log(page);
		//console.log(top);
	});
	
	$(window).on('hashchange', function (e) {
		e.preventDefault();
		
		console.log('hashchange');
		
		goToSection();
	});
	
	$(window).on('load', function () {
		goToSection();
	});
	
	$(window).smartresize(function () {
		goToSection();
	});	

	
	// Cookies
	jQuery.cookie=function(e,b,a){if(1<arguments.length&&(null===b||"object"!==typeof b)){a=jQuery.extend({},a);null===b&&(a.expires=-1);if("number"===typeof a.expires){var d=a.expires,c=a.expires=new Date;c.setDate(c.getDate()+d)}return document.cookie=[encodeURIComponent(e),"=",a.raw?String(b):encodeURIComponent(String(b)),a.expires?"; expires="+a.expires.toUTCString():"",a.path?"; path="+a.path:"",a.domain?"; domain="+a.domain:"",a.secure?"; secure":""].join("")}a=b||{};c=a.raw?function(a){return a}:
decodeURIComponent;return(d=RegExp("(?:^|; )"+encodeURIComponent(e)+"=([^;]*)").exec(document.cookie))?c(d[1]):null};

	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	// Placeholders
	//Placeholders.init();
		
	// Age Check Confirm
	$(document).on('click', '.agecheck .confirm', function(){
		var LOCAL_COOKIE = 'agecheck-cookie';
	  $.cookie(LOCAL_COOKIE, 'accepted', { path: '/', expires: 360 });
		$('.agecheck.intro').hide();
	});
	
});
