<?php 
	/**
	 * Template Name: Learn
	 */
	 
	include('header.php'); 
?>
        
  <main>
  
  	<?php
		$temp = $wp_query;
		$wp_query = null;
		$args = array( 'post_type' => 'page', 'pagename' => 'learn' );
		$wp_query = new WP_Query($args);
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		$post_title = $post->post_title;
		$image = get_field('learn_background_image');
		?>    
  	
  	<section id="masthead" style="background: url(<?php echo $image['url']; ?>) 50% 50% no-repeat; background-size:cover;">
    	<div class="content">
    		<h1 class="visibility-web"><?php echo $post_title; ?></h1>
    	</div>
  	</section>
  	
  	<?php 
		endwhile; 
		wp_reset_postdata(); 
		?>
  	
  	<section id="live-local">
    	<div class="content">
	    	
	    	<?php
				$temp = $wp_query;
				$wp_query = null;
				$args = array( 'post_type' => 'page', 'pagename' => 'learn' );
				$wp_query = new WP_Query($args);
				while ($wp_query->have_posts()) : $wp_query->the_post();
				
				$statement = get_field('statement');			
				?>
				
					<h2>Live. Local.</h2>
	    		
	    		<svg class="linebreak" width="100%" height="10px">
						<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
					</svg>
		    	
	        <div class="left"> 
		    	
		    	<?php
		    	if(get_field('sections')):
						while(has_sub_field('sections')):
						
						$headline = get_sub_field('headline');
						$text = get_sub_field('text');
						?>
	            
	            <div class="text">
		            <div class="title"><?php echo $headline; ?></div>
		            <div class="description"><?php echo $text; ?></div> 
		          </div>
								
						<?php
						endwhile;
					endif; 
					?>
					
					<div class="text">
	        	<div class="red title"><span class="red"><?php echo $statement; ?></span></div>
	        </div>
      	
      	<?php 
				endwhile; 
				wp_reset_postdata(); 
				?>
            
      	</div>
        <div class="right">
          <div class="pic_1 pic">
	          <img src="img/pic_1.png">
          </div>
          <div class="pic_2 pic">
	          <img src="img/pic_2.png">
          </div>
          <div class="pic_3 pic">
	          <img src="img/pic_3.png">
          </div>
          <div class="pic_4 pic">
	          <img src="img/pic_4.png">
          </div>
        </div>
    </div>
  	</section>
  	
  	<section id="team">
	  	
	  	<div class="content">
	  	
		  	<h2>Our Team</h2>
	  		
	  		<svg class="linebreak" width="100%" height="10px">
					<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
				</svg>
				
				<div class="grid">
					<div class="col-1">
						<div class="item">
							
							<?php
							$temp = $wp_query;
							$wp_query = null;
							$args = array( 
								'post_type' => 'team', 
								'Title' => 'Brewmaster',
								'tax_query' => array(
									array(
										'taxonomy' => 'Title',
										'field'    => 'slug',
										'terms'    => 'brewmaster',
										'operator' => 'IN'
									),
								) 
							);
							$wp_query = new WP_Query($args);
							while ($wp_query->have_posts()) : $wp_query->the_post();
							
							$name = $post->post_title;
							$bio = get_field('bio');
							$date_started = get_field('date_started');
							$image = get_field('headshot');
							$title = get_the_terms( $post->ID, 'Title' );	
							
							foreach($title as $job_title){
								$job_title = $job_title->name;
							}			
							?>
							
							<div class="image">
								<img src="<?php echo $image['url']; ?>">
							</div>
							
							<svg class="linebreak" width="100%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							
							<div class="name"><?php echo $name; ?></div>
							<div class="title"><?php echo $job_title; ?> <span>Since <?php echo substr($date_started, 0, 4); ?></span></div>
							<div class="description"><?php echo $bio; ?></div>
			      	
			      	<?php 
							endwhile; 
							wp_reset_postdata(); 
							?>
							
						</div>
					</div>
					<div class="col-3">
						
						<?php
						$temp = $wp_query;
						$wp_query = null;
						$args = array( 
							'post_type' => 'team', 
							'posts_per_page' => -1, 
							'tax_query' => array(
								array(
									'taxonomy' => 'Title',
									'field'    => 'slug',
									'terms'    => 'brewmaster',
									'operator' => 'NOT IN'
								),
							),
							'orderby' => 'menu_order',
						);
						$wp_query = new WP_Query($args);
						while ($wp_query->have_posts()) : $wp_query->the_post();
						
						$name = $post->post_title;
						$bio = get_field('bio');
						$date_started = get_field('date_started');
						$image = get_field('headshot');
						$title = get_the_terms( $post->ID, 'Title' );	
						
						foreach($title as $job_title){
							$job_title = $job_title->name;
						}			
						?>
						
						<div class="item">
							
							<div class="image">
								<img src="<?php echo $image['url']; ?>">
							</div>
							
							<svg class="linebreak" width="100%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							
							<div class="name"><?php echo $name; ?></div>
							<div class="title"><?php echo $job_title; ?></span></div>
							<div class="description"><?php echo $bio; ?></div>
							
						</div>
		      	
		      	<?php 
						endwhile; 
						wp_reset_postdata(); 
						?>
						
					</div>
				</div>
			
	  	</div>
	  	
  	</section>
  	
  	<?php
		$temp = $wp_query;
		$wp_query = null;
		$args = array( 'post_type' => 'guestbrewer', 'posts_per_page' => -1 );
		$wp_query = new WP_Query($args);
		if ($wp_query->have_posts()) :
		?>
  	
  	<section id="guest-brewers">
	  	
	  	<div class="content">
		  	
		  	<h2>Guest Brewers</h2>
		  	
		  	<div class="list">
			  	
			  	<?php
					while ($wp_query->have_posts()) : $wp_query->the_post();
					
					$name = $post->post_title;
					$description = get_field('description');
					$date = get_field('date_brewed');
					$image = get_field('photo');
					$beer_name = get_field('beer_brewed');
					
					if( $beer_name ):
						foreach( $beer_name as $beer ):
						    $beer_name = $beer->post_title;
						endforeach;
					endif;
					
					if(!$beer_name || empty($beer_name)){
						$beer_name = "???????";
					}	
					?>
					
					<div class="item clearfix">
				  	<div class="left">
				  		<div class="image">
					  		<img src="<?php echo $image['url']; ?>">
				  		</div>
			  		</div>
			  		<div class="right">
				  		<div class="name"><?php echo $name; ?></div>
				  		<svg class="linebreak" width="50%" height="10px">
								<line x1="7" x2="100%" y1="2" y2="2" stroke="#fafafa" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
							</svg>
							<div class="beer">
								<div class="brewed">Beer brewed:</div>
								<?php if(!empty($date)){ ?>
									<span class="date"><?php echo date('m/d/Y', strtotime($date)); ?></span>: <span class="name"><?php echo $beer_name; ?></span>
								<?php 
									} else {
								?>
									<span class="name"><?php echo $beer_name; ?></span>
								<?php
									}
								?>
							</div>
							<div class="description"><?php echo $description; ?></div>
			  		</div>
			  	</div>
	      	
	      	<?php 
					endwhile; 
					?>
			  	
		  	</div>
		  	
	  	</div>
	  	
  	</section>
  	
  	<?php 
		endif; 
		wp_reset_postdata(); 
		?>
  	
  	<section id="events" class="visibility-web">
	  	
	  	<div class="content">
		  	<div class="left">
			  	
			  	<?php
					$temp = $wp_query;
					$wp_query = null;
					$args = array( 'post_type' => 'page', 'pagename' => 'learn' );
					$wp_query = new WP_Query($args);
					while ($wp_query->have_posts()) : $wp_query->the_post();
					
					$headline = get_field('learn_workshops_headline');
					$description = get_field('learn_workshops_description');			
					?>
					
					<h2><?php echo $headline; ?></h2>
			  	<p><?php echo $description; ?></p>
	      	
	      	<?php 
					endwhile; 
					wp_reset_postdata(); 
					?> 	
			  	
		  	</div>
		  	<div class="right">
			  	<h2>Brewery Events</h2>
			  	
			  	<div class="list">
				  	
				  	<?php
						$temp = $wp_query;
						$wp_query = null;
						$args = array( 
							'post_type' => 'workshops', 
							'posts_per_page' => -1,
							'meta_key'	=> 'date',
							'orderby'	=> 'meta_value_num', 
							'order'		=> 'ASC',
						);
						$wp_query = new WP_Query($args);
						if($wp_query->have_posts()) :
							while ($wp_query->have_posts()) : $wp_query->the_post();
							
							$name = $post->post_title;
							$description = get_field('description');
							$date = DateTime::createFromFormat('Ymd', get_field('date'));
							$link = get_field('url');
							
							$current_date = date('U', strtotime('now'));
							
							if($date->format('U') >= $current_date){
							?>
							
							<div class="item">
						  	<div class="event">
									<span class="date"><?php echo $date->format('m/d/Y'); ?></span>: <span class="name"><?php echo $name; ?></span> <?php if(!empty($link)){ ?><a href="<?php echo $link; ?>" target="_blank">Link</a><?php } ?>
								</div>
								<div class="description"><?php echo $description; ?></div>
					  	</div>
			      	
			      	<?php
				      }
				       
							endwhile;
						else :
						?>
							
							<div class="not-found">No workshops currently scheduled. Check back soon!</div>
							
						<?php 
						endif;
						wp_reset_postdata(); 
						?>
				  	
			  	</div>
		  	</div>
	  	</div>
	  	
  	</section>
  	
  	<?php
		$temp = $wp_query;
		$wp_query = null;
		$args = array( 'post_type' => 'page', 'pagename' => 'learn' );
		$wp_query = new WP_Query($args);
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		$display = get_field('learn_display_video');
		if($display){
			$video_url = get_field('learn_video_url');
		}
		else {
			$hide = 'hidden';
		}
		?>    
  	
  	<section id="live" class="<?php echo $hide; ?> visibility-web">
    	<div class="content">
	    	<div class="video">
		    	<div class='embed-container'><iframe src='<?php echo $video_url; ?>' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
	    	</div>
    	</div>
  	</section>
  	
  	<?php 
		endwhile; 
		wp_reset_postdata(); 
		?>
  	
  </main>

<?php include('footer.php'); ?>