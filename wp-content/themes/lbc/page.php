<?php
get_header();
wp_reset_query();
?>
  
  <main>
	  
		<?php
		$temp = $wp_query;
		$wp_query = null;
		$args = array( 'post_type' => 'page', 'p' => get_the_ID(), 'posts_per_page' => 1 );
		$wp_query = new WP_Query($args);
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		$post_title = $post->post_title;
		$include_header = get_field('include_header');
		$header_image = get_field('header_image');
		?>
		
		<?php
			if($include_header){
		?>    
  	
  	<section id="masthead"  style="background: url(<?php echo $header_image['url'] ?>) 50% 50% no-repeat; background-size:cover;">
    	<div class="content">
    		<h1><?php echo $post_title; ?></h1>
    	</div>
  	</section>
  	
  	<?php
	  	}
	  ?>
	  
		<section id="page">
			
			<div class="content">
			
				<?php the_content(); ?>
	    
			</div>
	  	
		</section>
		
		<?php 
		endwhile; 
		wp_reset_postdata(); 
		?>
		
  </main>
        
<?php 
get_footer(); 
?>
