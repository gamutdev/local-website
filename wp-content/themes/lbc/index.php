<?php get_header(); ?>
        
  <main>
  
  	<section id="hero">
	  	
	  	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		  	
			  <ol class="carousel-indicators"></ol>
			
			  <div class="carousel-inner" role="listbox">
				  
				  <?php
					$temp = $wp_query;
					$wp_query = null;
					$args = array( 'post_type' => 'page', 'pagename' => 'homepage' );
					$wp_query = new WP_Query($args);
					while ($wp_query->have_posts()) : $wp_query->the_post();
					
					// Post specific	
					$post_id = $post->ID;
					$post_title = $post->post_title;
					$post_slug = $post->post_name;
					
					if(get_field('features')):
						while(has_sub_field('features')):
						
						$headline = get_sub_field('headline');
						$subhead = get_sub_field('subhead');
						$image = get_sub_field('background_image');
						?>
	            
	            <div class="item active" style="background: url(<?php echo $image['sizes']['large']; ?>) 50% 50% no-repeat; background-size:cover;">
					      <div class="carousel-caption">
					        <div class="content">
						    		<h1><?php the_sub_field('headline'); ?></h1>
						    		<?php
							    	if(!empty($subhead)){
							    	?>
						    		<h3><?php echo $subhead; ?></h3>
						    		<?php
							    	}

										if(get_sub_field('buttons')):

											echo '<div class="buttons">';

											while(has_sub_field('buttons')):
											
											$cta = get_sub_field('button_text');
											$link = get_sub_field('link');
											$classes = get_sub_field('classes');
											?>
												
												<a class="launch large <?php echo $classes; ?>" href="<?php echo $link; ?>"><?php echo $cta; ?></a>
													
											<?php 
											endwhile;

											echo '</div>';

										endif;
										?>
						    	</div>
					      </div>
					    </div>
								
						<?php 
						endwhile;
					endif; 
					?>
          
          <?php 
					endwhile; 
					wp_reset_postdata(); 
					?>
			  </div>
			
			  <!-- Controls -->
			  <!--
			  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			  -->
			</div>
	  	
  	</section>
	    	
  	<?php
		$temp = $wp_query;
		$wp_query = null;
		$args = array( 'post_type' => 'page', 'pagename' => 'homepage' );
		$wp_query = new WP_Query($args);
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		// Post specific	
		$post_id = $post->ID;
		$post_title = $post->post_title;
		$post_slug = $post->post_name;

		if( have_rows('callouts') ):
			while( have_rows('callouts') ) : the_row();

				$headline = get_sub_field('homepage_callout_headline');
				$text = get_sub_field('homepage_callout_text');
				$image = get_sub_field('homepage_callout_background_image');
				$button_cta = get_sub_field('button_cta');
				$button_type = get_sub_field('button_type');
				$shade = get_sub_field('add_shading');

				if(empty($headline)){
					$hide = 'hidden';
				}
				?>

				<section class="cta-block <?php if(!empty($shade)){ echo 'shaded'; } ?>" style="background: url(<?php echo $image['url']; ?>) 50% 50% no-repeat; background-size:cover;" class="<?php echo $hide; ?>">
					<div class="content">
						
						<div class="left">
							<h2><?php echo $headline; ?></h2>
							<h3><?php echo $text; ?></h3>

							<?php
							if($button_type = "URL"){
								$button_link = get_sub_field('button_link');
							?>

							<a 
								class="button launch large"
								href="<?php echo $button_link; ?>"
							><?php echo $button_cta; ?></a>

							<?php
							}
							else {
							?>

							<button 
								class="launch large"
								p-action-1="(click) call: openCurator"
							>Launch</button>

							<?php
							}
							?>
						</div>
					
					</div>
				</section>

				<?php
			endwhile;
		endif;

		endwhile; 
		wp_reset_postdata(); 
		?>	
  	
  	<section id="learn">
    	<div class="content">
	    	
	    	<?php
				$temp = $wp_query;
				$wp_query = null;
				$args = array( 'post_type' => 'page', 'pagename' => 'homepage' );
				$wp_query = new WP_Query($args);
				while ($wp_query->have_posts()) : $wp_query->the_post();
				
				// Post specific	
				$post_id = $post->ID;
				$post_title = $post->post_title;
				$post_slug = $post->post_name;
				
				$headline = get_field('homepage_callout_headline');
				$text = get_field('homepage_callout_text');
				$image = get_field('homepage_callout_background_image');
				?>
				
				<h2>Let's Brew This Together</h2>
    		
    		<svg class="linebreak" width="100%" height="10px">
					<line x1="7" x2="100%" y1="2" y2="2" stroke="#acacac" stroke-width="2" stroke-linecap="round" stroke-dasharray="1, 8" style="stroke: #acacac;"></line>
				</svg>
    		
    		<div class="items clearfix">
				
				<?php
				if(get_field('sections')):
					while(has_sub_field('sections')):
					
					$title = get_sub_field('title');
					$description = get_sub_field('description');
					$image = get_sub_field('image');
					$link = get_sub_field('link');		
					
					switch($link){
						case "shop":
							$clickthrough = "http://local-brewing-co-2.myshopify.com/";
							break;
						case "url":
							$clickthrough = get_sub_field('link_url');
							break;
						default:
							$clickthrough = home_url() .'/'. $link;
							break;
					}

					?>
            
            <a class="item" href="<?php echo $clickthrough; ?>">
	        		<div class="image">
		        		<img src="<?php echo $image['url']; ?>">
	        		</div>
	        		<div class="title"><?php echo $title; ?></div>
	        		<div class="description"><?php echo $description; ?></div>
	      		</a>
							
					<?php 
					endwhile;
				endif; 
				?>
				
    		</div>
		    
		    <?php 
				endwhile; 
				wp_reset_postdata(); 
				?>
				
    	</div>
  	</section>
  	
  	<?php
		$temp = $wp_query;
		$wp_query = null;
		$args = array( 'post_type' => 'page', 'pagename' => 'homepage' );
		$wp_query = new WP_Query($args);
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		// Post specific	
		$post_id = $post->ID;
		$post_title = $post->post_title;
		$post_slug = $post->post_name;
		
		$display = get_field('homepage_display_video');
		if($display){
			$video_url = get_field('homepage_video_url');
			
			if( !strstr($video_url, 'player') ){
				$video_id = str_replace("https://vimeo.com/", "", $video_url);
				$video_url = "https://player.vimeo.com/video/" . $video_id;
			}
		}
		else {
			$hide = 'hidden';
		}
		?>    
  	
  	<section id="story" class="<?php echo $hide; ?>">
    	<div class="content">
	    	
	    	<div class="video">	
		    	<div class='embed-container'><iframe src='<?php echo $video_url; ?>' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
	    	</div>
    	</div>
  	</section>
  	
  	<?php 
		endwhile; 
		wp_reset_postdata(); 
		?>
  	
  </main>

<?php get_footer(); ?>