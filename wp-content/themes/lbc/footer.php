    <footer>
      <div class="content">
	      
	      <div class="visibility-tablet">
		      <div class="contact">
						<p class="address clearfix">
							<a href="http://maps.apple.com/?daddr=69+Bluxome+St+San+Francisco,+CA+94107">
								<img src="img/home-address.png">
							</a>
						</p>
						<div class="hours">
							<h5>Open daily</h5>
							<ul class="leaders">
								
								<?php
								$temp = $wp_query;
								$wp_query = null;
								$args = array( 'post_type' => 'page', 'pagename' => 'general' );
								$wp_query = new WP_Query($args);
								while ($wp_query->have_posts()) : $wp_query->the_post();
								
								if(get_field('hours')):
									while(has_sub_field('hours')):
									?>
								
									<li><span><?php the_sub_field('day'); ?></span><span><?php the_sub_field('time'); ?></span></li>
											
									<?php 
									endwhile;
								endif; 
								?> 
								
								<?php 
								endwhile; 
								wp_reset_postdata(); 
								?>

							</ul>
							
							<?php
							$temp = $wp_query;
							$wp_query = null;
							$args = array( 'post_type' => 'page', 'pagename' => 'general' );
							$wp_query = new WP_Query($args);
							while ($wp_query->have_posts()) : $wp_query->the_post();
							
							$phone = get_field('phone');
							?>
							
							<a class="phone" href="tel: <?php echo $phone; ?>"><?php echo $phone; ?></a>
							
							<?php 
							endwhile; 
							wp_reset_postdata(); 
							?>
							
						</div>	
					</div>
	      </div>
        
        <div class="left">
	        <nav class="visibility-web">
		        <ul>
			        <li class="header"><a href="<?php echo home_url(); ?>/">Home</a></li>
			        <li class="header"><a href="https://www.enjoylocalbeer.com/" target="_blank">Shop</a></li>
			        <li class="header"><a href="<?php echo get_page_link( get_page_by_path( 'contact' ) ); ?>/">Contact</a></li>
		        </ul>
		        <ul>
			        <li class="header"><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>/">Our Beer</a></li>
			        <li><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>#/beerlist">Beer List</a></li>
			        <li><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>#/curator">Beer Curator</a></li>
			        <li><a href="<?php echo get_page_link( get_page_by_path( 'beer' ) ); ?>#/find">Find</a></li>
		        </ul>
		        <ul>
			        <li class="header"><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>/">Learn</a></li>
			        <li><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/live-local">Live. Local.</a></li>
			        <li><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/team">Our Team</a></li>
			        <li><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/guest-brewers">Guest Brewers</a></li>
			        <li><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/workshops">Workshops</a></li>
			        <li><a href="<?php echo get_page_link( get_page_by_path( 'learn' ) ); ?>#/live">Live Feed</a></li>
		        </ul>
		        <ul>
			        <li class="header"><a href="<?php echo get_page_link( get_page_by_path( 'jobs' ) ); ?>/">Jobs</a></li>
			        <li class="header"><a href="<?php echo get_page_link( get_page_by_path( 'distributors' ) ); ?>/">Distributors</a></li>
			        <li class="header p-invisible"><a href="<?php echo get_page_link( get_page_by_path( 'press' ) ); ?>/">Press</a></li>
		        </ul>
	        </nav>
	        
	        <ul class="socials">
		        
		        <?php
						$temp = $wp_query;
						$wp_query = null;
						$args = array( 'post_type' => 'page', 'pagename' => 'general' );
						$wp_query = new WP_Query($args);
						while ($wp_query->have_posts()) : $wp_query->the_post();
						
						if(get_field('social_media')):
							while(has_sub_field('social_media')):
							
							$service = get_sub_field('service');
							$url = get_sub_field('url');
							?>
							
							<?php
							if(strtolower($service) == "facebook"){
							?>
							<li><a class="<?php echo strtolower($service); ?> ding" href="<?php echo $url; ?>" target="_blank">G</a></li>
							<?php
							}
							?>
							<?php
							if(strtolower($service) == "twitter"){
							?>
		        	<li><a class="<?php echo strtolower($service); ?> ding" href="<?php echo $url; ?>" target="_blank">t</a></li>
		        	<?php
							}
							?>
		        	<?php
							if(strtolower($service) == "instagram"){
							?>
		        	<li><a class="<?php echo strtolower($service); ?> icon-instagram" href="<?php echo $url; ?>" target="_blank"></a></li>
		        	<?php
							}
							?>
		        	<?php
							if(strtolower($service) == "untappd"){
							?>
		        	<li><a class="<?php echo strtolower($service); ?> icon-untappd" href="<?php echo $url; ?>" target="_blank"></a></li>
		        	<?php
							}
							?>
									
							<?php 
							endwhile;
						endif; 
						?> 
						
						<?php 
						endwhile; 
						wp_reset_postdata(); 
						?>
						
	        </ul>
	        
	        <div class="mailing">
		        <form action="http://localbrewingco.us7.list-manage.com/subscribe/post?u=6d79c2a11ade5ff422b6c3416&amp;id=f71339c37f" method="POST">
			        <label for="email">Get on our mailing list</label>
			        <input type="email" id="email" placeholder="Email Address">
			        <button>JOIN</button>
		        </form>
	        </div>
	        
	        <div class="copyright">&copy; <?php echo date("Y"); ?> Local Brewing Co. All Rights Reserved.</div>
        </div>
        
        <div class="right">
	        <div class="contact">
						<p class="address clearfix">
							<a href="http://maps.apple.com/?daddr=69+Bluxome+St+San+Francisco,+CA+94107">
								<img src="img/home-address.png">
							</a>
						</p>
						<div class="hours">
							<h5>Open daily</h5>
							<ul class="leaders">
								
								<?php
								$temp = $wp_query;
								$wp_query = null;
								$args = array( 'post_type' => 'page', 'pagename' => 'general' );
								$wp_query = new WP_Query($args);
								while ($wp_query->have_posts()) : $wp_query->the_post();
								
								if(get_field('hours')):
									while(has_sub_field('hours')):
									?>
								
									<li><span><?php the_sub_field('day'); ?></span><span><?php the_sub_field('time'); ?></span></li>
											
									<?php 
									endwhile;
								endif; 
								?> 
								
								<?php 
								endwhile; 
								wp_reset_postdata(); 
								?>

							</ul>
							
							<?php
							$temp = $wp_query;
							$wp_query = null;
							$args = array( 'post_type' => 'page', 'pagename' => 'general' );
							$wp_query = new WP_Query($args);
							while ($wp_query->have_posts()) : $wp_query->the_post();
							
							$phone = get_field('phone');
							?>
							
							<a class="phone" href="tel: <?php echo $phone; ?>"><?php echo $phone; ?></a>
							
							<?php 
							endwhile; 
							wp_reset_postdata(); 
							?>
							
						</div>	
					</div>
        </div>
        
    		<div class="logo">
      		<div p-view="logo:h"></div>
    		</div>
    	</div>
    </footer>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-45025796-1');ga('send','pageview');
    </script>

		<?php
		$footer_scripts = get_field('footer_scripts', 191);
		if(!empty($footer_scripts)){
			echo $footer_scripts;
		}
		?>
    
  </body>
</html>