/**
*
* @license
* Copyright � 2013 Jason Proctor.  All rights reserved.
*
**/

var	MenuView = function ()
{
	positron.View.call (this);
};

positron.inherits (MenuView, positron.View);

MenuView.prototype.onDOMReady = function ()
{
	console.log ("MenuView.onDOMReady()");
	
	$('body').addClass('stopScroll');
}

MenuView.prototype.onBeforeInvisible = function ()
{
	console.log ("MenuView.onBeforeInvisible()");
	
	$('body').removeClass('stopScroll');
}