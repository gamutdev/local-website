/**
*
* @license
* Copyright � 2013 Jason Proctor.  All rights reserved.
*
**/

var	MapView = function ()
{
	//console.log ("OverlayView()");
	
	positron.View.call (this);
};

positron.inherits (MapView, positron.View);

MapView.prototype.onDOMReady = function ()
{
	console.log ("MapView.onDOMReady()");
	
	var section = this.params.section;
	
	// Map
	var map, lbc;
  var geocoder, markers;
  var MY_MAPTYPE_ID = 'sfbrewersguild';
	var markersArray = [];
	var latlng = [];
	var infos = [];
	var available = [];
	var order = 1;
	var globals = new Object();
			globals.zip = "";
			globals.account = "On Tap";
			globals.region = "";
			globals.beername = "";
  
  function initialize() {
	  
	  var stylez = [
	    {
		    "stylers": [
		      { "saturation": -100 }
		    ]
		  },{
		    "stylers": [
		      { "weight": 0.3 }
		    ]
		  },{
		    "featureType": "water",
		    "stylers": [
		      { "lightness": -65 }
		    ]
		  },{
		    "featureType": "road"  }
	  ];
	  
  	geocoder = new google.maps.Geocoder();
  	lbc = new google.maps.LatLng(37.776490,-122.397160);	
    var myOptions = {
	    zoom: 14,
	    center: lbc,
	    scrollwheel: false,
	    mapTypeControl: false,
	    mapTypeControlOptions: {
		    mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
		  },
		  panControl: false,
	    panControlOptions: {
	        position: google.maps.ControlPosition.TOP_RIGHT
	    },
	    streetViewControl: false,
	    zoomControl: true,
	    zoomControlOptions: {
	        style: google.maps.ZoomControlStyle.SMALL,
	        position: google.maps.ControlPosition.TOP_RIGHT
	    },
			draggable: true,
	    mapTypeId: MY_MAPTYPE_ID,
    };

    map = new google.maps.Map(document.getElementById("map"),myOptions);
    
    google.maps.Map.prototype.setCenterWithOffset= function(latlng, offsetX, offsetY) {
		    var map = this;
		    var ov = new google.maps.OverlayView();
		    ov.onAdd = function() {
		        var proj = this.getProjection();
		        var aPoint = proj.fromLatLngToContainerPixel(latlng);
		        aPoint.x = aPoint.x+offsetX;
		        aPoint.y = aPoint.y+offsetY;
		        map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
		    }; 
		    ov.draw = function() {}; 
		    ov.setMap(this); 
		};
    
    if(section == "beer"){
    
	    google.maps.event.addDomListener(window, 'resize', function() {
		    var width = $('.beer main section#find #results').width();
		    var offset = ($(window).width() * 20) / 100;
				map.setCenterWithOffset(lbc, -(width - offset), 0);
			});
		 
		 var width = $('.beer main section#find #results').width();
		 console.log(width);
		 var offset = ($(window).width() * 20) / 100;
		 map.setCenterWithOffset(lbc, -(width - offset), 0);
	 
	 }
	 else if(section == "beermap"){
		 
		 google.maps.event.addDomListener(window, 'resize', function() {
		    map.setCenter(lbc);
		 });
		 
		 map.setCenter(lbc);
		 
	 }
	 else {
		 google.maps.event.addDomListener(window, 'resize', function() {
		    map.setCenter(lbc);
		    map.panBy(0, -100);
		 });
		 
		 map.setCenter(lbc);
	 }
    
    if((section == "beer") || (section == "beermap")){
    	var lbc_icon = 'img/map-pin.png';
    }
    else {
	    var lbc_icon = 'img/home-map-pin.png';
	    map.panBy(0, -100);
    }
    
		var localMarker = new google.maps.Marker({
		    position: lbc,
		    map: map,
		    title:"Local Brewing Co.",
		    visible: true,
		    icon: lbc_icon
		});
		
		 var styledMapOptions = {
		    name: 'SFBG'
		  };
		        
		  var guildMapType = new google.maps.StyledMapType(stylez, styledMapOptions);
		  map.mapTypes.set(MY_MAPTYPE_ID, guildMapType);         
  }
  
  
	if((section == "beer") || (section == "beermap")){
	  
	  // Sets the map on all markers in the array.
		function setAllMap(map) {
		  for (var i = 0; i < markersArray.length; i++) {
		    markersArray[i].setMap(map);
		  }
		}
		
		// Removes the markers from the map, but keeps them in the array.
		function clearMarkers() {
		  setAllMap(null);
		}
		
		// Shows any markers currently in the array.
		function showMarkers() {
		  setAllMap(map);
		}
		
		// Deletes all markers in the array by removing references to them.
		function deleteMarkers() {
		  clearMarkers();
		  markersArray = [];
		}
	  
	  function closeInfos(){
			if(infos.length > 0){
				infos[0].set("marker",null);
				infos[0].close();
				infos.length = 0;
			}
	  }
	  
	  function getCurrentLocation(){
		  if (navigator.geolocation && navigator.geolocation.watchPosition)
			{
				console.log ("watching for position changes");
				
				navigator.geolocation.watchPosition
				(
					function (inPosition)
					{
						var latitude = inPosition.coords.latitude;
						var longitude = inPosition.coords.longitude;
	
						console.log ("watchPosition() returned location "
							+ latitude + "," + longitude);
						
						var currentLocation = new google.maps.LatLng(latitude,longitude);	
						
						var width = $('.beer main section#find #results').width();
					  console.log(width);
					  var offset = ($(window).width() * 20) / 100;
					  map.setCenterWithOffset(currentLocation, -(width - offset), 0);
						
						geocoder.geocode( { 'latLng': currentLocation}, function(results, status) {
				      if (status == google.maps.GeocoderStatus.OK) {
				      	
				      	if (results[0]) {
	                  for(var i=0; i<results[0].address_components.length; i++)
	                  {
	                      var postalCode = results[0].address_components[i].types;
	                      if(postalCode == "postal_code"){
		                      code = i;
		                      globals.zip = results[0].address_components[code].short_name;
		                      findZip(results[0].address_components[code].short_name);
	                      }
	                   }
	               }
				        
				      } else {
				        //alert('Geocode was not successful for the following reason: ' + status);
				      }
				    });
						
					},
					function (inError)
					{
						console.error ("watchPosition() returns error:");
						console.error (inError.code + ": " + inError.message);
					},
					{
						enableHighAccuracy: true
					}
				);
			}
			else
			{
				console.error ("navigator.geolocation.watchPosition() not defined, no location services");
			}
	  }
	
	  
	  function findZip(address) {
		  order = 1;  	
	  	deleteMarkers();
	  	
	  	geocoder.geocode( { 'address': address}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
	      	var region = results[0].address_components[2].long_name;
	      	console.log(region);
	      	
	      	var width = $('.beer main section#find #results').width();
				  console.log(width);
				  var offset = ($(window).width() * 20) / 100;
				  map.setCenterWithOffset(results[0].geometry.location, -(width - offset), 0);
	
	        map.setZoom(12);
		    	
					$.ajax({
						url: 'https://localbrewingco.com/cms/json',
						data: 'type=locations&account='+ globals.account +'&zip='+ globals.zip +'&beername='+ globals.beername,
					  dataType: 'jsonp',
					  success: function(data) {
						  console.log(data.locations);
					  	
					  	var locations = data.locations;
					  	
					  	$('#find .tap-list .list').empty();
					  	
					  	$('.map-result span').html(globals.zip);
						  $('.map-result').removeClass('p-invisible');
						  
						  order = 1;
						  
						  for(var a=0; a < locations.length; a++){
							  plotPoints(locations[a], globals.beername);
						  }
						  
						  if(locations.length < 1){
							  $('#find .tap-list .list')
							  	.append('<div class="not-found">No locations found. Check back soon!</div>');
						  }
						  
						  $('#find .reset').removeClass('hidden');
					  }
					});
	        
	      } else {
	        //alert('Geocode was not successful for the following reason: ' + status);
	      }
	    });
	  }
	  
	  
	  function findBeer() {
		  order = 1;  	
	  	deleteMarkers();
	  	
	  	$.ajax({
				url: 'https://localbrewingco.com/cms/json',
				data: 'type=locations&account='+ globals.account +'&zip='+ globals.zip +'&beername='+ globals.beername,
			  dataType: 'jsonp',
			  success: function(data) {
				  console.log(data.locations);
			  	
			  	var locations = data.locations;
			  	
			  	$('#find .tap-list .list').empty();
			  	
			  	$('.map-result span').html(globals.beername);
				  $('.map-result').removeClass('p-invisible');
				  
				  order = 1;
				  
				  for(var a=0; a < locations.length; a++){
					  plotPoints(locations[a], globals.beername);
				  }
				  
				  if(locations.length < 1){
					  $('#find .tap-list .list')
					  	.append('<div class="not-found">No locations found. Check back soon!</div>');
				  }
			  }
			});
	  }
	  
	  
	  window.getAllLocations = function(){
		  order = 1;
	  	deleteMarkers();
	  	
	    $.ajax({
			  url: 'https://localbrewingco.com/cms/json',
			  data: 'type=locations&account='+ globals.account +'&zip=&beername=',
			  dataType: 'jsonp',
			  success: function(data) {
				  console.log(data.locations);
				  
				  var locations = data.locations;
				  
				  $('#find .tap-list .list').empty();
				  
				  $('.map-result span').html('All Locations');
				  $('.map-result').removeClass('p-invisible');
				  
				  order = 1;
				  
				  for(var a=0; a < locations.length; a++){
					  plotPoints(locations[a]);
				  }
				  
				  if(locations.length < 1){
					  $('#find .tap-list .list')
					  	.append('<div class="not-found">No locations found. Check back soon!</div>');
				  }
				
			  }
			});
			
			//$('#find .filters .filter').trigger('click');
	  }
	
	  
	  function plotPoints(location, x){
		  
		  console.log(location);
		  
		  var lat = location.lat;
			var lng = location.lng;
			var name = location.name;
			var address = location.address;
			var city = location.city;
			var state = location.state;
			var zip = location.zip;
			var phone = location.phone;
			var website = location.website;
			
			if(location.website && location.website != "undefined"){
				if (location.website.indexOf("http://") == -1 && location.website.indexOf("https://") == -1) {
					var website = 'http://'+ location.website;
				}
				var website_hide = "";
			}
			else {
				var website = "";
				var website_hide = "hidden";
			}
			
			var availability = location.availability;
			
			var ontap_list = [];
			
			// On Tap
			if(globals.account == "On Tap"){
				var ontap = location.ontap;		
				for(var b=0; b < ontap.length; b++){
					console.log(ontap[b]);
					if(ontap_list.indexOf(ontap[b].name) == -1){
						ontap_list.push(ontap[b].name);
					}
				}
			}		
			
			// In Stores
			else if(globals.account == "In Stores"){
				var retail = location.retail;
				for(var b=0; b < retail.length; b++){
					console.log(retail[b]);
					if(ontap_list.indexOf(retail[b].name) == -1){
						ontap_list.push(retail[b].name);
					}
				}
			}	
			
			// Both
			else {
				var ontap = location.ontap;		
				for(var b=0; b < ontap.length; b++){
					console.log(ontap[b]);
					if(ontap_list.indexOf(ontap[b].name) == -1){
						ontap_list.push(ontap[b].name);
					}
				}
				var retail = location.retail;
				for(var b=0; b < retail.length; b++){
					console.log(retail[b]);
					if(ontap_list.indexOf(retail[b].name) == -1){
						ontap_list.push(retail[b].name);
					}
				}
			}
			
			if(ontap_list.length > 0){
				 var taplist = ontap_list.join("</span>, <span class=\"beer\">");
			}
			else {
				 var taplist = '';
			}
			
			console.log(ontap_list);
	  	
	  	var template = '<div class="beer-sum" data-order="'+ order +'">'+
	  										'<div class="order">'+ order +'</div>'+
												'<div class="info">'+
													'<h2 class="name">'+ name +'</h2>'+
													'<div class="description">'+ taplist +'</div>'+
												'</div>'+
											'</div>';
	  	
	  	$('#find .tap-list .list').append(template);
	  	
			
	  	var contentString = '<div id="content">'+
			    '<div id="siteNotice">'+
			    '</div>'+
			    '<h2 style="font-size:16px; color:black; margin-bottom:10px;">'+ name +'</h2>'+
			    '<div id="bodyContent">'+
			    	'<p style="font-size:12px; color:black;">'+ address +'<br>'+ city +', '+ state +' '+ zip +'<br><a class="'+ website_hide +'" href="'+ website +'" target="_blank">'+ website +'</a></p>'+
			    	//'<p style="font-size:12px; font-weight:600;">Available here:<br/>'+
			    	//'<span style="font-weight:400;">'+ details +'</span></p>'+
			    '</div>'+
			    '</div>';
			
			var infowindow = new google.maps.InfoWindow({
			    content: contentString
			});
			
			if(section == "beer"){
				var width = $('.beer main section#find #results').width();
			  console.log(width);
			  var offset = ($(window).width() * 20) / 100;
			  map.setCenterWithOffset(lbc, -(width - offset), 0);
		  }
	
	    map.setZoom(12);
	
	    	
	    var markerpos = new google.maps.LatLng(lat, lng);
	    var marker = new google.maps.Marker({
	        map: map,
	        position: markerpos,
	        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+ order +'|000000|FFFFFF'
	    });
	    
	    latlng.push(markerpos);
	    markersArray.push(marker);
	    
	    order++;
	    
	    setAllMap(map); 
			
	    
	    google.maps.event.addListener(marker, 'click', function() {          
		    if (!infowindow.getMap()) {
	        infowindow.open(map, marker);
	      } else {
	        infowindow.close();
	      };
	      
	      closeInfos();  
	      infos[0]=infowindow;
	    });
	    
	    if(x && (x != "")){
		    $('#find .tap-list .list .beer-sum .description span').removeClass('active');
		    $('#find .tap-list .list .beer-sum .description span').each(function(){
			    if( $(this).html().toLowerCase() == x.toLowerCase() ){
				    $(this).addClass('active');
			    }
		    });
	    }
	  	    	
	  }
		
	  
	  $(document).on('click', '#find .beer-sum', function(){
		  
		   var markerID = $(this).data('order');
			 var marker = markersArray[markerID - 1];
		   var markerlatLng = marker.getPosition();
			 
			 if(section == "beer"){
				 var width = $('.beer main section#find #results').width();
				 console.log(width);
				 var offset = ($(window).width() * 20) / 100;
				 map.setCenterWithOffset(markerlatLng, -(width - offset), 0);
			 }
			 
			 google.maps.event.trigger(markersArray[markerID - 1], "click");
		});
		
		
		$('#find .filters .filter').on('click touchend', function(){
	  	$('#find .filters .filter').removeClass('active');
	  	$(this).addClass('active');
	  	
	  	if( $(this).html().toLowerCase() == "all" ){
		  	globals.account = "";
	  	}
	  	else {
	  		globals.account = $(this).html();
	  		
	  		if(globals.account == "Retail"){
		  		globals.account = "retail";
	  		}
	  		if(globals.account == "In Stores"){
		  		globals.account = "retail";
	  		}
	  	}
	  	
	  	order = 1;  	
	  	deleteMarkers();
	  	
	  	$.ajax({
			  url: 'https://localbrewingco.com/cms/json',
			  data: 'type=locations&account='+ globals.account +'&zip='+ globals.zip +'&beername='+ globals.beername,
			  dataType: 'jsonp',
			  success: function(data) {
			  	
			  	console.log(data.locations);
			  	
			  	var locations = data.locations;
			  	
			  	$('#find .tap-list .list').empty();
			  	
			  	$('.map-result').removeClass('p-invisible');
				  
				  order = 1;
				  
				  for(var a=0; a < locations.length; a++){
					  plotPoints(locations[a], globals.beername);
				  }
				  
				  if(locations.length < 1){
					  $('#find .tap-list .list')
					  	.append('<div class="not-found">No locations found. Check back soon!</div>');
				  }
			  }
			});	    
	  });
	  
	  
	  $('.icon-search').on('click', function(){
		  $('form input').focus();
		});
		
		$('#shelf .zip').on('click', function(){
			getCurrentLocation();
		});
	  
	  
	  $('form').submit(function(e){
		  e.preventDefault();
		  
		  MapView.prototype.hidekeyboard.call(this);
		  
		  deleteMarkers();
		  
		  var val = $('input', this).val();
		  console.log(val);
		  
		  //$('.map-result span').html('...');
		  
		  if((val.length == 5) && !isNaN(val)){
			  globals.zip = val;
			  findZip(globals.zip);
		  }
		  else {
			  /*
			  globals.beername = val;
			  findBeer(globals.beername);
			  */
			  if(globals.zip != ""){
				  findZip(globals.zip);
			  }
		  }
		  
		  $('.search-close').addClass('p-invisible');
		  $('.search').removeClass('active');
		  $('input', this).val('');
		  
	  });
	  
	  initialize();
	  getAllLocations();
	  //getCurrentLocation();
  
  }
  else {
	  initialize();
  }
}

MapView.prototype.hidekeyboard = function ()
{
	document.activeElement.blur();
  $("input").blur();
}