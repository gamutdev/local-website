/**
*
* @license
* Copyright � 2013 Jason Proctor.  All rights reserved.
*
**/

var	BeerlistView = function ()
{
	console.log ("BeerlistView()");
	
	positron.View.call (this);
};
positron.inherits (BeerlistView, positron.View);

BeerlistView.prototype.onDOMReady = function ()
{
	console.log ("BeerlistView.onDOMReady()");
	
	//var isArchive = this.params.archive;
	
	var self = this;
	
	/*
	$('#beerlist #list').mCustomScrollbar({
		scrollInertia:150,
		axis: 'x',
		contentTouchScroll: 1,
		mouseWheel: {
			enable: false
		},
		scrollButtons:{ 
			enable: false,
			scrollAmount: 194,
			scrollType: "stepped" 
		}
	});
	*/
	
	/*
	if(document.URL.indexOf('#') > -1){
		if(document.URL.indexOf(',') > -1){
			var section = document.URL.split('#')[1].split(',')[0];
			var subsection = document.URL.split(',')[1];
			
			//alert(subsection);
			
			var params = new Object();
					params.action = 'beer-detail';
					params.slug = subsection;
					
			setTimeout(function(){
				gApplication.refreshView('overlay', params);
			}, 500);
		}
	}
	*/
	
	var type = this.params.type;
	
	$('#beerlist .filters .filter').removeClass('active');
	$('#beerlist .filters .filter.'+ type).addClass('active');
	
	//self.resizeScroller();
	
	$(window).resize(function(){
		//self.resizeScroller();
	});
	
	Application.prototype.goToSection.call(this);
	
}

BeerlistView.prototype.resizeScroller = function ()
{
	var count = $('section#beerlist #list .beer-sum').length;
	var margin_left = $('section#beerlist .content').css('margin-left');
	var margin_right = $('section#beerlist .content').css('margin-right');
	
	$('#list .scroller').css({'width': (parseInt((count * 194) + (parseInt(margin_right) + 20)) + (parseInt(margin_left) + 20)) + 'px'});
	
	//$('.mCSB_buttonLeft, .mCSB_buttonRight').css('width', parseInt(margin_left) + 'px');
	
	$('#beerlist #list').mCustomScrollbar("update");
}

