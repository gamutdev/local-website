/**
*
* @license
* Copyright � 2013 Jason Proctor.  All rights reserved.
*
**/

var	OverlayView = function ()
{
	positron.View.call (this);
};

positron.inherits (OverlayView, positron.View);

OverlayView.prototype.onDOMReady = function ()
{
	console.log ("OverlayView.onDOMReady()");
	
	$('body').addClass('stopScroll');
}

OverlayView.prototype.onBeforeInvisible = function ()
{
	console.log ("OverlayView.onBeforeInvisible()");
	
	localStorage.removeItem('lbc_favorite');
	localStorage.removeItem('lbc_saveprofile');
	
	$('.beer-sum').removeClass('noclick');
	$('body').removeClass('stopScroll');
}