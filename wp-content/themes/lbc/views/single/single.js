var	SingleView = function ()
{
	console.log ("SingleView()");
	
	positron.View.call (this);
};
positron.inherits (SingleView, positron.View);

SingleView.prototype.onDOMReady = function ()
{
	console.log ("SingleView.onDOMReady()");
	
	$('body').addClass('stopScroll');
	
	if( !$('body').hasClass('p-fullsize') ){
	
		function adjustHeight(){
			//var height = $('.single-details').eq(horzScroll.currentPage.pageX).outerHeight(true);
			
			var height = $('.single-details').outerHeight(true);
			$('#wrapper').css('height', height);
	
		}
		
		adjustHeight();
	
	}
	
	
	/**************/
	
	function convertValues(input){		
		switch (input) {
			case 0:
        return 181;
	    case 1:
        return 181;
	    case 2:
        return 160;
      case 3:
        return 140;
      case 4:
        return 120;
      case 5:
        return 100;
      case 6:
        return 80;
      case 7:
        return 60;
      case 8:
        return 40;
      case 9:
        return 20;
      case 10:
        return 0;
		}
	}
	
	
	// Beer Profile Graph
	function makeBeerGraph(a,b,c){
		
		if(!a || a == undefined){
			a = 0;
		}
		if(!b || b == undefined){
			b = 0;
		}
		if(!c || c == undefined){
			c = 0;
		}
	
		console.log('makeBeerGraph()');
		console.log(a);
		console.log(b);
		console.log(c);
		
		if((a || b || c) > 0){
			var beerGraph = $('.single #beerPoly')[0];
					beerGraph.setAttribute('points', '0,182 78,'+ convertValues(parseInt(a)) +' 157,'+ convertValues(parseInt(b)) +' 237,'+ convertValues(parseInt(c)) +' 315,182');
		}
		
	}
	
	
	// User Profile Graph
	function makeProfileGraph(a,b,c){
		
		if(!a || a == undefined){
			a = 0;
		}
		if(!b || b == undefined){
			b = 0;
		}
		if(!c || c == undefined){
			c = 0;
		}
	
		console.log('makeProfileGraph()');
		console.log(a);
		console.log(b);
		console.log(c);
	
		// x157 = hoppyness
		// x262 = alcohol
		// x315 = complexity
		
		if((a || b || c) > 0){
			var profileGraph = $('.single #profilePoly')[0];
					profileGraph.setAttribute('points', '0,182 52,'+ convertValues(parseInt(a)) +' 157,'+ convertValues(parseInt(b)) +' 262,'+ convertValues(parseInt(c)) +' 315,182');
		}
		
	}
	
	
	makeBeerGraph(
		this.params.hoppyness, 
		this.params.alcohol, 
		this.params.complexity
	);
	
	/*
	makeProfileGraph(
		beerProfile.hoppy, 
		beerProfile.boozy, 
		beerProfile.complex
	);
	*/
}

SingleView.prototype.close = function ()
{
	setTimeout(function(){
		$('.beer-sum').removeClass('noclick');
	}, 401);
}

SingleView.prototype.shareFacebook = function ()
{
	//alert('Facebook share: '+ this.params.name);
	
	var winWidth = 520;
	var winHeight = 350;
	var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
	
	var name = this.params.name;
	
	var title = 'Local Brewing Co.'+ name;
  var descr = 'Check out "'+ name +'"';
	window.open('http://www.facebook.com/sharer.php?u=http://localbrewingco.com&t='+ descr, 'Local Brewing Co', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
	
}

SingleView.prototype.shareTwitter = function ()
{
	//alert('Twitter share: '+ this.params.name);
	
	var winWidth = 520;
  var winHeight = 350;
  var winTop = (screen.height / 2) - (winHeight / 2);
  var winLeft = (screen.width / 2) - (winWidth / 2);
	
	var name = this.params.name;
	
	var twitterUrl = 'http://twitter.com/intent/tweet?text=Check out "'+ name +'" http://localbrewingco.com @localbrewingco';		
	window.open(twitterUrl, 'Local Brewing Co', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
	
}

SingleView.prototype.fave = function ()
{
	var self = this;
	var id = this.params.id;
	var name = this.params.name;
	
	console.log('SingleView.fave()');
	console.log(id);
	console.log(name);
	
	$.ajax({
  	url: "http://localbrewingco.com/cms/wp-admin/admin-ajax.php",
  	data: {
	    action: 'addFavorite',
	    beer_id: id,
	    user_id: localStorage.lbc_user
		},
		dataType: "jsonp",
		async: true,
		success: function (inData, inTextStatus, inXHR)
		{
      //console.log(inData.status + ': '+ name);
      
      if(inData.status == "exists"){
	      alert(name + ' is already a favorite.');
	    }
	    else if(inData.status == "added"){
		    $('.beer-sum[data-faveid="'+ id +'"]').addClass('favorite');
    		console.log(name + ' has been added to your favorites!');
    	}
    	else {
	    	// Remove the active class
	    	$('.fave[data-faveid="'+ id +'"]').removeClass('active');
	    	$('.beer-sum[data-faveid="'+ id +'"]').removeClass('favorite');
	    	console.log('Adding ' + name + ' did not work, removing...');
    	}
    	
    	// Refresh user feed
    	Application.prototype.refreshFaves.call(this);
		},
		error: function (inXHR, inTextStatus, inThing)
		{
			console.log(inXHR + inTextStatus + inThing);
		}
  });
}

SingleView.prototype.unfave = function ()
{
	var self = this;
	var id = this.params.id;
	var name = this.params.name;
	
	console.log('SingleView.unfave()');
	
	if(gApplication.type == "favorites"){
  	$('.beer-sum[data-faveid="'+ id +'"]').removeClass('p-visible').addClass('p-invisible');
  }
	
	$.ajax({
  	url: "http://localbrewingco.com/cms/wp-admin/admin-ajax.php",
  	data: {
	    action: 'removeFavorite',
	    beer_id: this.params.id,
	    user_id: localStorage.lbc_user
		},
		dataType: "jsonp",
		async: true,
		success: function (inData, inTextStatus, inXHR)
		{
      console.log(inData.status + ': '+ name);
      
      $('.fave[data-faveid="'+ id +'"]').removeClass('active');
	    $('.beer-sum[data-faveid="'+ id +'"]').removeClass('favorite');
	    
	    if(gApplication.type == "favorites"){
	    	$('.beer-sum[data-faveid="'+ id +'"]').removeClass('p-visible').addClass('p-invisible');
	    	
	    	// Update matches
	    	$('.num-results').empty().html($('.beer-sum:visible').length + ' matches');
	    	
	    	if( $('.beer-sum:visible').length < 1 ){
					$('.no-result').removeClass('p-invisible');
				}
				else {
					$('.no-result').addClass('p-invisible');
				}
	    }
      
      // Refresh user feed
      Application.prototype.refreshFaves.call(this);
		},
		error: function (inXHR, inTextStatus, inThing)
		{
			console.log(inXHR + inTextStatus + inThing);
		}
  });
}