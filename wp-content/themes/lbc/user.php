<?php
/*
Template Name: User
*/

$userid = $_GET['userid'];
$basic = $_GET['basic'];
$callback = $_GET['callback'];
	
$user = get_userdata( $userid );
$current_user = get_user_by( 'id', $userid );

$user_onboard = get_user_meta( $user->ID, 'lbc_onboard', true );
if(!$user_onboard){
	$user_onboard = "false";
}

function get_avatar_url_custom($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    return $matches[1];
}
		
// Output
$output = array(
  'id' => $user->ID,
  'username' => $user->display_name,
  'avatar' => get_avatar_url_custom(get_avatar( $user->ID, 100 )),
  'onboard' => $user_onboard,
);

if(!isset($basic) && empty($basic)){
	$args = array( 
		'post_type' => 'profiles', 
		'author' => $userid, 
		'post_status' => 'publish', 
		'posts_per_page' => -1, 
		'order' => 'ASC', 
		'orderby' => 'title'
	);
	
	$temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query($args);
	if ( $wp_query->have_posts() ) :
		while ($wp_query->have_posts()) : $wp_query->the_post();
		
		// Post specific	
		$post_id = $post->ID;
		$post_title = $post->post_title;
		
		// Output
		$output['profiles'][] = array(
		  'id' => $post_id,
		  'name' => $post_title,
		  'bitterness' => get_field('bitterness', $post_id),
		  'alcohol' => get_field('alcohol', $post_id),
		  'complexity' => get_field('complexity', $post_id),
		);
		
		endwhile; 
		wp_reset_postdata();
		$wp_query = null; 
		$wp_query = $temp;

	endif;
	
	if(isset($userid) && !empty($userid)){
		
		
		$args = array( 
			'post_type' => 'beer', 
			'post_status' => 'publish', 
			'posts_per_page' => -1,
			'order' => 'ASC',
			'orderby' => 'title', 
			'tax_query' => array(
				array(
					'taxonomy' => 'lbc_favorites',
					'field' => 'slug',
					'terms' => $current_user->user_nicename
				)
			)
		);
		
		$temp = $wp_query;
		$wp_query = null;
		$wp_query = new WP_Query($args);
		if ( $wp_query->have_posts() ) :
			while ($wp_query->have_posts()) : $wp_query->the_post();
			
			// Post specific	
			$post_id = $post->ID;
			$post_title = $post->post_title;
			$custom = get_post_custom($post_id);	
			
			// Beer Info
			$name = $post_title;
			$style_data = get_field('style');
			foreach($style_data as $key) {
				$style = $key->slug;
			}
			
			$short_description = get_field('short_description');
			$full_description = get_field('description');
			
			// Measurements
			$abv = get_field('abv');
			$ibu = get_field('ibu');
			
			// Profile
			$hoppyness = get_field('hoppyness');
			$alcohol = get_field('alcohol');
			$complexity = get_field('complexity');
			
			// Availability
			$ontap = get_field('ontap');
			$packaged = get_field('packaged');
			
			// Image
			$image = get_field('image');
		
			$beer[] = array(
				'id' => $post_id,
				'name' => $name,
			  'bitterness' => $hoppyness,
			  'alcohol' => $alcohol,
			  'complexity' => $complexity,
			);
			
			// Output
			$output['favorites'] = $beer;
			
			endwhile; 
			wp_reset_postdata();
			$wp_query = null; 
			$wp_query = $temp;
		
		endif;
	}
}
		

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Max-Age: 86400');
header('Cache-Control: no-cache');
header("Content-type: application/javascript");

if(isset($callback) && !empty($callback)){
	$jsonp = json_encode($output);
	die( $callback.'('.$jsonp.')' );
}
else {
	die( json_encode($output) );
}
?>