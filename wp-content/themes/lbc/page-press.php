<?php
get_header();
?>
  
  <main>
	  
		<section id="masthead" style="background: url(img/masthead-distributors.png) 50% 50% no-repeat; background-size:cover;">
    	<div class="content">
    		<h1 class="visibility-web">Press</h1>
    	</div>
  	</section>
	  
		<section id="page">
			
			<?php
				if(empty($_GET['article'])){
					$activeclass = 'list';	
				}
				else {
					$activeclass = 'expanded';
				}
			?>
			
			<div class="content <?php echo $activeclass; ?>">
		  	
		  	<div class="left-column">
			    
			    <div class="content-section clearfix">
		  	
				  	<?php
						if(empty($_GET['article'])){
							$args = array( 'post_type' => 'press', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' );
						}	
						else {
							$args = array( 'post_type' => 'press', 'name' => $_GET['article'] );
						}
							
						$temp = $wp_query;
						$wp_query = null;
						$wp_query = new WP_Query($args);
						while ($wp_query->have_posts()) : $wp_query->the_post();
						
						// Post specific	
						$post_id = $post->ID;
						$post_title = $post->post_title;
						$post_slug = $post->post_name;
						
						$content = get_field('press_content');
						
						?>
						
						<div class="text">
              <h1 class="title"><?php echo $post_title; ?></h1>
              <?php echo $content; ?>
	            </div> 
            </div>
            
            <?php 
						endwhile; 
						wp_reset_postdata(); 
						?>
		    	
					</div>
					
					<aside>
						<ul class="sections">
				    	<?php
					  	$args = array( 'post_type' => 'press', 'posts_per_page' => 10, 'orderby' => 'title', 'order' => 'ASC' );
							
							$temp = $wp_query;
							$wp_query = null;
							$wp_query = new WP_Query($args);
							while ($wp_query->have_posts()) : $wp_query->the_post();
								
								// General
								$post_count = $wp_query->post_count;
								$found_posts = $wp_query->found_posts;
								$pagecount = $wp_query->max_num_pages;
								
								// Post specific	
								$post_id = $post->ID;
								$post_title = $post->post_title;
								$post_slug = $post->post_name;
								$post_status = get_post_status($post_id);
								$post_date = get_the_date('U');
					  	?>
					  	
							<li class="<?php if($_GET['article'] == $post_slug){ echo 'active'; } ?>">
								<a href="<?php echo add_query_arg( 'article', $post_slug ); ?>">
									<div class="date"><?php echo get_the_date(); ?></div>
									<div class="title"><?php the_title(); ?></div>
								</a>
							</li>
							
							<?php
					  	endwhile; 
							wp_reset_postdata();
							$wp_query = null; 
							$wp_query = $temp;
					  	?>
				    </ul>
			  	</aside>
			  	
			  	<a class="button launch large" href="<?php echo remove_query_arg( 'article' ); ?>">All Press</a>
	    
				</div>
	  	
		</section>
  </main>
        
<?php 
get_footer(); 
?>
