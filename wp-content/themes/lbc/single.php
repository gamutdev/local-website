<?php
header("Content-Type: text/html");
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$url = $_SERVER['REQUEST_URI'];
$tokens = explode('/', $url);
$beerslug = $tokens[sizeof($tokens)-2];
 
if (in_array($_SERVER['HTTP_USER_AGENT'], array(
  'facebookexternalhit/1.0 (+https://www.facebook.com/externalhit_uatext.php)',
  'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)'
))) {
  //it's probably Facebook's bot
  
	  if(empty($beerslug)){
			$args = array( 'post_type' => 'beer', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' );
			$url = home_url();
		}	
		else {
			$args = array( 'post_type' => 'beer', 'name' => $beerslug );
			$url = home_url() . '/beer/'. $beerslug;
		}
  
  $temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query($args);
	while ($wp_query->have_posts()) : $wp_query->the_post();
	
	// Post specific	
	$post_id = $post->ID;
	$post_title = $post->post_title;
	$post_slug = $post->post_name;
	$description = get_field( "description" );

  ?>
  <!doctype html>
	<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js" lang="en" xmlns:fb="https://www.facebook.com/2008/fbml"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Local Brewing Co. -- San Francisco, CA -- Small Tanks, Big Beer.</title>
		
		<meta property="og:site_name" content="Local Brewing Co."> 
	  <meta property="og:title" content="Our Beer: <?php echo $post_title; ?>" /> 
	  <meta property="og:url" content="<?php echo $url; ?>" />
		<meta property="og:image" content="http://localbrewingco.com/img/agecheck-logo.png" />
	  <meta property="og:description" content="<?php echo $description; ?>" /> 

	</head>
	
	<body>
	
	<?php 
		endwhile; 
		wp_reset_postdata(); 
		
		die();
	?>
  <?php
}
else {
  //not Facebook
  
  header('Location: '. home_url() .'/beer/#/');
  
}
?>