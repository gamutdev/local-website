<?php
get_header();
?>
  
  <main>
	  
	  <?php
    if ( current_user_can('distributor') || current_user_can('administrator')) {
    ?>
	  
		<section id="masthead" style="background: url(img/masthead-distributors.png) 50% 50% no-repeat; background-size:cover;">
    	<div class="content">
    		<h1 class="visibility-web">Distributors</h1>
    	</div>
  	</section>
  	
		<?php
		}
		?>
	  
		<section id="page">
			
			<?php
	    	if ( current_user_can('distributor') || current_user_can('administrator')) {
	    ?>
			
			<?php
				if(empty($_GET['section'])){
					$activeclass = 'list';	
				}
				else {
					$activeclass = 'expanded';
				}
			?>
			
			<div class="content <?php echo $activeclass; ?>">
		  	
		  	<div class="left-column">
			  	
			    <p>Download Local Brewing Co. artwork, images and other assets for promotional use. Don't see something you need? Email us at <a href="mailto:info@localbrewingco.com">info@localbrewingco.com</a> and we'll do our best to get it for you.</p>
			    
			    <div class="content-section clearfix">
		  	
				  	<?php
					  $section = $_GET['section'];
					  
					  $section_title = $section;
					  $section_title = str_replace('-', " ", $section_title);
						
						if(isset($section)){
							
							echo '<h3>' . ucwords($section_title) . '</h3>';
						
							$temp = $wp_query;
							$wp_query = null;
							$args = array( 'post_type' => 'distributors', 'posts_per_page' => -1, 'type' => $section );
							$wp_query = new WP_Query($args);
							while ($wp_query->have_posts()) : $wp_query->the_post();
							
							// Post specific	
							$post_id = $post->ID;
							$post_title = $post->post_title;
							$post_extension = $post->post_mime_type;
							
							$post_content = $post->post_content;
							$post_content = preg_replace('/<img[^>]+./','', $post_content);
							$post_content = apply_filters('the_content', $post_content);
							$post_content = str_replace(']]>', ']]&gt;', $post_content);
							
							$image = get_field('preview_image');
							
							$valid_ext = array("tif", "pdf", "psd", "zip", "png", "tiff", "ai", "eps");														
							$rows = get_field('files');
							
							if($rows[0]['file']){
							?>
							
							<div class="item clearfix">
								<div class="image"><img src="<?php echo $thumb = $image['sizes'][ 'thumbnail' ]; ?>"></div>
								<div class="title"><?php echo $post_title; ?></div>
								<ul class="options">
									<?php
									if($rows)
									{
										foreach($rows as $row)
										{
											
											$ext = pathinfo($row['file']['url'], PATHINFO_EXTENSION);
							    		if(in_array($ext, $valid_ext)) {
								        echo '<li><a href="'.$row['file']['url'].'">';
							          echo strtoupper($ext);
							          echo '</a></li>';
						          }
						          
										}
									}
									?>
								</ul>
							</div>
						
						<?php
							}
							else {
								echo 'Sorry, no files here yet. Stay tuned!';
							} 
							endwhile; wp_reset_postdata(); 
							
						} else {
							
							$temp = $wp_query;
							$wp_query = null;
							$args = array( 'post_type' => 'distributors', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC' );
							$wp_query = new WP_Query($args);
							while ($wp_query->have_posts()) : $wp_query->the_post();
							
							// Post specific	
							$post_id = $post->ID;
							$post_title = $post->post_title;
							$post_extension = $post->post_mime_type;
							
							$post_content = $post->post_content;
							$post_content = preg_replace('/<img[^>]+./','', $post_content);
							$post_content = apply_filters('the_content', $post_content);
							$post_content = str_replace(']]>', ']]&gt;', $post_content);
							
							$image = get_field('preview_image');
							
							$valid_ext = array("tif", "pdf", "psd", "zip", "png", "tiff", "ai", "eps");														
							$rows = get_field('files');
							
							if($rows[0]['file']){
							?>
							
							<div class="item clearfix">
								<div class="image"><img src="<?php echo $thumb = $image['sizes'][ 'thumbnail' ]; ?>"></div>
								<div class="title"><?php echo $post_title; ?></div>
								<ul class="options">
									<?php
									if($rows)
									{
										foreach($rows as $row)
										{
											
											$ext = pathinfo($row['file']['url'], PATHINFO_EXTENSION);
							    		if(in_array($ext, $valid_ext)) {
								        echo '<li><a href="'.$row['file']['url'].'">';
							          echo strtoupper($ext);
							          echo '</a></li>';
						          }
						          
										}
									}
									?>
								</ul>
							</div>
						
						<?php 
							}
							else {
								echo 'Sorry, no files here yet. Stay tuned!';
							}
							endwhile; wp_reset_postdata();
						} 
						?>
					
		    	</div>
	    
				</div>
				
				<aside>
		    	<ul class="sections">
						
						<li class="all"><a href="<?php echo remove_query_arg( 'section' ); ?>">All</a></li>
						
						<?php
							$section = $_GET['section'];
							$args = array('order'=>'ASC','hide_empty'=>true);
							$terms = get_terms( 'Type', $args );
							if ( $terms ) {
							  foreach ( $terms as $term ) {
								  
								  if($section == $term->slug){
									  $active = 'active';
								  }
								  else {
									  $active = '';
								  }
								  
								  	?>
								  	
								  	<li class="<?php echo $active; ?>"><a href="<?php echo add_query_arg( 'section', $term->slug ); ?>"><?php echo $term->name; ?></a></li>
								  	
								  	<?php
										
							  }
							}
						?>
						
					</ul>
		  	</aside>
			
			<?php
		  } else {
			?>
			
				<!-- Login -->
		    <div class="form login-form">    
							
				  <h2>Distributor Log in</h2>
				
					<form id="loginform" name="loginform" action="<?php $_SERVER['REQUEST_URI'] ?>" method="post">				
						<div class="block">
							<label for="user_login">
							<input type="text" name="user_login" id="user_login" class="input" value="" tabindex="1" placeholder="Username" /></label>
						</div>
						
						<div class="block">
							<label for="user_pass">
							<input type="password" name="user_pass" id="user_pass" class="input" value="" tabindex="2" placeholder="Password" /></label>
						</div>
						
						<div class="block submit clearfix">
							<div class="reg_login left">
								<input type="submit" class="button-primary btn" value="Log in" tabindex="100" />
							</div>
						</div>
						
						<p>Need help? Email us at <a href="mailto:info@localbrewingco.com">info@localbrewingco.com</a></p>				
					</form>
					     	
		    </div>
			 
			<?php
		  }  
		  ?>
	  	
		</section>
  </main>
        
<?php 
get_footer(); 
?>
