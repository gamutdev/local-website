<?php
/** Enable W3 Total Cache */
//define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8/R@%qcsl3P~hQ9g=Xdru]2Uf5(?X!|Hc_I#MYS<,||c-+mEGF5f3r|mj/&=pmd]');
define('SECURE_AUTH_KEY',  '/!n>pmV|E]/MZj##TRXE0aV1Un]%fqB4W&1vpf-C*^6](`Pd`g-NX$MD9Z:B2`W8');
define('LOGGED_IN_KEY',    '/KbsWXsY/#u5I?s.=4@;(-]9lhLUV/WETXcl!Pd(RR9S!B+{b}NR0P8qS^MF+i8-');
define('NONCE_KEY',        'bqEC)@-[|v@w.{{w9|;$>BEN@ihT7Z)/|Td<fQg=;!&3|7sd374]tm 2#5R?zENJ');
define('AUTH_SALT',        'Ny(r]26B,,[st~BlMh?C)d0<*3fzSe}R5i!pAhP4ZhMC;!-|Swg]9syYKrA#=uyK');
define('SECURE_AUTH_SALT', 'jzO+=$mfb~%9ZJ%FCA=Omvc;0:hE.L;et63e41RJ|`Z)N]R|%Q*O`8ZT-uxO:Id_');
define('LOGGED_IN_SALT',   '&%X$rAnAvc@2_qTxr*s+8@LB1VMop{x0O1-sk-f@jh=>y|Ilm-N9Xq1Y-#L[R1E-');
define('NONCE_SALT',       'yKz%cTTxC!i<V&x3Aq$:p+!nq`6yha;L%lhf9wu-gzt?9WU6+9i2h{T^MN*KR;bk');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
